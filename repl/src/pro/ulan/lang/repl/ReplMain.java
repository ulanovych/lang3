package pro.ulan.lang.repl;

import jline.console.ConsoleReader;
import jline.console.UserInterruptException;
import org.fusesource.jansi.AnsiConsole;
import pro.ulan.lang.compiler.CompilationError;
import pro.ulan.lang.compiler.ast.File;
import pro.ulan.lang.compiler.ast.Scope;
import pro.ulan.lang.compiler.ast.Statement;
import pro.ulan.lang.compiler.evaluator.Evaluator;
import pro.ulan.lang.compiler.lexer.LexerImpl;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.typechecking.TypeCheckingVisitor;

import java.io.IOException;
import java.io.InputStream;

import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.ansi;

/**
 * To run from IntelliJ you may want to try forcing the use of the UnsupportedTerminal instead,
 * or setting <code>jline.WindowsTerminal.directConsole=false</code> as a system property.
 * <a href="https://github.com/jline/jline2/issues/185">See</a>
 */
public class ReplMain {
    private final LexerImpl lexer = new LexerImpl();

    private final TypeCheckingVisitor typer = new TypeCheckingVisitor();

    private final Evaluator evaluator = new Evaluator();

    private final ConsoleReader reader = new ConsoleReader();

    private final Scope typerScope = new Scope(null);

    private final Scope evalScope = new Scope(null);

    private ReplMain() throws IOException {
        AnsiConsole.systemInstall();
        reader.setEchoCharacter('\0');
        reader.setHandleUserInterrupt(true); // didn't quit by Ctrl+C without this
        reader.setPrompt(ansi().fg(GREEN).a("> ").reset().toString());
        Token[] tokens = lexer.tokenize(readStdlib());
        ParseResult<File> result = Parsers.file.parse(tokens, 0);
        File file = result.getValue();
        typer.visit(file, typerScope);
        evaluator.evaluate(file, evalScope);
    }

    private void loop() throws IOException {
        String line;
        try {
            while ((line = reader.readLine()) != null && !line.equals("exit")) {
                Token[] tokens = lexer.tokenize(line);
                ParseResult<Statement> statementParseResult = Parsers.stmt.parse(tokens, 0);
                Statement stmt = statementParseResult.getValue();
                typer.errors.clear();
                typer.visit(stmt, typerScope);
                if (!typer.errors.isEmpty()) {
                    for (CompilationError e : typer.errors) {
                        reader.println(e.getMessageWithCodeLine(line));
                        reader.println("");
                    };
                } else {
                    Object res = evaluator.evaluate(stmt, evalScope);
                    reader.println(res != null ? res.toString() : "null");
                }
            }
        } catch (UserInterruptException e) {/* Just quit. */}
    }

    public static void main(String[] args) throws IOException {
        ReplMain repl = new ReplMain();
        repl.loop();
    }

    static String readStdlib() throws IOException {
        InputStream stream = ReplMain.class.getResourceAsStream("/stdlib.lang");
        byte[] buffer = new byte[1024];
        StringBuilder builder = new StringBuilder();
        int i = -1;
        while ((i = stream.read(buffer)) > 0) {
            builder.append(new String(buffer, 0, i, "UTF8"));
        }
        stream.close();
        return builder.toString();
    }
}