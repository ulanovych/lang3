# Goals

## Fast compilation
Incremental compilation.

## IDE support
* Autocompletion
* Show expression type
* Import suggestion
* Parameter info
* Find usages
* Recoverable parsing
* Refactor/rename
* File structure
* Go to class/symbol

## DSLs
* html/xml
* sql

## No reserved words (remember clazz, klass, tpe, chr)

## literate programming

## RPC