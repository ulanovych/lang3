package pro.ulan.lang.meta;

public class Field {
    public final String name;
    public final Type type;

    public Field(String name, Type type) {
        this.name = name;
        this.type = type;
    }
}