package pro.ulan.lang.meta;

public class Param {
    public final String name;
    public final Type type;

    public Param(String name, Type type) {
        this.name = name;
        this.type = type;
    }
}
