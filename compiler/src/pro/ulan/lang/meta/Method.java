package pro.ulan.lang.meta;

import java.util.ArrayList;
import java.util.List;

public class Method {
    public final String name;
    public final List<Param> params = new ArrayList<>();
    public final Type returnType;
    public final boolean aNative;

    public Method(String name, Type returnType, boolean aNative) {
        this.name = name;
        this.returnType = returnType;
        this.aNative = aNative;
    }
}
