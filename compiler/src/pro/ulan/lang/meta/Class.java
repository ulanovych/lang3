package pro.ulan.lang.meta;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Class implements Type {
    private final String name;

    public final List<Field> fields = new ArrayList<>();

    public final List<Method> methods = new ArrayList<>();

    public Class(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public Optional<Method> getMethod(String name) {
        return methods.stream().filter((method) -> method.name.equals(name)).findFirst();
    }

    public void addField(Field field) {
        fields.add(field);
    }

    public void addMethod(Method method) {
        methods.add(method);
    }

    public Field findFieldWithName(String name) {
        return fields.stream().filter(field -> field.name.equals(name)).findFirst().orElse(null);
    }

    @Override
    public boolean equals(Object that) {
        if (that instanceof Class) {
            return this.name.equals(((Class)that).name);
        } else {
            return false;
        }
    }
}
