package pro.ulan.lang.meta;

import java.util.List;

public class Function implements Type {
    public final List<Param> params;
    public final Type returnType;

    public Function(List<Param> params, Type returnType) {
        this.params = params;
        this.returnType = returnType;
    }

    @Override
    public String getName() {
        StringBuilder builder = new StringBuilder();
        builder.append("(");

        for(int i = 0; i < params.size() - 1; i++) {
            builder.append(params.get(i).type.getName());
            builder.append(", ");
        }

        for(int i = params.size() - 1; i < params.size(); i++) {
            builder.append(params.get(i).type.getName());
        }

        builder.append(") => ");
        builder.append(returnType.toString());
        return builder.toString();
    }
}
