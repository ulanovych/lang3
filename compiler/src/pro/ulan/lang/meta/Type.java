package pro.ulan.lang.meta;

public interface Type {
    default boolean isSubtypeOf(Type that) {
        return this.equals(that);
    };

    String getName();
}
