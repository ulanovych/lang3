package pro.ulan.lang.meta;

import java.util.Map;

public class StructuralType implements Type {
    public final Map<String, Type> map;

    public StructuralType(Map<String, Type> map) {
        this.map = map;
    }

    @Override
    public String getName() {
        return "{" + map.entrySet().stream()
            .map(entry -> entry.getKey() + ": " + entry.getValue().getName())
            .reduce((x, y) -> x + ", " + y)
            .orElse("") + "}";
    }
}
