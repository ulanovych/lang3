package pro.ulan.lang.meta;

public class TypeVar implements Type {
    public final String name;

    public TypeVar(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }
}
