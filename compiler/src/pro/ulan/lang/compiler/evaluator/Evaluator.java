package pro.ulan.lang.compiler.evaluator;

import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.util.NonExhaustiveMatch;
import pro.ulan.lang.compiler.util.NotImplemented;
import pro.ulan.lang.meta.Method;
import pro.ulan.lang.meta.Type;
import pro.ulan.lang.meta.Class;

import java.util.List;

public class Evaluator {

    public Object evaluate(File file, Scope scope) {
        Object result = null;

        List<Statement> stmts = file.stmts;

        for(int i = 0; i < stmts.size(); i++) {
            Statement stmt = stmts.get(i);
            result = evaluate(stmt, scope);
        }

        return result;
    }

    public Object evaluate(IntLit intLit) {
        return Integer.parseInt(intLit.token.value);
    }

    public Object evaluate(StringLit lit, Scope scope) {
        return lit.token.value;
    }

    public Object evaluate(NameExpression select, Scope scope) {
        return scope.get(select.token.value);
    }

    public Object evaluate(SetVar setVar, Scope scope) {
        Object res = evaluate(setVar.value, scope);
        scope.putIfContainsOrDelegateToParent(setVar.name.value, res);
        return res;
    }

    public Object evaluate(ValDef valDef, Scope scope) {
        scope.put(valDef.name.value, evaluate(valDef.expr, scope));
        return null;
    }

    public Object evaluate(FunDef def, Scope scope) {
        scope.put(def.name.value, def);
        return null;
    }

    public Object evaluate(MethodCall call, Scope scope) {
        List<Expr> args = call.args;
        Type type = call.expr.getTypeOrThrow();
        Class klass = (Class) type;
        Method method = klass.methods.stream().filter(x -> x.name.equals(call.name.value)).findFirst().get();
//        Class intType = (Class) scope.get("Int");
//        Class stringType = (Class) scope.get("String");
//        Class boolType = (Class) scope.get("Bool");
        if (method.aNative) {
            if (type.getName().equals("Int")) {
                if (args.size() == 1 && args.get(0).getTypeOrThrow().getName().equals("Int")) {
                    int x = (Integer) evaluate(call.expr, scope);
                    int y = (Integer) evaluate(args.get(0), scope);
                    if (call.getTypeOrThrow().getName().equals("Int")) {
                        if (call.name.value.equals("+")) return x + y;
                        else if (call.name.value.equals("-")) return x - y;
                        else if (call.name.value.equals("*")) return x * y;
                        else if (call.name.value.equals("/")) return x / y;
                    } else if (call.getTypeOrThrow().getName().equals("Bool")) {
                        if (call.name.value.equals("==")) return x == y;
                    }
                }
            } else if (type.getName().equals("String")) {
                if (args.size() == 1 && args.get(0).getTypeOrThrow().getName().equals("String") && call.getTypeOrThrow().getName().equals("String")) {
                    String x = (String) evaluate(call.expr, scope);
                    String y = (String) evaluate(args.get(0), scope);
                    if (call.name.value.equals("+")) return x + y;
                }
            }
        }
        throw new NotImplemented();
    }

    public Object evaluate(FunCall call, Scope scope) {
        Scope scope1 = new Scope(scope);
        FunDef def = (FunDef) evaluate(call.expr, scope);

        for (int i = 0; i < def.params.list.size(); i++) {
            Param param = def.params.list.get(i);
            Expr arg = call.args.get(i);
            scope1.put(param.name.value, evaluate(arg, scope));
        }

        List<Statement> stmts = def.body.stmts;
        Object result = null;
        for (int i = 0; i < stmts.size(); i++) {
            result = evaluate(stmts.get(i), scope1);
        }
        return result;
    }

    public Object evaluate(ConditionalExpression expr, Scope scope) {
        Boolean cond = (Boolean) evaluate(expr.condition, scope);
        return evaluate(cond? expr.operand1: expr.operand2, scope);
    }

    public Object evaluate(ClassDef def, Scope scope) {
//        throw new NotImplemented();
        return null;
    }

    public Object evaluate(ObjLit lit, Scope scope) {
        Scope obj = new Scope(null);
        lit.valDefs.forEach(def -> obj.put(def.name.value, evaluate(def.expr, scope)));
        return obj;
    }

    public Object evaluate(GetField getField, Scope scope) {
        Scope obj = (Scope) evaluate(getField.expr, scope);
        return obj.get(getField.fieldName.value);
    }

    public Object evaluate(Node expr, Scope scope) {
        if (expr instanceof IntLit) {
            return evaluate((IntLit) expr);
        } else if (expr instanceof StringLit) {
            return evaluate((StringLit) expr, scope);
        } else if (expr instanceof MethodCall) {
            return evaluate((MethodCall) expr, scope);
        } else if (expr instanceof FunCall) {
            return evaluate((FunCall) expr, scope);
        } else if (expr instanceof NameExpression) {
            return evaluate((NameExpression) expr, scope);
        } else if (expr instanceof SetVar) {
            return evaluate((SetVar) expr, scope);
        } else if (expr instanceof ValDef) {
            return evaluate((ValDef) expr, scope);
        } else if (expr instanceof FunDef) {
            return evaluate((FunDef) expr, scope);
        } else if (expr instanceof ConditionalExpression) {
            return evaluate((ConditionalExpression) expr, scope);
        } else if (expr instanceof ClassDef) {
            return evaluate((ClassDef) expr, scope);
        } else if (expr instanceof ObjLit) {
            return evaluate((ObjLit) expr, scope);
        } else if (expr instanceof GetField) {
            return evaluate((GetField) expr, scope);
        } else {
            throw new NonExhaustiveMatch(expr);
        }
    }
}
