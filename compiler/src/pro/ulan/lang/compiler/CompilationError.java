package pro.ulan.lang.compiler;

import pro.ulan.lang.compiler.lexer.Token;

import java.util.Arrays;

public abstract class CompilationError {
    public final Token firstToken;
    public final Token lastToken;

    public CompilationError(Token firstToken, Token lastToken) {
        this.firstToken = firstToken;
        this.lastToken = lastToken;
    }

    public abstract String getMessage();

    public String getMessageWithCodeLine(String src) {
        String[] lines = src.split("\n");
        String firstLine = getMessage();
        int lineNumber = firstToken.row;
        String lineNumberPrefix = new Integer(lineNumber).toString() + ": ";
        String secondLine = lineNumberPrefix + lines[firstToken.row - 1];
        String thirdLine;
        int startColumn = firstToken.column - 1;
        if (firstToken.row == lastToken.row) {
            int endColumn = lastToken.column + lastToken.value.length() - 1;
            char[] chars = new char[endColumn];
            Arrays.fill(chars, 0, lastToken.column - 1, ' ');
            Arrays.fill(chars, lastToken.column - 1, endColumn, '~');
            thirdLine = new String(new char[lineNumberPrefix.length()]).replace('\0', ' ') + new String(chars);
        } else {
            thirdLine = new String(new char[startColumn]).replace('\0', ' ') + '^';
        }

        return firstLine + '\n' + secondLine + '\n' + thirdLine;
    }
}