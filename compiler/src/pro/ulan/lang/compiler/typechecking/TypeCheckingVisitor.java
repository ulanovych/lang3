package pro.ulan.lang.compiler.typechecking;

import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.ast.Param;
import pro.ulan.lang.compiler.evaluator.Evaluator;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.typechecking.errors.FieldNotFoundError;
import pro.ulan.lang.compiler.typechecking.errors.NoSuchFieldError;
import pro.ulan.lang.compiler.typechecking.errors.TypeMismatchError;
import pro.ulan.lang.compiler.typechecking.errors.VariableNotFoundError;
import pro.ulan.lang.compiler.util.NonExhaustiveMatch;
import pro.ulan.lang.compiler.util.NotImplemented;
import pro.ulan.lang.meta.Class;
import pro.ulan.lang.meta.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import static pro.ulan.lang.compiler.util.MiscUtil.doNothing;

public class TypeCheckingVisitor extends AbstractPayloadVisitor<Scope> {
    private final Evaluator evaluator = new Evaluator();
    public final List<TypeCheckerError> errors = new ArrayList<>();

    @Override
    public void visit(File file, Scope scope) {
        file.stmts.forEach((stmt) -> {
            visit(stmt, scope);
        });
    }

    private void createClassFieldsForPrimaryConstructorParameters(ClassDef classDef, Class klass, Scope scope) {
        List<Param> params = classDef.params.list;
        for (int i = 0; i < params.size(); i++) {
            Param param = params.get(i);
            Type type = (Type) evaluator.evaluate(param.type, scope);
            klass.addField(new Field(param.name.value, type));
        }
    }

    @Override
    public void visit(ClassDef classDef, Scope scope) {
        Class klass = new Class(classDef.name.value);
        classDef.setType(klass);
        scope.put(classDef.name.value, klass);

        createClassFieldsForPrimaryConstructorParameters(classDef, klass, scope);

        classDef.stmts.forEach(stmt -> {
            if (stmt instanceof ValDef) {
                ValDef valDef = (ValDef) stmt;
                Scope memberScope = new Scope(scope);
                memberScope.put("this", klass);
                visit(valDef, memberScope);
                Type type = valDef.getTypeOrNull();
                if (type != null) {
                    klass.addField(new Field(valDef.name.value, type));
                } else {
                    errors.add(new TypeCheckerError(valDef.name, "unable to type check due to previous errors"));
                }
            } else if (stmt instanceof FunDef) {
                FunDef funDef = (FunDef) stmt;
                Scope methodScope = new Scope(scope);
                methodScope.put("this", klass);
                visit(funDef, methodScope);
                Type returnType = funDef.getTypeOrNull();
                if (returnType == null) {

                } else {
                    Statement firstStmt = funDef.body.stmts.get(0);
                    boolean aNative = firstStmt instanceof NameExpression && ((NameExpression) firstStmt).token.value.equals("native");
                    Method method = new Method(funDef.name.value, returnType, aNative);
                    funDef.params.list.forEach(param -> {
                        Type type = (Type) evaluator.evaluate(param.type, scope);
                        method.params.add(new pro.ulan.lang.meta.Param(param.name.value, type));
                    });
                    klass.addMethod(method);
                }
            } else {
                throw new NonExhaustiveMatch(stmt);
            }
        });
    }

    @Override
    public void visit(FunDef funDef, Scope scope) {
        Scope bodyScope = new Scope(scope);

        if (funDef.isGeneric()) {
            TypeParamsDef typeParams = funDef.typeParams.get();
            for (Token token : typeParams.list) {
                bodyScope.put(token.value, new TypeVar(token.value));
            }
        }

        List<pro.ulan.lang.meta.Param> params = new ArrayList<>();
        for (Param param : funDef.params.list) {
            Type type = (Type) evaluator.evaluate(param.type, bodyScope);
            if (type == null) {
                errors.add(new TypeCheckerError(param.type.firstToken, param.type.lastToken, "type not found"));
            } else {
                pro.ulan.lang.meta.Param p = new pro.ulan.lang.meta.Param(param.name.value, type);
                bodyScope.put(p.name, p.type);
                params.add(p);
            }
        }


        List<Statement> stmts = funDef.body.stmts;

        Type returnType;

        if (funDef.returnTypeExpr.isNone()) {
            stmts.forEach(stmt -> {
                visit(stmt, bodyScope);
            });
            returnType = stmts.get(stmts.size() - 1).getTypeOrNull();
        } else {
            returnType = (Type) evaluator.evaluate(funDef.returnTypeExpr.get(), bodyScope);
        }

        if (returnType == null) {
            errors.add(new TypeCheckerError(funDef.name, "unable to get function return type"));
        } else {
            funDef.setType(returnType);

            Function function = new Function(params, returnType);

            scope.put(funDef.name.value, function);

            if (funDef.returnTypeExpr.isSome()) {
                stmts.forEach(stmt -> {
                    visit(stmt, bodyScope);
                });
            }

            Statement lastStmt = stmts.get(stmts.size() - 1);
            Type actual = lastStmt.getTypeOrNull();
            if (actual != null) {
                if (!actual.isSubtypeOf(returnType)) {
                    errors.add(new TypeMismatchError(lastStmt.firstToken, returnType, actual));
                }
            }
        }
    }

    @Override
    public void visit(ValDef valDef, Scope scope) {
        if (scope.get(valDef.name.value) != null) {
            errors.add(new TypeCheckerError(valDef.name, "value " + valDef.name.value + " already present in scope"));
            return;
        }

        visit(valDef.expr, scope);

        Type actual = valDef.expr.getTypeOrNull();

        if (actual == null) {

        } else {
            if (valDef.typeExpr.isNone()) {
                valDef.setType(actual);
            } else {
                Type type = (Type) evaluator.evaluate(valDef.typeExpr.get(), scope);
                valDef.setType(type);
            }

            if (!actual.isSubtypeOf(valDef.getTypeOrThrow())) {
                errors.add(new TypeMismatchError(valDef.expr.firstToken, valDef.getTypeOrThrow(), actual));
            }

            scope.put(valDef.name.value, valDef.getTypeOrThrow());
        }
    }

    @Override
    public void visit(Param param, Scope scope) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(ConditionalExpression expr, Scope scope) {
        Expr cond = expr.condition;
        Expr op1 = expr.operand1;
        Expr op2 = expr.operand2;

        visit(cond, scope);
        visit(op1, scope);
        visit(op2, scope);

        Type boolType = (Type) scope.get("Bool");

        Type condType = cond.getTypeOrNull();
        Type op1Type = op1.getTypeOrNull();
        Type op2Type = op2.getTypeOrNull();

        if (condType != null && op1Type != null && op2Type != null) {
            if (!condType.isSubtypeOf(boolType)) {
                errors.add(new TypeMismatchError(expr.questionMark, boolType, condType));
            }

            if (!op2.getTypeOrThrow().isSubtypeOf(op1.getTypeOrThrow())) {
                errors.add(new TypeMismatchError(expr.colon, op1Type, op2Type));
            }

            expr.setType(op1.getTypeOrThrow());
        }
    }

    @Override
    public void visit(InfixExpression expr, Scope scope) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(NameExpression expr, Scope scope) {
        Object obj = scope.get(expr.token.value);
        if (obj == null) {
            errors.add(new VariableNotFoundError(expr.token));
        } else {
            if (obj instanceof ValDef) {
                ValDef valDef = (ValDef) obj;
                expr.setType(valDef.getTypeOrThrow());
            } else if (obj instanceof Type) {
                expr.setType((Type) obj);
            } else throw new NonExhaustiveMatch(obj);
        }
    }

    @Override
    public void visit(PostfixExpression expr, Scope scope) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(PrefixExpression expr, Scope scope) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(GetField getField, Scope scope) {
        visit(getField.expr, scope);
        Type type = getField.expr.getTypeOrThrow();
        if (type instanceof StructuralType) {
            StructuralType objType = (StructuralType) type;
            Type fieldType = objType.map.get(getField.fieldName.value);
            if (fieldType == null) {
                errors.add(new FieldNotFoundError(objType, getField.fieldName));
            } else {
                getField.setType(fieldType);
            }
        } else if (type instanceof Class) {
            Class klass = (Class) type;
            Field field = klass.findFieldWithName(getField.fieldName.value);
            if (field != null) {
                getField.setType(field.type);
            } else {
                errors.add(new NoSuchFieldError(type, getField.fieldName));
            }
        } else {
            throw new NonExhaustiveMatch(type);
        }
    }

    @Override
    public void visit(SetField setField, Scope scope) {
        visit(setField.expr, scope);
        Type type = setField.getTypeOrNull();
        if (type == null) {
            doNothing();
        } else {
            if (type instanceof Class) {
                Class klass = (Class) type;
                Field field = klass.findFieldWithName(setField.fieldName.value);
                if (field != null) {
                    setField.setType(field.type);
                } else {
                    errors.add(new NoSuchFieldError(type, setField.fieldName));
                }
            }
            else {
                throw new NonExhaustiveMatch(type);
            }
        }
    }

    @Override
    public void visit(MethodCall call, Scope scope) {
        visit(call.expr, scope);

        call.args.forEach(arg -> {
            visit(arg, scope);
        });
        Type type = call.expr.getTypeOrThrow();

        if (!(type instanceof Class)) {
            throw new RuntimeException("can't call methods on " + type.getName());
        }

        Class klass = (Class) type;

        List<Method> methods = klass.methods.stream().filter(method -> method.name.equals(call.name.value)).collect(Collectors.toList());
        if (methods.isEmpty()) {
            throw new RuntimeException(type.getName() + " has no " + call.name.value + " method");
        } else if (methods.size() > 1) {
            throw new RuntimeException(type.getName() + " has more than 1 method " + call.name.value);
        }

        Method method = methods.get(0);

        if (call.args.size() != method.params.size()) {
            throw new RuntimeException("expected " + method.params.size() + " arguments, got " + call.args.size());
        }

        call.setType(method.returnType);
    }

    @Override
    public void visit(FunCall call, Scope scope) {
        visit(call.expr, scope);

        call.args.forEach(arg -> {
            visit(arg, scope);
        });

        Type exprType = call.expr.getTypeOrNull();
        if (exprType != null) {
            if (!(exprType instanceof Function)) {
                errors.add(new TypeCheckerError(call.openRoundBracket, exprType + " is not a function to call"));
            } else {
                call.setType(((Function) exprType).returnType);
            }
        }
    }

    @Override
    public void visit(Arg arg, Scope scope) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(StringLit lit, Scope scope) {
        Type type = (Type) scope.get("String");
        if (type == null) {
            errors.add(new TypeCheckerError(lit.token, "type String not found"));
        } else {
            lit.setType(type);
        }
    }

    @Override
    public void visit(IntLit lit, Scope scope) {
        Type type = (Type) scope.get("Int");

        if (type == null) {
            errors.add(new TypeCheckerError(lit.token, "type Int not found"));
        } else {
            lit.setType(type);
        }
    }

    @Override
    public void visit(ArrLit lit, Scope scope) {
        lit.exprs.forEach(expr -> visit(expr, scope));
        throw new NotImplemented();
        //lit.setType();
    }

    @Override
    public void visit(ObjLit lit, Scope scope) {
        HashMap<String, Type> map = new HashMap<>();

        lit.valDefs.forEach(def -> {
            visit(def, scope);
            map.put(def.name.value, def.expr.getTypeOrThrow());
        });

        lit.setType(new StructuralType(map));
    }

    @Override
    public void visit(SetVar setVar, Scope scope) {
        visit(setVar.value, scope);

        Type expectedType = (Type) scope.get(setVar.name.value);
        if (expectedType == null) {
            errors.add(new VariableNotFoundError(setVar.name));
        } else {
            Type actualType = setVar.value.getTypeOrThrow();
            if (!actualType.isSubtypeOf(expectedType)) {
                errors.add(new TypeMismatchError(setVar.value.firstToken, setVar.value.lastToken, expectedType, actualType));
            } else {
                setVar.setType(actualType);
            }
        }
    }
}
