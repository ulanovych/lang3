package pro.ulan.lang.compiler.typechecking;

import pro.ulan.lang.compiler.CompilationError;
import pro.ulan.lang.compiler.lexer.Token;

public class TypeCheckerError extends CompilationError {
    public final String message;

    public TypeCheckerError(Token firstToken, Token lastToken, String message) {
        super(firstToken, lastToken);
        this.message = message;
    }

    public TypeCheckerError(Token token, String message) {
        this(token, token, message);
    }

    @Override
    public String getMessage() {
        return message;
    }
}
