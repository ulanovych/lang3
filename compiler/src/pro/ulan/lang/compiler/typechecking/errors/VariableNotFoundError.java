package pro.ulan.lang.compiler.typechecking.errors;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;

public class VariableNotFoundError extends TypeCheckerError {
    public VariableNotFoundError(Token token) {
        super(token, "variable " + token.value + " not found");
    }
}