package pro.ulan.lang.compiler.typechecking.errors;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;
import pro.ulan.lang.meta.Type;

public class TypeMismatchError extends TypeCheckerError {
    public final Type expectedType;
    public final Type actualType;

    public TypeMismatchError(Token token, Type expectedType, Type actualType) {
        this(token, token, expectedType, actualType);
    }

    public TypeMismatchError(Token firstToken, Token lastToken, Type expectedType, Type actualType) {
        super(firstToken, lastToken, "expected " + expectedType.getName() + ", got " + actualType.getName());
        this.expectedType = expectedType;
        this.actualType = actualType;
    }
}