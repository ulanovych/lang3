package pro.ulan.lang.compiler.typechecking.errors;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;
import pro.ulan.lang.meta.Type;

public class FieldNotFoundError extends TypeCheckerError {
    public FieldNotFoundError(Type type, Token token) {
        super(token, "field " + token.value + " not found in " + type.getName());
    }
}
