package pro.ulan.lang.compiler.typechecking.errors;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;
import pro.ulan.lang.meta.Type;

public class NoSuchFieldError extends TypeCheckerError {
    public NoSuchFieldError(Type type, Token field) {
        super(field, "type " + type.getName() + " has no such field " + field.value);
    }
}
