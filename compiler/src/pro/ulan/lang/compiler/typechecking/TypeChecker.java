package pro.ulan.lang.compiler.typechecking;

import pro.ulan.lang.compiler.ast.Node;
import pro.ulan.lang.compiler.ast.Scope;

import java.util.List;

public interface TypeChecker {
    public default List<TypeCheckerError> typeCheck(Node node, Scope scope) {
        TypeCheckingVisitor typer = new TypeCheckingVisitor();
        typer.visit(node, scope);
        return typer.errors;
    }
}
