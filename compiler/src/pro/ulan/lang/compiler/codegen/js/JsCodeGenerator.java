package pro.ulan.lang.compiler.codegen.js;

import pro.ulan.lang.compiler.ast.Node;

public class JsCodeGenerator {
    public String generateJsCode(Node node) {
        JsCodeGeneratorVisitor visitor = new JsCodeGeneratorVisitor();
        visitor.visit(node);
        return visitor.getResult();
    }

    public static String encodeIdent(String s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            switch(s.charAt(i)) {
                case ':': builder.append("$colon"); break;
                case '=': builder.append("$equals"); break;
                default: builder.append(s.charAt(i)); break;
            }
        }
        return builder.toString();
    }
}