package pro.ulan.lang.compiler.codegen.js;

import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.util.NonExhaustiveMatch;

import java.util.List;

import static pro.ulan.lang.compiler.codegen.js.JsCodeGenerator.encodeIdent;

public class JsCodeGeneratorVisitor extends BaseToStringVisitor {

    @Override
    public void visit(File file) {
        List<Statement> stmts = file.stmts;
        for (int i = 0; i < stmts.size(); i++) {
            visit(stmts.get(i));
            if (i != stmts.size() - 1) {
                appendln(";");
                appendln();
            }
        }
    }

    @Override
    public void visit(ClassDef classDef) {
        appendln("var " + classDef.name.value + " = {", 1);
        append("apply: function(");
        List<Param> params = classDef.params.list;
        append(params.stream().map(x -> x.name.value).reduce((x, y) -> x + ", " + y).get());
        appendln(") {", 1);
        appendln("return {", 1);
        for(int i = 0; i < Math.min(params.size(), 1); i++) {
            append(params.get(i).name.value + ": " + params.get(i).name.value);
        }
        for(int i = 1; i < params.size(); i++) {
            appendln(",");
            append(params.get(i).name.value + ": " + params.get(i).name.value);
        }
        for (Statement stmt : classDef.stmts) {
            appendln(",");
            if (stmt instanceof ValDef) {
                ValDef def = (ValDef) stmt;
                append(def.name.value + ": ");
                visit(def.expr);
            } else if (stmt instanceof FunDef) {
                FunDef def = (FunDef) stmt;
                append(encodeIdent(def.name.value) + ": ");
                visit(def);
            } else {
                throw new NonExhaustiveMatch(stmt);
            }
        }

        appendln(-1);
        appendln("};", -1);
        appendln("}", -1);
        append("}");
    }

    @Override
    public void visit(FunDef funDef) {
        append("function(");
        List<Param> params = funDef.params.list;
        for (int i = 0; i < params.size(); i++) {
            append(params.get(i).name.value);
            if (i != params.size() - 1) {
                append(", ");
            }
        }
        appendln(") {", 1);
        visit(funDef.body);
        appendln(-1);
        append("}");
    }

    @Override
    public void visit(FunBody funBody) {
        List<Statement> stmts = funBody.stmts;
        int size = stmts.size();

        for (int i = 0; i < size - 1; i++) {
            visit(stmts.get(i));
            appendln(";");
        }
        for (int i = size - 1; i < size; i++) {
            append("return ");
            visit(stmts.get(i));
            append(";");
        }
    }

    @Override
    public void visit(ValDef valDef) {
        append("var " + valDef.name.value + " = ");
        visit(valDef.expr);
    }

    @Override
    public void visit(Param param) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(ConditionalExpression expr) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(InfixExpression expr) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(NameExpression expr) {
        append(expr.token.value);
    }

    @Override
    public void visit(PostfixExpression expr) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(PrefixExpression expr) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(GetField getField) {
        visit(getField.expr);
        append("." + getField.fieldName.value);
    }

    @Override
    public void visit(SetField setField) {
        visit(setField.expr);
        append("." + setField.fieldName.value + " = ");
        visit(setField.value);
    }

    @Override
    public void visit(MethodCall call) {
        visit(call.expr);
        append("." + encodeIdent(call.name.value) + "(");
        List<Expr> args = call.args;
        int size = args.size();
        for (int i = 0; i < size; i++) {
            visit(args.get(i));
            if (i != size - 1) {
                append(", ");
            }
        }
        append(")");
    }

    @Override
    public void visit(FunCall call) {
        visit(call.expr);
        append(".apply(");
        List<Expr> args = call.args;
        for (int i = 0; i < args.size(); i++) {
            visit(args.get(i));
            if (i != args.size() - 1) append(", ");
        }
        append(")");
    }

    @Override
    public void visit(Arg arg) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(StringLit lit) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(IntLit lit) {
        append(lit.token.value);
    }

    @Override
    public void visit(ArrLit lit) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(ObjLit lit) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(SetVar setVar) {
        throw new RuntimeException("not implemented");
    }
}
