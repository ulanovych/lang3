package pro.ulan.lang.compiler.lexer;

public interface Lexer {
    Token[] tokenize(String src);
}
