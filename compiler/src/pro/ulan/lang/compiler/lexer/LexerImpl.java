package pro.ulan.lang.compiler.lexer;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Character.isAlphabetic;
import static java.lang.Character.isDigit;
import static java.lang.Character.isWhitespace;

public class LexerImpl implements Lexer {
    public static final int firstRowNumber = 1;
    public static final int firstColumnNumber = 1;

    public Token[] tokenize(String src) {
        char[] chars = (src + "\0\0\0").toCharArray();

        List<Token> tokens = new ArrayList();

        int index = 0;

        int row = firstRowNumber;
        int column = firstColumnNumber;

        int tokenFirstIndex = 0;
        int tokenLastIndex = 0;

        int tokenRow = firstRowNumber;
        int tokenColumn = firstColumnNumber;

        loop: while (true) {
            char chr = chars[index];

            if (isWhitespace(chr)) {
                do {
                    if (chars[index] == '\n') {
                        row++;
                        column = firstColumnNumber;
                    } else {
                        column++;
                    }
                    index++;
                } while (isWhitespace(chars[index]));
            } else if (isAlphabetic(chr)) {
                tokenFirstIndex = index;
                tokenRow = row;
                tokenColumn = column;
                do {
                    index++;
                    column++;
                } while (isAlphabetic(chars[index]));
                tokenLastIndex = index - 1;
                String value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1);
                Token token = new Token(value, TokenType.Identifier, tokenRow, tokenColumn);
                tokens.add(token);
            } else if (chr == '`') {
                index++;
                column++;
                tokenRow = row;
                tokenColumn = column;
                tokenFirstIndex = index;
                do {
                    index++;
                    column++;
                } while (chars[index] != '`');
                index++;
                column++;
                tokenLastIndex = index - 2;
                String value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1);
                Token token = new Token(value, TokenType.Identifier, tokenRow, tokenColumn);
                tokens.add(token);
            } else if (chr == '"') {
                tokenRow = row;
                tokenColumn = column + 1;
                tokenFirstIndex = index + 1;
                do {
                    index++;
                    column++;
                } while (chars[index] != '"');
                index++;
                column++;
                tokenLastIndex = index - 2;
                String value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1);
                Token token = new Token(value, TokenType.StrLit, tokenRow, tokenColumn);
                tokens.add(token);
            } else if (isDigit(chr)) {
                tokenFirstIndex = index;
                tokenRow = row;
                tokenColumn = column;
                do {
                    index++;
                    column++;
                } while (isDigit(chars[index]));
                tokenLastIndex = index - 1;
                String value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1);
                Token token = new Token(value, TokenType.IntLit, tokenRow, tokenColumn);
                tokens.add(token);
            } else if (isOpChar(chr)) {
                tokenFirstIndex = index;
                tokenRow = row;
                tokenColumn = column;
                do {
                    index++;
                    column++;
                } while (isOpChar(chars[index]));
                tokenLastIndex = index - 1;
                String value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1);
                TokenType tokenType = TokenType.getTokenType(value);
                if (tokenType == null) throw new RuntimeException("unknown token: " + value);
                Token token = new Token(value, tokenType, tokenRow, tokenColumn);
                tokens.add(token);
            } else {
                TokenType type = null;
                tokenRow = row;
                tokenColumn = column;
                switch (chr) {
                    case '(': type = TokenType.OpenRoundBracket; break;
                    case ')': type = TokenType.CloseRoundBracket; break;
                    case '[': type = TokenType.OpenSquareBracket; break;
                    case ']': type = TokenType.CloseSquareBracket; break;
                    case '{': type = TokenType.OpenCurlyBracket; break;
                    case '}': type = TokenType.CloseCurlyBracket; break;
                    case ',': type = TokenType.Comma; break;
                    case '\0': {
                        for(int i = 0; i < 3; i++) {
                            tokens.add(new Token("\0", TokenType.Eof, tokenRow, tokenColumn));
                        }
                        break loop;
                    }
                    default: throw new RuntimeException("unknown character: '" + chr + "'");
                }
                index++;
                column++;
                Token token = new Token(Character.toString(chr), type, tokenRow, tokenColumn);
                tokens.add(token);
            }
        }
        return tokens.toArray(new Token[tokens.size()]);
    }

    private static boolean isOpChar(char chr) {
        switch (chr) {
            case '.':
            case '=':
            case ':':
            case '+':
            case '-':
            case '*':
            case '/':
            case '?':
            case '!':
            case '~':
            case '|':
            case '<':
            case '>':
                return true;
            default:
                return false;
        }
    }
}
