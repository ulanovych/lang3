package pro.ulan.lang.compiler.lexer;

public class Token {
    public final String value;
    public final TokenType type;
    public final int row;
    public final int column;

    public Token(String value, TokenType type, int row, int column) {
        this.value = value;
        this.type = type;
        this.row = row;
        this.column = column;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token = (Token) o;

        if (row != token.row) return false;
        if (column != token.column) return false;
        if (!value.equals(token.value)) return false;
        return type == token.type;

    }

    @Override
    public int hashCode() {
        int result = value.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + row;
        result = 31 * result + column;
        return result;
    }

    @Override
    public String toString() {
        return "Token(\"" + value + "\", " + type + ", " + row + ", " + column + ")";
    }
}
