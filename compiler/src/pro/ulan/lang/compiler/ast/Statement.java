package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.meta.Type;

public abstract class Statement extends Node {
    public final Token firstToken;
    public final Token lastToken;

    public Statement(Token firstToken, Token lastToken) {
        this.firstToken = firstToken;
        this.lastToken = lastToken;
    }

    private Type type;

    public Type getTypeOrThrow() {
        if (type == null) {
            throw new RuntimeException(this + " don't have type. Either you didn't run typer or it's a bug");
        }
        return type;
    }

    public Type getTypeOrNull() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

}
