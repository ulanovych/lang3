package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class PostfixExpression extends Expr {
    public final Expr operand;
    public final Token operator;

    public PostfixExpression(Expr operand, Token operator) {
        super(operator, operator);
        this.operand = operand;
        this.operator = operator;
    }
}
