package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

import java.util.List;

public class MethodCall extends Expr {
    public final Expr expr;
    public final Token name;
    public final List<Expr> args;

    public MethodCall(Expr expr, Token name, List<Expr> args) {
        super(name, name);
        this.expr = expr;
        this.name = name;
        this.args = args;
    }
}
