package pro.ulan.lang.compiler.ast;

import java.util.List;

public class Args {
    public final List<Arg> list;

    public Args(List<Arg> list) {
        this.list = list;
    }
}
