package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class InfixExpression extends Expr {
    public final Token operator;
    public final Expr left;
    public final Expr right;

    public InfixExpression(Token operator, Expr left, Expr right) {
        super(operator, operator);
        this.operator = operator;
        this.left = left;
        this.right = right;
    }
}
