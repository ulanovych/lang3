package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public abstract class Expr extends Statement {
    public Expr(Token firstToken, Token lastToken) {
        super(firstToken, lastToken);
    }
}
