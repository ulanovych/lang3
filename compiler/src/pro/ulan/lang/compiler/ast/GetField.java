package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class GetField extends Expr {
    public final Expr expr;
    public final Token fieldName;

    public GetField(Expr expr, Token fieldName) {
        super(fieldName, fieldName);
        this.expr = expr;
        this.fieldName = fieldName;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + fieldName + ")";
    }
}
