package pro.ulan.lang.compiler.ast;

public interface PayloadVisitor<T> {
    void visit(File file, T payload);
    void visit(ClassDef classDef, T payload);
    void visit(FunDef funDef, T payload);
    void visit(ValDef valDef, T payload);
    void visit(Param param, T payload);
    void visit(Node node, T payload);
    void visit(ConditionalExpression expr, T payload);
    void visit(InfixExpression expr, T payload);
    void visit(NameExpression expr, T payload);
    void visit(PostfixExpression expr, T payload);
    void visit(PrefixExpression expr, T payload);
    void visit(GetField getField, T payload);
    void visit(SetField setField, T payload);
    void visit(MethodCall call, T payload);
    void visit(FunCall call, T payload);
    void visit(Arg arg, T payload);
    void visit(StringLit lit, T payload);
    void visit(IntLit lit, T payload);
    void visit(ArrLit lit, T payload);
    void visit(ObjLit lit, T payload);
    void visit(SetVar setVar, T payload);
}
