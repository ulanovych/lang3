package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class PrefixExpression extends Expr {
    public final Token operator;
    public final Expr operand;

    public PrefixExpression(Token operator, Expr operand) {
        super(operator, operator);
        this.operator = operator;
        this.operand = operand;
    }
}
