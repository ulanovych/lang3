package pro.ulan.lang.compiler.ast;

import java.util.HashMap;
import java.util.Map;

public class Scope {
    final Scope parent;
    private final Map<String, Object> map = new HashMap<>();

    public Scope(Scope parent) {
        this.parent = parent;
    }

    public void put(String name, Object value) {
        map.put(name, value);
    }

    public void putIfContainsOrDelegateToParent(String key, Object value) {
        if (map.containsKey(key)) {
            map.put(key, value);
        } else if (parent != null) {
            parent.putIfContainsOrDelegateToParent(key, value);
        } else {
            throw new RuntimeException("key " + key + " not found in scope");
        }
    }

    public Object get(String name) {
        Object result = map.get(name);
        return result != null ? result : parent != null ? parent.get(name) : null;
    }
}
