package pro.ulan.lang.compiler.ast;

public class Arg {
    public final Expr expr;

    public Arg(Expr expr) {
        this.expr = expr;
    }
}
