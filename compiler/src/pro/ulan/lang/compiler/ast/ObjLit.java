package pro.ulan.lang.compiler.ast;


import pro.ulan.lang.compiler.lexer.Token;

import java.util.List;

public class ObjLit extends Expr {
    public final List<ValDef> valDefs;

    public ObjLit(Token openCurlyBracket, List<ValDef> valDefs, Token closeCurlyBracket) {
        super(openCurlyBracket, closeCurlyBracket);
        this.valDefs = valDefs;
    }
}
