package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class SetField extends Expr {
    public final Expr expr;
    public final Token fieldName;
    public final Expr value;

    public SetField(Expr expr, Token fieldName, Expr value) {
        super(expr.firstToken, value.lastToken);
        this.expr = expr;
        this.fieldName = fieldName;
        this.value = value;
    }
}
