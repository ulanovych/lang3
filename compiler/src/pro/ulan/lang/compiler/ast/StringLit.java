package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class StringLit extends Expr {
    public final Token token;

    public StringLit(Token token) {
        super(token, token);
        this.token = token;
    }
}