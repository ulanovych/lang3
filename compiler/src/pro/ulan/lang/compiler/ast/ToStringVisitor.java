package pro.ulan.lang.compiler.ast;

import java.util.List;

public class ToStringVisitor extends BaseToStringVisitor {
    public void visit(File file) {
        List<Statement> stmts = file.stmts;
        for(int i = 0; i < stmts.size() - 1; i++) {
            visit(stmts.get(i));
            appendln();
            appendln();
        }

        for(int i = stmts.size() - 1; i < stmts.size(); i++) {
            visit(stmts.get(i));
        }
    }

    public void visit(FunDef funDef) {
        append("fun " + funDef.name.value);

        if (funDef.typeParams.isSome()) {
            append("<");
            append(funDef.typeParams.get().list.stream().map(x -> x.value).reduce((x, y) -> x + ", " + y).get());
            append(">");
        }

        append("(");

        if (funDef.params.list.size() > 0) {
            visit(funDef.params.list.get(0));
            for(int i = 1; i < funDef.params.list.size(); i++) {
                append(", ");
                visit(funDef.params.list.get(0));
            }
        }

        append(")");

        if (funDef.returnTypeExpr.isSome()) {
            append(": ");
            visit(funDef.returnTypeExpr.get());
        }

        appendln(" {", 1);

        for(int i = 0; i < funDef.body.stmts.size(); i++) {
            Statement stmt = funDef.body.stmts.get(i);
            visit(stmt);
            if (i != funDef.body.stmts.size() - 1) {
                appendln(";");
            } else {
                appendln(";", -1);
            }
        }
        append("}");
    }

    @Override
    public void visit(FunBody funBody) {
        throw new RuntimeException("not implemented");
    }

    @Override
    public void visit(ValDef valDef) {
        append("val " + valDef.name.value + " = ");
        visit(valDef.expr);
    }

    public void visit(Param param) {
        append(param.name.value);
        if (param.type != null) {
            append(": ");
            visit(param.type);
        }
    }

    public void visit(ConditionalExpression expr) {
        append("(");
        visit(expr.condition);
        append("?");
        visit(expr.operand1);
        append(":");
        visit(expr.operand2);
        append(")");
    }

    public void visit(InfixExpression expr) {
        append("(");
        visit(expr.left);
        append(expr.operator.value);
        visit(expr.right);
        append(")");
    }

    public void visit(NameExpression expr) {
        append(expr.token.value);
    }

    public void visit(PostfixExpression expr) {
        append("(");
        visit(expr.operand);
        append(expr.operator.value);
        append(")");
    }

    public void visit(PrefixExpression expr) {
        append("(");
        append(expr.operator.value);
        visit(expr.operand);
        append(")");
    }

    public void visit(GetField getField) {
        append("(");
        visit(getField.expr);
        append(".");
        append(getField.fieldName.value);
        append(")");
    }

    @Override
    public void visit(SetField setField) {
        visit(setField.expr);
        append("." + setField.fieldName.value + " = ");
        visit(setField.value);
    }

    public void visit(MethodCall call) {
        append("(");
        visit(call.expr);
        append(".");
        append(call.name.value);
        append("(");
        List<Expr> args = call.args;
        if (args.size() > 0) visit(args.get(0));
        for(int i = 1; i < args.size(); i++) {
            append(", ");
            visit(args.get(i));
        }
        append(")");
        append(")");
    }

    @Override
    public void visit(FunCall call) {
        visit(call.expr);

        append("(");

        List<Expr> args = call.args;

        for(int i = 0; i < args.size(); i++) {
            visit(args.get(i));
            if (i != args.size() - 1) {
                append(", ");
            }
        }

        append(")");
    }

    public void visit(Arg arg) {
        visit(arg.expr);
    }

    public void visit(StringLit lit) {
        append("\"" + lit.token.value + "\"");
    }

    public void visit(IntLit lit) {
        append(lit.token.value);
    }

    public void visit(ClassDef classDef) {
        append("class " + classDef.name.value);

        if (classDef.typeParams.isSome()) {
            append("<");
            append(classDef.typeParams.get().list.stream().map(x -> x.value).reduce((x, y) -> x + ", " + y).get());
            append(">");
        }

        append("(");

        for (int i = 0; i < classDef.params.list.size(); i++) {
            visit(classDef.params.list.get(i));
            if (i != classDef.params.list.size() - 1) {
                append(", ");
            }
        }

        append(") {");

        List<Statement> stmts = classDef.stmts;

        if (stmts.isEmpty()) {
            append("}");
        } else {
            appendln("", 1);
            for (int i = 0; i < stmts.size(); i++) {
                visit(stmts.get(i));
                if (i != stmts.size() - 1) {
                    appendln("");
                } else {
                    appendln("", -1);
                }
            }
            append("}");
        }
    }

    @Override
    public void visit(ArrLit lit) {
        append("[");

        for(int i = 0; i < lit.exprs.size(); i++) {
            visit(lit.exprs.get(i));
            if (i != lit.exprs.size() - 1) {
                append(", ");
            }
        }

        append("]");
    }

    @Override
    public void visit(ObjLit lit) {
        append("{");

        List<ValDef> defs = lit.valDefs;

        if (defs.isEmpty()) {
            append("}");
        } else {
            appendln("", 1);

            for (int i = 0; i < defs.size(); i++) {
                visit(defs.get(i));
                if (i != defs.size() - 1) {
                    appendln(",");
                } else {
                    appendln("", -1);
                }
            }

            append("}");
        }
    }

    @Override
    public void visit(SetVar setVar) {
        append(setVar.name.value);
        append(" = ");
        visit(setVar.value);
    }
}
