package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class Param {
    public final Token name;
    public final Expr type;

    public Param(Token name, Expr type) {
        this.name = name;
        this.type = type;
    }
}
