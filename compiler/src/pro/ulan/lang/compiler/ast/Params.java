package pro.ulan.lang.compiler.ast;

import java.util.List;

public class Params {
    public final List<Param> list;

    public Params(List<Param> list) {
        this.list = list;
    }
}
