package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

import java.util.List;

public class FunCall extends Expr {
    public final Expr expr;
    public final Token openRoundBracket;
    public final List<Expr> args;

    public FunCall(Expr expr, Token openRoundBracket, List<Expr> args) {
        super(openRoundBracket, openRoundBracket);
        this.expr = expr;
        this.openRoundBracket = openRoundBracket;
        this.args = args;
    }
}
