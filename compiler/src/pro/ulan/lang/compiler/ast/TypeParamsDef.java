package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

import java.util.List;

public class TypeParamsDef extends Node {
    public final List<Token> list;

    public TypeParamsDef(List<Token> list) {
        this.list = list;
    }
}
