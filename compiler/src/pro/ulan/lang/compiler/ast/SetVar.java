package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class SetVar extends Expr {
    public final Token name;
    public final Expr value;

    public SetVar(Token name, Expr value) {
        super(name, name);
        this.name = name;
        this.value = value;
    }
}