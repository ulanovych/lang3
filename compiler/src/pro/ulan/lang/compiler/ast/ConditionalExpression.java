package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class ConditionalExpression extends Expr {
    public final Expr condition;
    public final Token questionMark;
    public final Expr operand1;
    public final Token colon;
    public final Expr operand2;

    public ConditionalExpression(Expr condition, Token questionMark, Expr operand1, Token colon, Expr operand2) {
        super(questionMark, questionMark);
        this.condition = condition;
        this.questionMark = questionMark;
        this.operand1 = operand1;
        this.colon = colon;
        this.operand2 = operand2;
    }
}
