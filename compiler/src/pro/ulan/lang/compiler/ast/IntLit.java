package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class IntLit extends Expr {
    public final Token token;

    public IntLit(Token token) {
        super(token, token);
        this.token = token;
    }
}
