package pro.ulan.lang.compiler.ast;

public abstract class AbstractPayloadVisitor<T> implements PayloadVisitor<T> {
    public void visit(Node node, T payload) {
        if (node instanceof File) {
            visit((File) node, payload);
        } else if (node instanceof FunDef) {
            visit((FunDef) node, payload);
        } else if (node instanceof ConditionalExpression) {
            visit((ConditionalExpression) node, payload);
        } else if (node instanceof InfixExpression) {
            visit((InfixExpression) node, payload);
        } else if (node instanceof NameExpression) {
            visit((NameExpression) node, payload);
        } else if (node instanceof PostfixExpression) {
            visit((PostfixExpression) node, payload);
        } else if (node instanceof PrefixExpression) {
            visit((PrefixExpression) node, payload);
        } else if (node instanceof GetField) {
            visit((GetField) node, payload);
        } else if (node instanceof SetField) {
            visit((SetField) node, payload);
        } else if (node instanceof MethodCall) {
            visit((MethodCall) node, payload);
        } else if (node instanceof FunCall) {
            visit((FunCall) node, payload);
        } else if (node instanceof StringLit) {
            visit((StringLit) node, payload);
        } else if (node instanceof IntLit) {
            visit((IntLit) node, payload);
        } else if (node instanceof ClassDef) {
            visit((ClassDef) node, payload);
        } else if (node instanceof ValDef) {
            visit((ValDef) node, payload);
        } else if (node instanceof ArrLit) {
            visit((ArrLit) node, payload);
        } else if (node instanceof ObjLit) {
            visit((ObjLit) node, payload);
        } else if (node instanceof SetVar) {
            visit((SetVar) node, payload);
        } else {
            throw new RuntimeException("Non-exhaustive match: " + node.getClass());
        }
    }
}
