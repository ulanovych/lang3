package pro.ulan.lang.compiler.ast;

import java.util.List;

public class File extends Node {
    public final List<Statement> stmts;

    public File(List<Statement> stmts) {
        this.stmts = stmts;
    }
}
