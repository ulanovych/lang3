package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.util.Option;

public class ValDef extends Statement {
    public final Token name;
    public final Option<Expr> typeExpr;
    public final Statement expr;

    public ValDef(Token name, Option<Expr> typeExpr, Statement stmt) {
        super(name, name);
        this.name = name;
        this.typeExpr = typeExpr;
        this.expr = stmt;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + name + ")";
    }
}
