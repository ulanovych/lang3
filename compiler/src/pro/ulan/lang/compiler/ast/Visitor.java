package pro.ulan.lang.compiler.ast;

public interface Visitor {
    void visit(File file);
    void visit(ClassDef classDef);
    void visit(FunDef funDef);
    void visit(FunBody funBody);
    void visit(ValDef valDef);
    void visit(Param param);
    void visit(Node node);
    void visit(ConditionalExpression expr);
    void visit(InfixExpression expr);
    void visit(NameExpression expr);
    void visit(PostfixExpression expr);
    void visit(PrefixExpression expr);
    void visit(GetField getField);
    void visit(SetField setField);
    void visit(MethodCall call);
    void visit(FunCall call);
    void visit(Arg arg);
    void visit(StringLit lit);
    void visit(IntLit lit);
    void visit(ArrLit lit);
    void visit(ObjLit lit);
    void visit(SetVar setVar);
}
