package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.util.Option;

import java.util.List;

public class ClassDef extends Statement {
    public final Token name;
    public final Option<TypeParamsDef> typeParams;
    public final Params params;
    public final List<Statement> stmts;

    public ClassDef(Token name, Option<TypeParamsDef> typeParams, Params params, List<Statement> stmts) {
        super(name, name);
        this.name = name;
        this.typeParams = typeParams;
        this.params = params;
        this.stmts = stmts;
    }
}
