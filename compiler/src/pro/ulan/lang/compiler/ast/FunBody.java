package pro.ulan.lang.compiler.ast;

import java.util.List;

public class FunBody extends Node {
    public final List<Statement> stmts;

    public FunBody(List<Statement> stmts) {
        this.stmts = stmts;
    }
}
