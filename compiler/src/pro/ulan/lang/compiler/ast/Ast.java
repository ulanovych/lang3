package pro.ulan.lang.compiler.ast;

public class Ast {
    public static void print(Node node) {
        ToStringVisitor visitor = new ToStringVisitor();
        visitor.visit(node);
        System.out.println(visitor.getResult());
    }
}
