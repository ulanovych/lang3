package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

import java.util.List;

public class ArrLit extends Expr {
    public final Token openSquareBracket;
    public final List<Expr> exprs;
    public final Token closeSquareBracket;

    public ArrLit(Token openSquareBracket, List<Expr> exprs, Token closeSquareBracket) {
        super(openSquareBracket, closeSquareBracket);
        this.openSquareBracket = openSquareBracket;
        this.exprs = exprs;
        this.closeSquareBracket = closeSquareBracket;
    }
}
