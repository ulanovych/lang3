package pro.ulan.lang.compiler.ast;

public abstract class Node {
    public void prettyPrint() {
        Ast.print(this);
    }

    public String toPrettyString() {
        ToStringVisitor visitor = new ToStringVisitor();
        visitor.visit(this);
        return visitor.getResult();
    }
}
