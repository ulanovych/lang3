package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;

public class NameExpression extends Expr {
    public final Token token;

    public NameExpression(Token token) {
        super(token, token);
        this.token = token;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "(" + token + ")";
    }
}