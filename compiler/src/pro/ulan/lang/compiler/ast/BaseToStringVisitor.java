package pro.ulan.lang.compiler.ast;

public abstract class BaseToStringVisitor extends AbstractVisitor {
    private final StringBuilder builder = new StringBuilder();
    private int indentLevel = 0;
    private String indentString = "    ";

    protected void append(String s) {
        builder.append(s);
    }

    protected void appendln() {
        appendln("", 0);
    }

    protected void appendln(String s) {
        appendln(s, 0);
    }

    protected void appendln(int indexDelta) {
        appendln("", indexDelta);
    }

    protected void appendln(String s, int indentDelta) {
        builder.append(s);
        builder.append("\n");
        indentLevel += indentDelta;
        for (int i = 0; i < indentLevel; i++) {
            append(indentString);
        }
    }

    public String getResult() {
        return builder.toString();
    }
}
