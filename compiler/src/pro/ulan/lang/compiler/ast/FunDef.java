package pro.ulan.lang.compiler.ast;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.util.Option;

public class FunDef extends Statement {
    public final Token name;
    public final Option<TypeParamsDef> typeParams;
    public final Params params;
    public final Option<Expr> returnTypeExpr;
    public final FunBody body;

    public FunDef(Token name, Option<TypeParamsDef> typeParams, Params params, Option<Expr> returnTypeExpr, FunBody body) {
        super(name, name);
        this.name = name;
        this.typeParams = typeParams;
        this.params = params;
        this.returnTypeExpr = returnTypeExpr;
        this.body = body;
    }

    public boolean isGeneric() {
        return typeParams.isSome();
    }

    @Override
    public String toString() {
        return getClass().getName() + "(" + name + ")";
    }
}
