package pro.ulan.lang.compiler.ast;

import java.util.List;

public abstract class AbstractVisitor implements Visitor {
    public void visit(Node node) {
        if (node instanceof File) {
            visit((File) node);
        } else if (node instanceof FunDef) {
            visit((FunDef) node);
        } else if (node instanceof FunBody) {
            visit((FunBody) node);
        } else if (node instanceof ConditionalExpression) {
            visit((ConditionalExpression) node);
        } else if (node instanceof InfixExpression) {
            visit((InfixExpression) node);
        } else if (node instanceof NameExpression) {
            visit((NameExpression) node);
        } else if (node instanceof PostfixExpression) {
            visit((PostfixExpression) node);
        } else if (node instanceof PrefixExpression) {
            visit((PrefixExpression) node);
        } else if (node instanceof GetField) {
            visit((GetField) node);
        } else if (node instanceof SetField) {
            visit((SetField) node);
        } else if (node instanceof MethodCall) {
            visit((MethodCall) node);
        } else if (node instanceof FunCall) {
            visit((FunCall) node);
        } else if (node instanceof StringLit) {
            visit((StringLit) node);
        } else if (node instanceof IntLit) {
            visit((IntLit) node);
        } else if (node instanceof ClassDef) {
            visit((ClassDef) node);
        } else if (node instanceof ValDef) {
            visit((ValDef) node);
        } else if (node instanceof ArrLit) {
            visit((ArrLit) node);
        } else if (node instanceof ObjLit) {
            visit((ObjLit) node);
        } else if (node instanceof SetVar) {
            visit((SetVar) node);
        } else {
            throw new RuntimeException("Non-exhaustive match: " + node.getClass());
        }
    }

    protected void visit(List<Statement> stmts) {
        stmts.forEach((stmt) -> visit(stmt));
    }
}
