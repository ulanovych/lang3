package pro.ulan.lang.compiler.util;

public final class Option<T> {
    private final T value;

    private Option(T value) {
        this.value = value;
    }

    public boolean isNone() { return value == null; }

    public boolean isSome() { return value != null; }

    public T get() {
        if (value == null) {
            throw new RuntimeException("empty option");
        }
        return value;
    }

    public static <T> Option<T> none() {
        return new Option<T>(null);
    }

    public static <T> Option<T> of(T value) {
        return new Option<T>(value);
    }

    public T getOrElse(T deflt) {
        return value != null ? value : deflt;
    }
}
