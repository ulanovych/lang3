package pro.ulan.lang.compiler.util;

public class NotImplemented extends RuntimeException {
    public NotImplemented() {
        super("not implemented");
    }
}
