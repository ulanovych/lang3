package pro.ulan.lang.compiler.util;

public class NonExhaustiveMatch extends RuntimeException {
    public NonExhaustiveMatch(Object obj) {
        super(obj.toString());
    }
}
