package pro.ulan.lang.compiler.util;

public class Tuple2<A, B> {
    public final A _1;
    public final B _2;

    public Tuple2(A _1, B _2) {
        this._1 = _1;
        this._2 = _2;
    }

    public <C> Tuple3<A, B, C> add(C _3) {
        return new Tuple3<>(_1, _2, _3);
    }
}
