package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

public class IdentifierParser extends Parser<Token> {
    @Override
    public ParseResult<Token> parse(Token[] tokens, int index) {
        Token token = tokens[index];
        if (token.type == TokenType.Identifier) {
            return new ParseResult.Success<>(token, index + 1);
        } else {
            return new ParseResult.Error<>("expected identifier, got: " + token.value);
        }
    }
}
