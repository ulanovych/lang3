package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;

public interface InfixParselet {
    Expr parse(ExprParser parser, Expr left, Token token);
    int getPrecedence();
}
