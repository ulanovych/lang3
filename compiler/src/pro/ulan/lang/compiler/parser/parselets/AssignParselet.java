package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.Precedence;
import pro.ulan.lang.compiler.util.NonExhaustiveMatch;
import pro.ulan.lang.compiler.util.NotImplemented;

public class AssignParselet implements InfixParselet {
    @Override
    public Expr parse(ExprParser parser, Expr left, Token token) {
        ParseResult<Expr> result = parser.parse(parser.tokens, parser.index);
        Expr right = result.getValue();
        if (left instanceof NameExpression) {
            NameExpression nameExpression = (NameExpression) left;
            return new SetVar(nameExpression.token, right);
        } else if (left instanceof GetField) {
            GetField getField = (GetField) left;
            return new SetField(getField.expr, getField.fieldName, right);
        } else throw new NonExhaustiveMatch(left);
    }

    @Override
    public int getPrecedence() {
        return Precedence.Assignment.ordinal();
    }
}
