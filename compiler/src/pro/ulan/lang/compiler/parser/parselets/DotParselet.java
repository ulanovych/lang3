package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.MethodCall;
import pro.ulan.lang.compiler.ast.GetField;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.*;

import java.util.List;

public class DotParselet implements InfixParselet {
    @Override
    public Expr parse(ExprParser parser, Expr left, Token token) {
        Token name = parser.consume(TokenType.Identifier);
        if (parser.tokens[parser.index].type != TokenType.OpenRoundBracket) {
            return new GetField(left, name);
        } else {
            parser.consume();
            ParseResult<List<Expr>> result = Parsers.expr.repeat(TokenType.Comma, Cardinality.ZeroOrMore).parse(parser.tokens, parser.index);
            List<Expr> args = result.getValue();
            parser.index = result.getIndex();
            parser.consume(TokenType.CloseRoundBracket);
            return new MethodCall(left, name, args);
        }
    }

    @Override
    public int getPrecedence() {
        return Precedence.Dot.ordinal();
    }
}
