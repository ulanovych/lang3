package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.NameExpression;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;

public class NameParselet implements PrefixParselet {
    public Expr parse(ExprParser parser, Token token) {
        return new NameExpression(token);
    }
}