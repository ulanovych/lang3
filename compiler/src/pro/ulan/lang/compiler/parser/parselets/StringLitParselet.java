package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.StringLit;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;

public class StringLitParselet implements PrefixParselet {
    @Override
    public Expr parse(ExprParser parser, Token token) {
        return new StringLit(token);
    }
}
