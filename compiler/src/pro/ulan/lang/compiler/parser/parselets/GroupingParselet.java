package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.Precedence;

public class GroupingParselet implements PrefixParselet {
    @Override
    public Expr parse(ExprParser parser, Token token) {
        Expr expr = parser.parseExpr();
        parser.consume(TokenType.CloseRoundBracket);
        return expr;
    }


}
