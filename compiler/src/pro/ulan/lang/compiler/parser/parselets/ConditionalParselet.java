package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.ConditionalExpression;
import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.Precedence;

public class ConditionalParselet implements InfixParselet {
    @Override
    public Expr parse(ExprParser parser, Expr left, Token token) {
        Expr thenArm = parser.parseExpr(getPrecedence());
        Token colon = parser.consume(TokenType.Colon);
        Expr elseArm = parser.parseExpr(getPrecedence());
        return new ConditionalExpression(left, token, thenArm, colon, elseArm);
    }

    @Override
    public int getPrecedence() {
        return Precedence.Ternary.ordinal();
    }
}
