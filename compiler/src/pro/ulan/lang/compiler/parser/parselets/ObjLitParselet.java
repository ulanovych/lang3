package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.ObjLit;
import pro.ulan.lang.compiler.ast.ValDef;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.Cardinality;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.RepeatedParser;
import pro.ulan.lang.compiler.util.Option;

import java.util.List;

import static pro.ulan.lang.compiler.parser.Parsers.objLitKeyValueParser;

public class ObjLitParselet implements PrefixParselet {
    private RepeatedParser<ValDef> valDefRepeatedParser = null;

    @Override
    public Expr parse(ExprParser parser, Token token) {
        // handle cyclic ref
        if (valDefRepeatedParser == null) {
            valDefRepeatedParser = objLitKeyValueParser.map(tuple -> new ValDef(tuple._1, Option.none(), tuple._2))
                .repeat(TokenType.Comma, Cardinality.ZeroOrMore);
        }

        ParseResult<List<ValDef>> result = valDefRepeatedParser.parse(parser.tokens, parser.index);
        assert result.isSuccess();
        parser.index = result.getIndex();
        parser.consume(TokenType.CloseCurlyBracket);
        return new ObjLit(token, result.getValue(), parser.tokens[parser.index - 1]);
    }
}