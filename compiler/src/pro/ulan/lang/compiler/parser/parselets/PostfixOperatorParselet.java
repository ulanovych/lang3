package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.PostfixExpression;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.Precedence;

public class PostfixOperatorParselet implements InfixParselet {
    @Override
    public Expr parse(ExprParser parser, Expr left, Token token) {
        return new PostfixExpression(left, token);
    }

    @Override
    public int getPrecedence() {
        return Precedence.Postfix.ordinal();
    }
}
