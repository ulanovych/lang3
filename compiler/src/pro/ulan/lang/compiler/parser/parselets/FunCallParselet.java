package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.FunCall;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.*;

import java.util.List;

public class FunCallParselet implements InfixParselet {
    @Override
    public Expr parse(ExprParser parser, Expr left, Token token) {
        Parser<List<Expr>> p = Parsers.expr.repeat(TokenType.Comma, Cardinality.ZeroOrMore);
        ParseResult<List<Expr>> result = p.parse(parser.tokens, parser.index);
        List<Expr> args = result.getValue();
        parser.index = result.getIndex();
        parser.consume(TokenType.CloseRoundBracket);
        return new FunCall(left, token, args);
    }

    @Override
    public int getPrecedence() {
        return Precedence.Call.ordinal();
    }
}
