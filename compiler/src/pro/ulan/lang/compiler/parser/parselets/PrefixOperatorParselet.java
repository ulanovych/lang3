package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.PrefixExpression;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.Precedence;

public class PrefixOperatorParselet implements PrefixParselet {
    public Expr parse(ExprParser parser, Token token) {
        Expr operand = parser.parseExpr(Precedence.Unary.ordinal());
        return new PrefixExpression(token, operand);
    }
}
