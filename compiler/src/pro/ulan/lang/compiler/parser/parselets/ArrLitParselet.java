package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.ArrLit;
import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.Cardinality;
import pro.ulan.lang.compiler.parser.ExprParser;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.Parsers;

import java.util.List;

public class ArrLitParselet implements PrefixParselet {

    @Override
    public Expr parse(ExprParser parser, Token token) {
        ParseResult<List<Expr>> result = Parsers.expr.repeat(TokenType.Comma, Cardinality.ZeroOrMore).parse(parser.tokens, parser.index);
        assert result.isSuccess();
        parser.index = result.getIndex();
        parser.consume(TokenType.CloseSquareBracket);
        return new ArrLit(token, result.getValue(), parser.tokens[parser.index - 1]);
    }
}