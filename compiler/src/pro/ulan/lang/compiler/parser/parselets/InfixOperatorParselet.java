package pro.ulan.lang.compiler.parser.parselets;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.MethodCall;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ExprParser;

import java.util.Arrays;

public class InfixOperatorParselet implements InfixParselet {
    private final int precedence;

    public InfixOperatorParselet(int precedence) {
        this.precedence = precedence;
    }

    @Override
    public Expr parse(ExprParser parser, Expr left, Token token) {
        Expr right = parser.parseExpr(getPrecedence());
        return new MethodCall(left, token, Arrays.asList(right));
    }

    @Override
    public int getPrecedence() {
        return precedence;
    }
}
