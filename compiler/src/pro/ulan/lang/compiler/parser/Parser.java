package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.util.Option;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

import java.util.function.Function;

public abstract class Parser<T> {
    public abstract ParseResult<T> parse(Token[] tokens, int index);

    public <U> Parser<U> map(Function<T, U> f) {
        return new MapParser<T, U>(this, f);
    }

    public RepeatedParser<T> repeat(TokenType separator, Cardinality cardinality) {
        return new RepeatedParser<>(this, Option.of(separator), cardinality);
    }

    public RepeatedParser<T> repeat(Cardinality cardinality) {
        return new RepeatedParser<>(this, Option.none(), cardinality);
    }
}
