package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.lexer.Token;

public class OrParser<A extends R, B extends R, R> extends Parser<R>{
    final Parser<A> parser1;
    final Parser<B> parser2;

    public OrParser(Parser<A> parser1, Parser<B> parser2) {
        this.parser1 = parser1;
        this.parser2 = parser2;
    }

    @Override
    public ParseResult<R> parse(Token[] tokens, int index) {
        ParseResult<? extends A> result1 = parser1.parse(tokens, index);
        if (result1.isSuccess()) {
            return new ParseResult.Success<>(result1.getValue(), result1.getIndex());
        } else {
            ParseResult<? extends B> result2 = parser2.parse(tokens, index);
            if (result2.isSuccess()) {
                return new ParseResult.Success<>(result2.getValue(), result2.getIndex());
            } else {
                return new ParseResult.Error<>(result1.getError() + " or " + result2.getError());
            }
        }
    }
}
