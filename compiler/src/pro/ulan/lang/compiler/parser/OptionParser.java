package pro.ulan.lang.compiler.parser;


import pro.ulan.lang.compiler.util.Option;
import pro.ulan.lang.compiler.lexer.Token;

public class OptionParser<T> extends Parser<Option<T>> {
    final Parser<T> parser;

    public OptionParser(Parser<T> parser) {
        this.parser = parser;
    }

    @Override
    public ParseResult<Option<T>> parse(Token[] tokens, int index) {
        ParseResult<? extends T> result = parser.parse(tokens, index);
        if (result.isSuccess()) {
            return new ParseResult.Success<>(Option.of(result.getValue()), result.getIndex());
        } else {
            return new ParseResult.Success<>(Option.<T>none(), index);
        }
    }
}
