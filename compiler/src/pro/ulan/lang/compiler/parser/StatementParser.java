package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.lexer.Token;

public class StatementParser extends Parser<Statement> {
    @Override
    public ParseResult<Statement> parse(Token[] tokens, int index) {
        Token token = tokens[index];
        if (token.value.equals("class")) {
            ParseResult<ClassDef> result = Parsers.classDef.parse(tokens, index);
            return new ParseResult.Success<>(result.getValue(), result.getIndex());
        } else if (token.value.equals("fun")) {
            ParseResult<FunDef> result = Parsers.funDef.parse(tokens, index);
            return new ParseResult.Success<>(result.getValue(), result.getIndex());
        } else if (token.value.equals("val")) {
            ParseResult<ValDef> result = Parsers.valDef.parse(tokens, index);
            return new ParseResult.Success<>(result.getValue(), result.getIndex());
        } else {
            ParseResult<Expr> result = Parsers.expr.parse(tokens, index);
            return result.map((x) -> x);
        }
    }
}
