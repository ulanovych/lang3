package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.util.Tuple2;
import pro.ulan.lang.compiler.lexer.Token;

public class AndParser<A, B> extends Parser<Tuple2<A, B>>{
    final Parser<A> parser1;
    final Parser<B> parser2;

    public AndParser(Parser<A> parser1, Parser<B> parser2) {
        this.parser1 = parser1;
        this.parser2 = parser2;
    }

    @Override
    public ParseResult<Tuple2<A, B>> parse(Token[] tokens, int index) {
        ParseResult<? extends A> result1 = parser1.parse(tokens, index);
        if (result1.isError()) {
            return new ParseResult.Error<>(result1.getError());
        } else {
            ParseResult<? extends B> result2 = parser2.parse(tokens, result1.getIndex());
            if (result2.isError()) {
                return new ParseResult.Error<>(result2.getError());
            } else {
                Tuple2<A, B> value = new Tuple2<>(result1.getValue(), result2.getValue());
                return new ParseResult.Success<>(value, result2.getIndex());
            }
        }
    }
}
