package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.util.Option;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

import java.util.ArrayList;
import java.util.List;

public class RepeatedParser<T> extends Parser<List<T>> {
    final Parser<T> parser;
    final Option<TokenType> separator;
    final Cardinality cardinality;

    public RepeatedParser(Parser<T> parser, Option<TokenType> separator, Cardinality cardinality) {
        this.parser = parser;
        this.separator = separator;
        this.cardinality = cardinality;
    }

    @Override
    public ParseResult<List<T>> parse(Token[] tokens, int index) {
        ParseResult<? extends T> result = parser.parse(tokens, index);
        if (result.isError()) {
            switch (cardinality) {
                case ZeroOrMore: return new ParseResult.Success<>(new ArrayList<T>(), index);
                case OneOrMore: return new ParseResult.Error<>(result.getError());
            }
        }

        index = result.getIndex();

        List<T> values = new ArrayList<T>();

        values.add(result.getValue());

        if (separator.isSome()) {
            while (true) {
                if (tokens[index].type == separator.get()) {
                    result = parser.parse(tokens, index + 1);
                    if (result.isSuccess()) {
                        values.add(result.getValue());
                        index = result.getIndex();
                    } else {
                        return new ParseResult.Error<>(result.getError());
                    }
                } else {
                    return new ParseResult.Success<>(values, index);
                }
            }
        } else {
            while (true) {
                result = parser.parse(tokens, result.getIndex());
                if (result.isSuccess()) {
                    values.add(result.getValue());
                    index = result.getIndex();
                } else {
                    return new ParseResult.Success<>(values, index);
                }
            }
        }

    }
}
