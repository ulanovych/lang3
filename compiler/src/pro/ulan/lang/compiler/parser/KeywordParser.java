package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

public class KeywordParser extends Parser<Token> {
    final String keyword;

    public KeywordParser(String keyword) {
        this.keyword = keyword;
    }

    @Override
    public ParseResult<Token> parse(Token[] tokens, int index) {
        Token token = tokens[index];
        if (token.type == TokenType.Identifier && token.value.equals(keyword)) {
           return new ParseResult.Success<>(token, index + 1);
        } else {
            return new ParseResult.Error<>("expected " + keyword + ", got " + token.value);
        }
    }
}
