package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.parselets.*;
import pro.ulan.lang.compiler.util.Tuple2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static pro.ulan.lang.compiler.parser.ParserDsl.*;

public class Parsers {
    public static IdentifierParser ident = new IdentifierParser();

    public static Parser<Expr> expr = new ExprParser(
        new HashMap<TokenType, PrefixParselet>() {{
            put(TokenType.Plus, new PrefixOperatorParselet());
            put(TokenType.Minus, new PrefixOperatorParselet());
            put(TokenType.Tilde, new PrefixOperatorParselet());
            put(TokenType.ExclamationMark, new PrefixOperatorParselet());
            put(TokenType.Identifier, new NameParselet());
            put(TokenType.IntLit, new IntLitParselet());
            put(TokenType.StrLit, new StringLitParselet());
            put(TokenType.OpenRoundBracket, new GroupingParselet());
            put(TokenType.OpenSquareBracket, new ArrLitParselet());
            put(TokenType.OpenCurlyBracket, new ObjLitParselet());
        }},
        new HashMap<TokenType, InfixParselet>() {{
            put(TokenType.Plus, new InfixOperatorParselet(Precedence.Additive.ordinal()));
            put(TokenType.Minus, new InfixOperatorParselet(Precedence.Additive.ordinal()));
            put(TokenType.Asterisk, new InfixOperatorParselet(Precedence.Multiplicative.ordinal()));
            put(TokenType.Slash, new InfixOperatorParselet(Precedence.Multiplicative.ordinal()));
            put(TokenType.Eq, new InfixOperatorParselet(Precedence.Equality.ordinal()));
            put(TokenType.QuestionMark, new ConditionalParselet());
            put(TokenType.Dot, new DotParselet());
            put(TokenType.OpenRoundBracket, new FunCallParselet());
            put(TokenType.Assign, new AssignParselet());
            put(TokenType.CustomOp, new InfixOperatorParselet(Precedence.Lowest.ordinal()));
        }}
    );

    public static Parser<Expr> typeExpr = new ExprParser(
        new HashMap<TokenType, PrefixParselet>() {{
            put(TokenType.Identifier, new NameParselet());
        }},
        new HashMap<TokenType, InfixParselet>() {{
            put(TokenType.VertBar, new InfixOperatorParselet(Precedence.LogicalOr.ordinal()));
        }}
    );

    public static Parser<TypeParamsDef> typeParametersParser = (
        and(
            token(TokenType.Lt),
            ident.repeat(TokenType.Comma, Cardinality.OneOrMore),
            token(TokenType.Gt)
        ).map(tuple -> new TypeParamsDef(tuple._2))
    );

    public static Parser<Statement> stmt = new StatementParser();

    public static Parser<ValDef> valDef = (
        and(
            keyword("val"),
            ident,
            opt(and(token(TokenType.Colon), typeExpr).map((x) -> x._2)),
            token(TokenType.Assign),
            stmt
        ).map((x) -> new ValDef(x._2, x._3, x._5))
    );

    public static Parser<Param> param = (
        and(
            ident,
            opt(and(token(TokenType.Colon), expr).map((x) -> x._2))
        ).map((x) -> new Param(x._1, x._2.getOrElse(null)))
    );

    public static Parser<Params> paramList = (
        and(
            token(TokenType.OpenRoundBracket),
            param.repeat(TokenType.Comma, Cardinality.ZeroOrMore),
            token(TokenType.CloseRoundBracket)
        ).map((x) -> new Params(x._2))
    );

    public static Parser<List<Statement>> stmts = stmt.repeat(Cardinality.ZeroOrMore);

    public static Parser<FunBody> funBody = (
        or(
            and(
                token(TokenType.OpenCurlyBracket),
                stmts,
                token(TokenType.CloseCurlyBracket)
            ).map((x) -> new FunBody(x._2)),
            and(
                token(TokenType.Assign),
                stmt
            ).map((x) -> new FunBody(Arrays.asList(x._2)))
        )
    );

    public static Parser<FunDef> funDef = (
        and(
            keyword("fun"),
            ident,
            opt(typeParametersParser),
            paramList,
            opt(and(token(TokenType.Colon), typeExpr).map((x) -> x._2)),
            funBody
        ).map((x) -> new FunDef(x._2, x._3, x._4, x._5, x._6))
    );

    public static Parser<ClassDef> classDef = (
        and(
            keyword("class"),
            ident,
            opt(typeParametersParser),
            opt(paramList),
            opt(
                and(
                    token(TokenType.OpenCurlyBracket),
                    stmts,
                    token(TokenType.CloseCurlyBracket)
                ).map(x -> x._2)
            )
        ).map((x) -> new ClassDef(x._2, x._3, x._4.getOrElse(new Params(new ArrayList<>())), x._5.getOrElse(new ArrayList<>())))
    );

    public static Parser<Arg> arg = expr.map((x) -> new Arg(x));

    public static Parser<Args> args = arg.repeat(TokenType.Comma, Cardinality.ZeroOrMore).map((xs) -> new Args(xs));

    public static Parser<File> file = stmts.map((x) -> new File(x));

    public static Parser<Tuple2<Token, Expr>> objLitKeyValueParser = (
        and(ident, token(TokenType.Assign), expr).map(tuple -> new Tuple2<>(tuple._1, tuple._3))
    );
}
