package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.lexer.Token;

import java.util.function.Function;

public class MapParser<T, U> extends Parser<U> {
    private final Parser<T> parser;
    private final Function<T, U> f;

    public MapParser(Parser<T> parser, Function<T, U> f) {
        this.parser = parser;
        this.f = f;
    }

    @Override
    public ParseResult<U> parse(Token[] tokens, int index) {
        return parser.parse(tokens, index).map(f);
    }
}
