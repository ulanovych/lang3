package pro.ulan.lang.compiler.parser;

import java.util.function.Function;

public abstract class ParseResult<T> {
    private final T value;
    protected final int index;
    private final String error;

    ParseResult(T value, int index, String error) {
        this.value = value;
        this.index = index;
        this.error = error;
    }

    public boolean isError() { return value == null; }

    public boolean isSuccess() { return value != null; }

    public T getValue() {
        if (isError()) {
            throw new RuntimeException("parse error: " + error);
        }
        return value;
    }

    public abstract int getIndex();

    public String getError() { return error; };

    public <U> ParseResult<U> map(Function<T, U> f) {
        if (isSuccess()) {
            return new Success<U>(f.apply(value), index);
        } else {
            return new Error<U>(error);
        }
    }

    public static class Success<T> extends ParseResult<T> {
        Success(T value, int index) {
            super(value, index, null);
        }

        @Override public  int getIndex() {
            return index;
        }
    }

    public static class Error<T> extends ParseResult<T> {
        Error(String error) {
            super(null, -1, error);
        }

        @Override public int getIndex() {
            throw new RuntimeException("no index for parser error available");
        }
    }
}
