package pro.ulan.lang.compiler.parser;

public enum Precedence {
    OccupyZeroPrecedence,
    Lowest,
    Assignment,
    Ternary,
    LogicalOr,
    LogicalAnd,
    BitwiseInclusiveOr,
    BitwiseExclusiveOr,
    BitwiseAnd,
    Equality,
    Relational,
    Shift,
    Additive,
    Multiplicative,
    Unary,
    Postfix,
    Call,
    Dot
}
