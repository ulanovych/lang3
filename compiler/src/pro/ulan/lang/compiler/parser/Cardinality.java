package pro.ulan.lang.compiler.parser;

public enum Cardinality {
    ZeroOrMore, OneOrMore
}
