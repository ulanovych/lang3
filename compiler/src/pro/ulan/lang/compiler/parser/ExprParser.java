package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.parselets.*;

import java.util.HashMap;
import java.util.Map;

public class ExprParser extends Parser<Expr> {
    public final Map<TokenType, PrefixParselet> prefixParselets;
    public final Map<TokenType, InfixParselet> infixParselets;

    public ExprParser(Map<TokenType, PrefixParselet> prefixParselets, Map<TokenType, InfixParselet> infixParselets) {
        this.prefixParselets = prefixParselets;
        this.infixParselets = infixParselets;
    }

    public Token[] tokens;
    public int index;

    @Override
    public ParseResult<Expr> parse(Token[] tokens, int index) {
        this.tokens = tokens;
        this.index = index;
        try {
            Expr expr = parseExpr(0);
            return new ParseResult.Success<>(expr, this.index);
        } catch(RuntimeException e) {
            return new ParseResult.Error<>(e.getMessage());
        }
    }

    public Token consume() {
        return tokens[index++];
    }

    public Token consume(TokenType type) {
        if (tokens[index].type != type) {
            throw new RuntimeException("expected " + type + ", got " + tokens[index]);
        }
        return consume();
    }

    public Expr parseExpr() {
        return parseExpr(0);
    }

    public Expr parseExpr(int precedence) {
        Token token = consume();
        PrefixParselet prefixParselet = prefixParselets.get(token.type);

        if (prefixParselet == null) {
            throw new RuntimeException("no prefix parselet for " + token);
        }

        Expr left = prefixParselet.parse(this, token);

        while (precedence < getPrecedence()) {
            token = consume();
            InfixParselet infix = infixParselets.get(token.type);
            left = infix.parse(this, left, token);
        }

        return left;
    };

    private int getPrecedence() {
        InfixParselet parser = infixParselets.get(tokens[index].type);
        return parser != null ? parser.getPrecedence() : 0;
    }
}
