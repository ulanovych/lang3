package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

public class TokenParser extends Parser<Token>{
    final TokenType type;

    public TokenParser(TokenType type) {
        this.type = type;
    }

    @Override
    public ParseResult<Token> parse(Token[] tokens, int index) {
        Token token = tokens[index];
        if (token.type == type) {
            return new ParseResult.Success<>(token, index + 1);
        } else {
            return new ParseResult.Error<>("expected " + type.name() + ", got " + token);
        }
    }
}
