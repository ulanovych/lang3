package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.util.*;

public class ParserDsl {
    public static KeywordParser keyword(String keyword) {
        return new KeywordParser(keyword);
    }

    public static TokenParser token(TokenType type) {
        return new TokenParser(type);
    }

    public static <T> Parser<Option<T>> opt(Parser<T> parser) {
        return new OptionParser<T>(parser);
    }

    public static <A, B> Parser<Tuple2<A, B>> and(Parser<A> a, Parser<B> b) {
        return new AndParser<>(a, b);
    }

    public static <A, B, C> Parser<Tuple3<A, B, C>> and(Parser<A> a, Parser<B> b, Parser<C> c) {
        return and(and(a, b), c).map((x) -> x._1.add(x._2));
    }

    public static <A, B, C, D> Parser<Tuple4<A, B, C, D>> and(Parser<A> a, Parser<B> b, Parser<C> c, Parser<D> d) {
        return and(and(a, b, c), d).map((x) -> x._1.add(x._2));
    }

    public static <A, B, C, D, E> Parser<Tuple5<A, B, C, D, E>> and(Parser<A> a, Parser<B> b, Parser<C> c, Parser<D> d, Parser<E> e) {
        return and(and(a, b, c, d), e).map((x) -> x._1.add(x._2));
    }

    public static <A, B, C, D, E, F> Parser<Tuple6<A, B, C, D, E, F>> and(Parser<A> a, Parser<B> b, Parser<C> c, Parser<D> d, Parser<E> e, Parser<F> f) {
        return and(and(a, b, c, d, e), f).map((x) -> x._1.add(x._2));
    }

    public static <A extends R, B extends R, R> Parser<R> or(Parser<A> a, Parser<B> b) {
        return new OrParser<>(a, b);
    }

    public static <A extends R, B extends R, C extends R, R> Parser<R> or(Parser<A> a, Parser<B> b, Parser<C> c) {
        return or(or(a, b), c);
    }
}
