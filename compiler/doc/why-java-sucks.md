# Vague NPE cause

When you get a NPE in a complex expression like

    ...
    if (args.size() == 1 && args.get(0).getType().getName().equals("Int") && call.getType().getName().equals("Int")) {
    ...

all information you get is a line number

    java.lang.NullPointerException
        at pro.ulan.lang.compiler.evaluator.Evaluator.evaluate(Evaluator.java:22)

Java! Why don't you tell me what exactly was null, `args`, `args.get(0)`, `args.get(0).getType()`, `args.get(0).getType().getName()`,
`call`, `call.getType()`, `call.getType().getName()`?

# Cast after `instanceOf` check
Why do we have to cast if we already checked that it's proper object?

    ...
    } else if (node instanceof StringLit) {
        visit((StringLit) node);
    } else if (node instanceof IntLit) {
        visit((IntLit) node);
    ...

# Reference equality via `==` operator
`.equals()` is used much more often that reference equality so should be by default.

    if (call.name.actualToken == "+") return x + y; // doesn't work even if call.name.actualToken is "+"

# Not type-unsafe `equals()
Here is perfectly fine code

    klass.methods.stream().filter(method -> method.name.equals(call.name)).findFirst();

Just looking for the method with given name, what could be simpler. No room for errors,
it's compiled fine and it won't find anything ever. Cause `method.name` is a `String`
and `call.name` is not. Who cares. Type safety? Never heard.


The right way

    klass.methods.stream().filter(x -> x.name.equals(call.name.value)).findFirst();
