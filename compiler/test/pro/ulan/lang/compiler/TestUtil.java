package pro.ulan.lang.compiler;

public class TestUtil {
    public static String makeSrc(Void dummy, String... lines) {
        return String.join("\n", lines);
    }

    public static final String stdlib = makeSrc(null
        , "class Bool {}"
        , ""
        , "class Int {"
        , "    fun `+`(that: Int): Int  = native"
        , "    fun `-`(that: Int): Int  = native"
        , "    fun `*`(that: Int): Int  = native"
        , "    fun `/`(that: Int): Int  = native"
        , ""
        , "    fun `==`(that: Int): Bool  = native"
        , "}"
        , ""
        , "class String {"
        , "    fun `+`(that: String): String  = native"
        , "}"
        , ""
    );
}
