package pro.ulan.lang.compiler.parser;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.ArrLit;
import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.GetField;
import pro.ulan.lang.compiler.ast.NameExpression;
import pro.ulan.lang.compiler.lexer.LexerImpl;
import pro.ulan.lang.compiler.lexer.Token;

import static org.junit.Assert.assertEquals;
import static pro.ulan.lang.compiler.parser.Parsers.expr;

public class ExprParserTest extends BaseParserTest {

    @Test
    public void test() {
        LexerImpl lexer = new LexerImpl();
        Token[] tokens = lexer.tokenize("a + b a + b");
        Parser<Expr> parser = Parsers.expr;
        ParseResult<Expr> result = parser.parse(tokens, 0);
        assertEquals("(a.+(b))", result.getValue().toPrettyString());
    }

    @Test
    public void methodCall() {
        ParseResult<Expr> result = parse(expr, "\"FF\".toInt(16)");
        assertEquals("(\"FF\".toInt(16))", result.getValue().toPrettyString());
    }

    @Test
    public void getField() {
        ParseResult<Expr> result = parse(expr, "user.name");
        Expr expr = result.getValue();
        GetField getField = (GetField) expr;
        assertEquals("user", ((NameExpression) getField.expr).token.value);
        assertEquals("name", getField.fieldName.value);
    }

    @Test
    public void select() {
        ParseResult<Expr> result = parse(expr, "a.a + b.b");
        assertEquals("((a.a).+((b.b)))", result.getValue().toPrettyString());
    }

    @Test
    public void grouping() {
        ParseResult<Expr> result = parse(expr, "(2 + 2) * (2 + 2)");
        Expr expr = result.getValue();
        assertEquals("((2.+(2)).*((2.+(2))))", expr.toPrettyString());
    }

    @Test
    public void functionCall() {
        ParseResult<Expr> result = parse(expr, "f(x, y)");
        Expr expr = result.getValue();
        assertEquals("f(x, y)", expr.toPrettyString());
    }

    @Test
    public void arrLit() {
        ParseResult<Expr> result = parse(expr, "[1, 2]");
        ArrLit lit = (ArrLit) result.getValue();
        assertEquals("[1, 2]", lit.toPrettyString());
    }
}
