package pro.ulan.lang.compiler.parser;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.ClassDef;
import pro.ulan.lang.compiler.ast.File;

public class ClassDefParserTest extends BaseParserTest {
    @Test
    public void classDef() {
        ParseResult<ClassDef> result = parse(Parsers.classDef, "class Foo");
        result.getValue();
    }

    @Test
    public void classDef2() {
        ParseResult<ClassDef> result = parse(Parsers.classDef
            , "class Foo {"
            , "    fun bar() = 42"
            , "}"
        );
        result.getValue();
    }

    @Test
    public void classDefWithVal() {
        parseSuccess(Parsers.classDef
            , "class Foo {"
            , "    val i = 42"
            , "}"
        );
    }

    @Test
    public void stdlib() {
        ParseResult<File> result = parse(Parsers.file
            , "class C {"
            , "    fun f() = 42"
            , "}"
        );
        result.getValue();
    }

    @Test
    public void genericClass() {
        ClassDef classDef = parseSuccess(Parsers.classDef, "class Foo<T>");
        expect(classDef.toPrettyString()).toBe(null
            , "class Foo<T>() {}"
        );
    }
}
