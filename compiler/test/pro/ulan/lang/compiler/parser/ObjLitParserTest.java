package pro.ulan.lang.compiler.parser;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.ast.ObjLit;

import static org.junit.Assert.assertEquals;
import static pro.ulan.lang.compiler.parser.Parsers.expr;

public class ObjLitParserTest extends BaseParserTest {
    @Test
    public void objLitEmpty() {
        ParseResult<Expr> result = parse(expr, "{}");
        ObjLit lit = (ObjLit) result.getValue();
        assertEquals("{}", lit.toPrettyString());
    }

    @Test
    public void objLitSingleField() {
        ParseResult<Expr> result = parse(expr, "{port = 8080}");
        ObjLit lit = (ObjLit) result.getValue();
        expect(lit).toBe(null
            , "{"
            , "    val port = 8080"
            , "}"
        );
    }

    @Test
    public void objLitMultipleFields() {
        ParseResult<Expr> result = parse(expr, "{host = \"localhost\", port = 8080}");
        ObjLit lit = (ObjLit) result.getValue();
        expect(lit).toBe(null
            , "{"
            , "    val host = \"localhost\","
            , "    val port = 8080"
            , "}"
        );
    }

}
