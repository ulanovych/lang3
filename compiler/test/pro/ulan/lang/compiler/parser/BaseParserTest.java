package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.ast.Node;
import pro.ulan.lang.compiler.lexer.BaseLexerTest;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

import static org.junit.Assert.fail;


public abstract class BaseParserTest extends BaseLexerTest {
    public <T> ParseResult<T> parse(Parser<T> parser, String s) {
        Token[] tokens = tokenize(s);
        ParseResult<T> result = parser.parse(tokens, 0);
        if (result.isError()) {
            fail("parsing error: " + result.getError());
        }
        if (result.isSuccess() && (tokens[result.getIndex()].type != TokenType.Eof)) {
            fail("didn't parse all, stopped at " + tokens[result.getIndex()]);
        }
        return result;
    }

    public <T> T parseSuccess(Parser<T> parser, String s) {
        return parse(parser, s).getValue();
    }

    public <T> T parseSuccess(Parser<T> parser, String... lines) {
        return parse(parser, String.join("\n", lines)).getValue();
    }

    public <T> ParseResult<T> parse(Parser<T> parser, String... lines) {
        return parse(parser, String.join("\n", lines));
    }

    static <T extends Node> ParserExpectationBuilder<T> expect(T node) {
        return new ParserExpectationBuilder<>(node);
    }
}
