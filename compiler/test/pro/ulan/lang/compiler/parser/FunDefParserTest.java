package pro.ulan.lang.compiler.parser;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.FunDef;

public class FunDefParserTest extends BaseParserTest {
    @Test
    public void funDef() {
        ParseResult<FunDef> result = parse(Parsers.funDef, "fun f(): Int = 42");
        result.getValue();
    }

    @Test
    public void funMultilineBody() {
        parseSuccess(Parsers.funDef
            , "fun `:=`(newValue: T) {"
            , "    this.currentValue = newValue"
            , "}"
        );
    }

    @Test
    public void genericFun() {
        FunDef funDef = parseSuccess(Parsers.funDef, "fun f<T>(x: T) = x");
        expect(funDef.toPrettyString()).toBe(null
            , "fun f<T>(x: T) {"
            , "    x;"
            , "}"
        );
    }
}
