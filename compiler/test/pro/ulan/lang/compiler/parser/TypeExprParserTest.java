package pro.ulan.lang.compiler.parser;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.Expr;

public class TypeExprParserTest extends BaseParserTest {
    @Test
    public void orType() {
        ParseResult<Expr> result = parse(Parsers.typeExpr, "Int | String");
        Expr expr = result.getValue();
        expect(expr).toBe("(Int.|(String))");
    }
}
