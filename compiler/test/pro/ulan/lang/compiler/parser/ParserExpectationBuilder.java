package pro.ulan.lang.compiler.parser;

import pro.ulan.lang.compiler.ast.Node;

import static org.junit.Assert.assertEquals;

class ParserExpectationBuilder<T extends Node> {
    final T node;

    ParserExpectationBuilder(T node) {
        this.node = node;
    }

    void toBe(Void dummy, String... lines) {
        toBe(String.join("\n", lines));
    }

    void toBe(String s) {
        assertEquals(s, node.toPrettyString());
    }
}
