package pro.ulan.lang.compiler.parser;

import junit.framework.TestCase;
import org.junit.Test;
import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.util.Tuple2;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static pro.ulan.lang.compiler.TestUtil.makeSrc;

public class ParserTest extends BaseParserTest {

    private <T extends Node> void test(Parser<T> parser, String src, String expected) {
        Token[] tokens = tokenize(src);

        ParseResult<T> result = parser.parse(tokens, 0);

        if (result.isSuccess() && (tokens[result.getIndex()].type != TokenType.Eof)) {
            throw new RuntimeException("didn't parse all, stopped at " + tokens[result.getIndex()]);
        }

        String actual = result.getValue().toPrettyString();

        assertEquals(expected, actual);

    }

    @Test
    public void testFunDef() {
        ParseResult<FunDef> result = parse(Parsers.funDef, "fun f(x, y) { a b c }");
        FunDef funDef = result.getValue();

        assertEquals(funDef.name.value, "f");

        Params params = funDef.params;

        assertEquals(params.list.size(), 2);

        assertEquals("x", params.list.get(0).name.value);
        assertEquals("y", params.list.get(1).name.value);

        List<Statement> stmts = funDef.body.stmts;

        assertEquals(3, stmts.size());

        assertEquals("a", ((NameExpression)stmts.get(0)).token.value);
        assertEquals("b", ((NameExpression)stmts.get(1)).token.value);

    }

    @Test
    public void factorial() {
        ParseResult<FunDef> result = parse(Parsers.funDef, "fun f(x: Int): Int = x == 1 ? 1 : f(x - 1)");
        FunDef funDef = result.getValue();

        assertEquals(funDef.name.value, "f");

        Params params = funDef.params;

        assertEquals(params.list.size(), 1);

        assertEquals("x", params.list.get(0).name.value);

        List<Statement> stmts = funDef.body.stmts;

        assertEquals(1, stmts.size());

        assertEquals(ConditionalExpression.class, stmts.get(0).getClass());
    }

    @Test
    public void ident() {
        ParseResult<Token> result = parse(Parsers.ident, "a");
        assertEquals(1, result.getIndex());
    }

    @Test
    public void repeatNoSep() {
        ParseResult<List<Token>> result = parse(Parsers.ident.repeat(Cardinality.ZeroOrMore), "a b");
        assertEquals(2, result.getIndex());
    }


    @Test
    public void stmts() {
        ParseResult<List<Statement>> result = parse(Parsers.stmts, "a b");
        assertEquals(2, result.getIndex());
        List<Statement> stmts = result.getValue();
        TestCase.assertEquals(2, stmts.size());
        assertEquals("a", ((NameExpression)stmts.get(0)).token.value);
        assertEquals("b", ((NameExpression)stmts.get(1)).token.value);
    }

    @Test
    public void funBody() {
        FunBody body = parse(Parsers.funBody, "{a b}").getValue();
        assertEquals(2, body.stmts.size());
    }

    @Test
    public void valDef() {
        ParseResult<ValDef> result = parse(Parsers.valDef, "val x = 42");
        assertEquals("val x = 42", result.getValue().toPrettyString());
    }

    @Test
    public void expr() {
        parse(Parsers.expr, "\"Hello \" + \"World!\"");
    }

    @Test
    public void classDef() {
        String actual = parse(Parsers.classDef, "class Int { fun plus(that: Int): Int = native }").getValue().toPrettyString();

        String expected = (
            "class Int() {\n" +
            "    fun plus(that: Int): Int {\n" +
            "        native;\n" +
            "    }\n" +
            "}"
        );

        assertEquals(expected, actual);
    }

    @Test
    public void classAssignment() {
        ParseResult<Statement> result = parse(Parsers.stmt, "val Foo = class Bar {}");
        assertEquals("val Foo = class Bar() {}", result.getValue().toPrettyString());
    }

    @Test
    public void classDefEmpty() {
        ParseResult<ClassDef> result = parse(Parsers.classDef, "class Int {}");
        assertEquals("class Int() {}", result.getValue().toPrettyString());
    }


    @Test
    public void stmtClassDefEmpty() {
        ParseResult<Statement> result = parse(Parsers.stmt, "class Int {}");
        assertEquals("class Int() {}", result.getValue().toPrettyString());
    }

    @Test
    public void stmtFun() {
        ParseResult<Statement> result = parse(Parsers.stmt, "fun f(x: Int): Int = x");
        assertEquals("fun f(x: Int): Int {\n    x;\n}", result.getValue().toPrettyString());
    }

    @Test
    public void stmtValDef() {
        ParseResult<Statement> result = parse(Parsers.stmt, "val i = 42");
        assertEquals("val i = 42", result.getValue().toPrettyString());
    }

    @Test
    public void operatorIntoMethodCall() {
        ParseResult<Statement> result = parse(Parsers.stmt, "2 + 2");
        assertEquals("(2.+(2))", result.getValue().toPrettyString());
    }

    @Test public void funReturnsClass() {
        test(Parsers.funDef,
            makeSrc(null
                , "fun f() = class F {}"
            ),
            makeSrc(null
                , "fun f() {"
                , "    class F() {};"
                , "}"
            )
        );
    }

    @Test public void simpleClass() {
        test(Parsers.classDef,
            makeSrc(null
                , "class User(id: Int, name: String)"
            ),
            makeSrc(null
                , "class User(id: Int, name: String) {}"
            )
        );
    }

    @Test
    public void objLitKeyValue() throws Exception {
        Token[] tokens = tokenize("port = 8080");
        ParseResult<Tuple2<Token, Expr>> result = Parsers.objLitKeyValueParser.parse(tokens, 0);
        Tuple2<Token, Expr> tuple = result.getValue();
        assertEquals("port", tuple._1.value);
        assertEquals("8080", ((IntLit)tuple._2).token.value);
    }

    @Test
    public void assign() {
        test(Parsers.expr, "x = 42", "x = 42");
    }

    @Test
    public void assign1() {
        test(Parsers.file, "val x = 42 x = 43 x", "val x = 42\n\nx = 43\n\nx");
    }

    @Test
    public void setField() {
        test(Parsers.stmt, "user.id = 42", "user.id = 42");
    }
}
