package pro.ulan.lang.compiler.codegen.js;

import org.junit.Ignore;
import org.junit.Test;

import java.io.InputStream;
import java.util.Scanner;

public class JsCodeGeneratorTest extends BaseJsCodeGeneratorTest {
    @Test
    @Ignore
    public void testFoo() throws Exception {
        InputStream stream = getClass().getResourceAsStream("/reactive.lang");
        Scanner scanner = new Scanner(stream).useDelimiter("\\A");
        String src = scanner.next();
        stream.close();
        String code = generateJsCode(src);
        System.out.println(code);
    }
}
