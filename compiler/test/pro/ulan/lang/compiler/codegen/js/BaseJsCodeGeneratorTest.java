package pro.ulan.lang.compiler.codegen.js;

import pro.ulan.lang.compiler.ast.File;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.typechecking.BaseTypeCheckerTest;

abstract class BaseJsCodeGeneratorTest extends BaseTypeCheckerTest {
    public static final JsCodeGenerator generator = new JsCodeGenerator();
    String generateJsCode(String src) {
        File file = parseSuccess(Parsers.file, src);
        File file1 = runTyper(Parsers.file, src);
        return generator.generateJsCode(file1);
    }
}
