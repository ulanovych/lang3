package pro.ulan.lang.compiler;

import org.junit.Test;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;

public class CompilationErrorTest extends BaseCompilerTest {
    @Test
    public void getMessageWithCodeLine() throws Exception {
        Token token = new Token("42", TokenType.Identifier, 2, 17);
        CompilationError error = new CompilationError(token, token) {
            @Override
            public String getMessage() {
                return "expected String, got Int";
            }
        };

        expect(error.getMessageWithCodeLine("\nval x: String = 42")).toBe(null
            , "expected String, got Int"
            , "2: val x: String = 42"
            , "                   ~~"
        );
    }
}