package pro.ulan.lang.compiler.lexer;

import pro.ulan.lang.compiler.BaseCompilerTest;

public abstract class BaseLexerTest extends BaseCompilerTest {
    private final Lexer lexer = new LexerImpl();

    protected Token[] tokenize(String s) {
        return lexer.tokenize(s);
    }

    public static TokenExpectationBuilder expect(Token actualToken) {
        return new TokenExpectationBuilder(actualToken);
    }
}
