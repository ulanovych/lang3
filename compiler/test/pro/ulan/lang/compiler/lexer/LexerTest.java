package pro.ulan.lang.compiler.lexer;

import org.junit.Test;

public class LexerTest extends BaseLexerTest {
    @Test
    public void test() {
        Token[] tokens = tokenize("data User(\n    id: Int,\n    name: String\n)");

        expect(tokens[0]).toBe("data", TokenType.Identifier);
        expect(tokens[1]).toBe("User", TokenType.Identifier);
        expect(tokens[2]).toBe("(", TokenType.OpenRoundBracket);
        expect(tokens[3]).toBe("id", TokenType.Identifier);
        expect(tokens[4]).toBe(":", TokenType.Colon);
        expect(tokens[5]).toBe("Int", TokenType.Identifier);
        expect(tokens[6]).toBe(",", TokenType.Comma);
        expect(tokens[7]).toBe("name", TokenType.Identifier);
        expect(tokens[8]).toBe(":", TokenType.Colon);
        expect(tokens[9]).toBe("String", TokenType.Identifier);
        expect(tokens[10]).toBe(")", TokenType.CloseRoundBracket);
        expect(tokens[11]).toBe("\0", TokenType.Eof);
    }

    @Test
    public void identifierInBackticks() {
        Token[] tokens = tokenize("fun `<=>`");

        expect(tokens[0]).toBe("fun", TokenType.Identifier);
        expect(tokens[1]).toBe("<=>", TokenType.Identifier);
        expect(tokens[2]).toBe("\0", TokenType.Eof);
    }

    @Test public void objLit() {
        Token[] tokens = tokenize("{host: \"localhost\", port: 8080}");

        expect(tokens[0]).toBe("{", TokenType.OpenCurlyBracket);
        expect(tokens[1]).toBe("host", TokenType.Identifier);
        expect(tokens[2]).toBe(":", TokenType.Colon);
        expect(tokens[3]).toBe("localhost", TokenType.StrLit);
        expect(tokens[4]).toBe(",", TokenType.Comma);
        expect(tokens[5]).toBe("port", TokenType.Identifier);
        expect(tokens[6]).toBe(":", TokenType.Colon);
        expect(tokens[7]).toBe("8080", TokenType.IntLit);
        expect(tokens[8]).toBe("}", TokenType.CloseCurlyBracket);
    }

   @Test public void emptyString() {
        Token[] tokens = tokenize("x = \"\"");

        expect(tokens[0]).toBe("x", TokenType.Identifier);
        expect(tokens[1]).toBe("=", TokenType.Assign);
        expect(tokens[2]).toBe("", TokenType.StrLit);
    }

   @Test public void or() {
        Token[] tokens = tokenize("|");

        expect(tokens[0]).toBe("|", TokenType.VertBar);
    }

    @Test
    public void genericClass() {
        Token[] tokens = tokenize("class Foo<T>(v: T)");

        expect(tokens[0]).toBe("class", TokenType.Identifier);
        expect(tokens[1]).toBe("Foo", TokenType.Identifier);
        expect(tokens[2]).toBe("<", TokenType.Lt);
        expect(tokens[3]).toBe("T", TokenType.Identifier);
        expect(tokens[4]).toBe(">", TokenType.Gt);
        expect(tokens[5]).toBe("(", TokenType.OpenRoundBracket);
        expect(tokens[6]).toBe("v", TokenType.Identifier);
        expect(tokens[7]).toBe(":", TokenType.Colon);
        expect(tokens[8]).toBe("T", TokenType.Identifier);
        expect(tokens[9]).toBe(")", TokenType.CloseRoundBracket);
    }
}