package pro.ulan.lang.compiler.lexer;

import static org.junit.Assert.assertEquals;

public class TokenExpectationBuilder {
    private final Token actualToken;

    public TokenExpectationBuilder(Token actualToken) {
        this.actualToken = actualToken;
    }

    public void toBe(Token expectedToken) {
        assertEquals(expectedToken, actualToken);
    }

    public void toBe(String expectedValue, TokenType expectedType) {
        assertEquals(expectedValue, actualToken.value);
        assertEquals(expectedType, actualToken.type);
    }
}
