package pro.ulan.lang.compiler.typechecking;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.ClassDef;
import pro.ulan.lang.compiler.ast.Scope;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.util.Tuple2;
import pro.ulan.lang.meta.Class;

public class ClassDefTypeCheckerTest extends BaseTypeCheckerTest {
    @Test
    public void inferMethodReturnType() {
        Tuple2<ClassDef, Scope> tuple = runTyperAndReturnScope(Parsers.classDef
            , "class Foo {"
            , "    fun f() = 42"
            , "}"
        );
        Scope scope = tuple._2;
        Class klass = (Class) scope.get("Foo");
        expect(klass.getName()).toBe("Foo");
        expect(klass.methods.get(0).name).toBe("f");
        expect(klass.methods.get(0).returnType.getName()).toBe("Int");
    }

    @Test
    public void methodAccessingThisMembers() {
        Tuple2<ClassDef, Scope> tuple = runTyperAndReturnScope(Parsers.classDef
            , "class Foo {"
            , "    val i = 42"
            , "    fun f() = this.i"
            , "}"
        );
        Scope scope = tuple._2;
        Class klass = (Class) scope.get("Foo");
        expect(klass.getName()).toBe("Foo");
        expect(klass.fields.get(0).name).toBe("i");
        expect(klass.fields.get(0).type.getName()).toBe("Int");
        expect(klass.methods.get(0).name).toBe("f");
        expect(klass.methods.get(0).returnType.getName()).toBe("Int");
    }
}
