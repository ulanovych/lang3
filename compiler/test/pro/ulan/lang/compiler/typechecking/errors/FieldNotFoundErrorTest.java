package pro.ulan.lang.compiler.typechecking.errors;

import org.junit.Test;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;
import pro.ulan.lang.meta.StructuralType;

import java.util.HashMap;
import java.util.List;

public class FieldNotFoundErrorTest extends BaseTypeCheckerErrorTest {
    @Test
    public void objLit() {
        List<TypeCheckerError> errors = typeCheckForErrors("{}.foo");
        Token token = new Token("foo", TokenType.Identifier, 1, 4);
        StructuralType type = new StructuralType(new HashMap<>());
        expect(errors.get(0)).toBe(new FieldNotFoundError(type, token));
    }
}
