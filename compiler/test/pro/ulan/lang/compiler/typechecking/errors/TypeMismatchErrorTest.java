package pro.ulan.lang.compiler.typechecking.errors;

import org.junit.Test;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;
import pro.ulan.lang.meta.Class;

import java.util.List;

public class TypeMismatchErrorTest extends BaseTypeCheckerErrorTest {
    @Test
    public void valDef() throws Exception {
        List<TypeCheckerError> errors = typeCheckForErrors("val s: String = 42");
        Token token = new Token("42", TokenType.IntLit, 1, 17);
        TypeMismatchError expectedError = new TypeMismatchError(token, new Class("String"), new Class("Int"));
        expect(errors.get(0)).toBe(expectedError);
    }

    @Test
    public void setVar() throws Exception {
        List<TypeCheckerError> errors = typeCheckForErrors("val i: Int = 42 i = \"s\"");
        Token token = new Token("s", TokenType.StrLit, 1, 22);
        TypeMismatchError expectedError = new TypeMismatchError(token, new Class("Int"), new Class("String"));
        expect(errors.get(0)).toBe(expectedError);
    }

    @Test
    public void funDef() throws Exception {
        List<TypeCheckerError> errors = typeCheckForErrors("fun f(): String = 42");
        Token token = new Token("42", TokenType.IntLit, 1, 19);
        TypeMismatchError expectedError = new TypeMismatchError(token, new Class("String"), new Class("Int"));
        expect(errors.get(0)).toBe(expectedError);
    }
}
