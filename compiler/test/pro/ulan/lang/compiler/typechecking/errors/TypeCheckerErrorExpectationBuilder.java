package pro.ulan.lang.compiler.typechecking.errors;

import pro.ulan.lang.compiler.typechecking.TypeCheckerError;

import static org.junit.Assert.assertEquals;

public class TypeCheckerErrorExpectationBuilder {
    public final TypeCheckerError actualError;

    public TypeCheckerErrorExpectationBuilder(TypeCheckerError actualError) {
        this.actualError = actualError;
    }

    public void toBe(TypeCheckerError expectedError) {
        assertEquals(expectedError.firstToken, actualError.firstToken);
        assertEquals(expectedError.lastToken, actualError.lastToken);
        assertEquals(expectedError.getMessage(), actualError.getMessage());
    }
}
