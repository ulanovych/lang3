package pro.ulan.lang.compiler.typechecking.errors;

import pro.ulan.lang.compiler.ast.File;
import pro.ulan.lang.compiler.ast.Scope;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.typechecking.BaseTypeCheckerTest;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;
import pro.ulan.lang.compiler.util.UnreachableException;

import java.util.List;

import static org.junit.Assert.fail;

public class BaseTypeCheckerErrorTest extends BaseTypeCheckerTest {
}
