package pro.ulan.lang.compiler.typechecking.errors;

import org.junit.Test;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;

import java.util.List;

public class VarAlreadyDefinedErrorTest extends BaseTypeCheckerErrorTest {
    @Test
    public void duplicateValDef() throws Exception {
        List<TypeCheckerError> errors = typeCheckForErrors("val i = 42 val i = 43");
        Token token = new Token("i", TokenType.Identifier, 1, 16);
        TypeCheckerError expectedError = new TypeCheckerError(token, "value i already present in scope");
        expect(errors.get(0)).toBe(expectedError);
    }
}
