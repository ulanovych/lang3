package pro.ulan.lang.compiler.typechecking.errors;

import org.junit.Test;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.typechecking.TypeCheckerError;

import java.util.List;

public class VariableNotFoundErrorTest extends BaseTypeCheckerErrorTest {
    @Test
    public void test() throws Exception {
        List<TypeCheckerError> errors = typeCheckForErrors("foo");
        Token token = new Token("foo", TokenType.Identifier, 1, 1);
        expect(errors.get(0)).toBe(new VariableNotFoundError(token));
    }

    @Test
    public void setMissingVariabe() throws Exception {
        List<TypeCheckerError> errors = typeCheckForErrors("x = 42");
    }
}
