package pro.ulan.lang.compiler.typechecking;

import pro.ulan.lang.compiler.TestUtil;
import pro.ulan.lang.compiler.ast.File;
import pro.ulan.lang.compiler.ast.Node;
import pro.ulan.lang.compiler.ast.Scope;
import pro.ulan.lang.compiler.lexer.Lexer;
import pro.ulan.lang.compiler.lexer.LexerImpl;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.parser.BaseParserTest;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.Parser;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.typechecking.errors.TypeCheckerErrorExpectationBuilder;
import pro.ulan.lang.compiler.util.Tuple2;
import pro.ulan.lang.compiler.util.UnreachableException;
import pro.ulan.lang.meta.Type;

import java.util.List;

import static org.junit.Assert.fail;

public abstract class BaseTypeCheckerTest extends BaseParserTest {
    protected static final TypeChecker typeChecker = new TypeCheckerImpl();
    protected static Scope stdlibScope = null;

    static {
        Lexer lexer = new LexerImpl();
        Token[] tokens = lexer.tokenize(TestUtil.stdlib);
        ParseResult<File> result = Parsers.file.parse(tokens, 0);
        File file = result.getValue();
        Scope scope = new Scope(null);
        typeChecker.typeCheck(file, scope);
        BaseTypeCheckerTest.stdlibScope = scope;
    }

    protected <T extends Node> T runTyper(Parser<T> parser, String src) {
        return runTyperAndReturnScope(parser, src)._1;
    }

    protected <T extends Node> Tuple2<T, Scope> runTyperAndReturnScope(Parser<T> parser, String src) {
        ParseResult<T> result = parse(parser, src);
        T node = result.getValue();
        Scope scope = new Scope(BaseTypeCheckerTest.stdlibScope);
        List<TypeCheckerError> errors = typeChecker.typeCheck(node, scope);
        if (!errors.isEmpty()) {
            errors.forEach(e -> System.out.println(e.getMessageWithCodeLine(src)));
            fail("there were type checker errors");
        }
        return new Tuple2<>(node, scope);
    }

    protected <T extends Node> Tuple2<T, Scope> runTyperAndReturnScope(Parser<T> parser, String... lines) {
        return runTyperAndReturnScope(parser, String.join("\n", lines));
    }

    protected <T extends Node> T runTyper(Parser<T> parser, String... lines) {
        return runTyper(parser, String.join("\n", lines));
    }

    protected List<TypeCheckerError> typeCheckForErrors(String src) {
        Token[] tokens = tokenize(src);
        ParseResult<File> result = Parsers.file.parse(tokens, 0);
        File file = result.getValue();
        List<TypeCheckerError> errors = typeChecker.typeCheck(file, new Scope(stdlibScope));
        if (!errors.isEmpty()) {
            return errors;
        }
        fail("errors were expected");
        throw new UnreachableException();
    }

    public static TypeExpectationBuilder expect(Type actualType) {
        return new TypeExpectationBuilder(actualType);
    }

    public static TypeCheckerErrorExpectationBuilder expect(TypeCheckerError actualError) {
        return new TypeCheckerErrorExpectationBuilder(actualError);
    }
}