package pro.ulan.lang.compiler.typechecking;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.Expr;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.meta.StructuralType;

public class ObjLitTypeTest extends BaseTypeCheckerTest {
    @Test
    public void objLitEmpty() {
        Expr expr = runTyper(Parsers.expr, "{}");
        StructuralType type = (StructuralType) expr.getTypeOrThrow();
        expect(type.getName()).toBe("{}");
    }

    @Test
    public void objLitOneField() {
        Expr expr = runTyper(Parsers.expr, "{port = 8080}");
        StructuralType type = (StructuralType) expr.getTypeOrThrow();
        expect(type.getName()).toBe("{port: Int}");
    }

    @Test
    public void objLitFewFields() {
        Expr expr = runTyper(Parsers.expr, "{host = \"localhost\", port = 8080}");
        StructuralType type = (StructuralType) expr.getTypeOrThrow();
        expect(type.getName()).toBe("{port: Int, host: String}");
    }
}
