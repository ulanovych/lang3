package pro.ulan.lang.compiler.typechecking;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.meta.Type;

import static org.junit.Assert.assertEquals;

public class TypeCheckerTest extends BaseTypeCheckerTest {
    @Test
    public void intLit() {
        Expr expr = runTyper(Parsers.expr, "42");
        IntLit lit = (IntLit) expr;
        lit.getTypeOrThrow();
    }

    @Test
    public void inferValType() {
        ValDef def = runTyper(Parsers.valDef, "val x = 42");
        Type type = def.getTypeOrThrow();
        assertEquals("Int", type.getName());
    }

    @Test
    public void inferFunctionResultType() {
        FunDef funDef = runTyper(Parsers.funDef, "fun f() = 42");
        Type type = funDef.getTypeOrThrow();
        assertEquals("Int", type.getName());
    }
}
