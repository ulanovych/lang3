package pro.ulan.lang.compiler.typechecking;

import pro.ulan.lang.meta.Class;
import pro.ulan.lang.meta.Type;

import static org.junit.Assert.assertEquals;

public class TypeExpectationBuilder {
    private final Type actualType;

    public TypeExpectationBuilder(Type actualType) {
        this.actualType = actualType;
    }


    public void toHaveName(String expectedName) {
        assertEquals(expectedName, actualType.getName());
    }
}
