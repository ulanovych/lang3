package pro.ulan.lang.compiler.typechecking;

import org.junit.Test;
import pro.ulan.lang.compiler.ast.File;
import pro.ulan.lang.compiler.ast.FunDef;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.typechecking.errors.TypeMismatchError;
import pro.ulan.lang.meta.Class;
import pro.ulan.lang.meta.TypeVar;

import java.util.List;

public class GenericFunTypeCheckerTest extends BaseTypeCheckerTest {
    @Test
    public void genericFunDefReturnTypeInference() {
        FunDef funDef = runTyper(Parsers.funDef
            , "fun f<T>(x: T) = x"
        );
        expect(funDef.getTypeOrNull()).toHaveName("T");
    }

    @Test
    public void genericFunDefReturnTypeMismatch() {
        List<TypeCheckerError> errors = typeCheckForErrors("fun f<T>(): T = 42");
        Token token = new Token("42", TokenType.IntLit, 1, 17);
        expect(errors.get(0)).toBe(new TypeMismatchError(token, new TypeVar("T"), new Class("Int")));
    }

    @Test
    public void genericFunCallTypeInference() {
        File file = runTyper(Parsers.file
            , "fun f<T>(x: T) = x"
            , "f(42)"
        );

        expect(file.stmts.get(1).getTypeOrNull()).toHaveName("Int");
    }
}
