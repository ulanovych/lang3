package pro.ulan.lang.compiler;

import static org.junit.Assert.assertEquals;

public class StringExpectationBuilder {
    public final String actual;

    public StringExpectationBuilder(String str) {
        this.actual = str;
    }

    public void toBe(Void dummy, String... expected) {
        assertEquals(String.join("\n", expected), actual);
    }

    public void toBe(String expected) {
        assertEquals(expected, actual);
    }
}