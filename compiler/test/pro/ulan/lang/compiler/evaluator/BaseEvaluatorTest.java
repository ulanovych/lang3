package pro.ulan.lang.compiler.evaluator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import pro.ulan.lang.compiler.TestUtil;
import pro.ulan.lang.compiler.ast.*;
import pro.ulan.lang.compiler.lexer.Lexer;
import pro.ulan.lang.compiler.lexer.LexerImpl;
import pro.ulan.lang.compiler.lexer.Token;
import pro.ulan.lang.compiler.lexer.TokenType;
import pro.ulan.lang.compiler.parser.ParseResult;
import pro.ulan.lang.compiler.parser.Parsers;
import pro.ulan.lang.compiler.typechecking.TypeCheckingVisitor;


public class BaseEvaluatorTest {
    private static final Lexer lexer = new LexerImpl();
    private static final TypeCheckingVisitor typer = new TypeCheckingVisitor();
    private static Scope scope = null;

    static {
        Token[] tokens = lexer.tokenize(TestUtil.stdlib);
        ParseResult<File> result = Parsers.file.parse(tokens, 0);
        File file = result.getValue();
        Scope scope = new Scope(null);
        typer.visit(file, scope);
        typer.errors.forEach(e -> System.out.println(e.getMessageWithCodeLine(TestUtil.stdlib)));
        BaseEvaluatorTest.scope = scope;
    }

    Object evaluate(String src) {
        Lexer lexer = new LexerImpl();
        Token[] tokens = lexer.tokenize(src);
        ParseResult<File> result = Parsers.file.parse(tokens, 0);
        if (tokens[result.getIndex()].type != TokenType.Eof) {
            fail("parsing unexpectedly stoped at " + tokens[result.getIndex()]);
        }
        File file = result.getValue();
        Scope scope1 = new Scope(BaseEvaluatorTest.scope);
        TypeCheckingVisitor typer = new TypeCheckingVisitor();
        typer.visit(file, scope1);
        Evaluator evaluator = new Evaluator();
        return evaluator.evaluate(file, scope1);
    }

//    Object evaluate(String src) {
//        Lexer lexer = new LexerImpl();
//        Token[] tokens = lexer.tokenize(stdlib);
//        File file = Parsers.file.parse(tokens, 0).getValue();
//        Scope stdlibScope = new Scope(null);
//        file.setScope(stdlibScope);
//        TypeCheckingVisitor typer = new TypeCheckingVisitor();
//        typer.visit(file);
//        tokens = lexer.tokenize(src);
//        file = Parsers.file.parse(tokens, 0).getValue();
//        file.setScope(stdlibScope);
//        typer = new TypeCheckingVisitor();
//        typer.visit(file);
//        if (!typer.errors.isEmpty()) {
//            String[] lines = src.split("\n");
//            typer.errors.forEach(e -> {
//                System.out.println(e.msg);
//                System.out.println(lines[e.token.row]);
//                System.out.print(new String(new char[e.token.column]).replace('\0', ' '));
//                System.out.println("^\n\n");
//            });
//            Assert.fail("there were typer errors");
//        }
//        Evaluator evaluator = new Evaluator();
//        return evaluator.evaluate(file, stdlibScope);
//    }
//
    void test(Object expected, String... lines) {
        Object result = evaluate(String.join("\n", lines));
        assertEquals(expected, result);
    }
}
