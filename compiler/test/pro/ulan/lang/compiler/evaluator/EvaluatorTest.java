package pro.ulan.lang.compiler.evaluator;

import org.junit.Ignore;
import org.junit.Test;
import pro.ulan.lang.compiler.ast.Scope;

import static org.junit.Assert.assertEquals;

public class EvaluatorTest extends BaseEvaluatorTest {
    @Test
    public void intLit() {
        assertEquals(42, evaluate("42"));
    }

    @Test
    public void strLit() {
        assertEquals("John", evaluate("\"John\""));
    }

    @Test
    public void objLit() {
        Scope obj = (Scope) evaluate("{host = \"localhost\", port = 8080}");
        assertEquals("localhost", obj.get("host"));
        assertEquals(8080, obj.get("port"));
    }

    @Test
    public void stringConcat() {
        assertEquals("Hello world!", evaluate("\"Hello \" + \"world!\""));
    }

    @Test
    public void intArithmSimple() {
        assertEquals(4, evaluate("2 + 2"));
    }

    @Test
    public void intArithmComplex() {
        assertEquals(6, evaluate("2 + 2 * 2"));
    }

    @Test
    public void localVars() {
        assertEquals(42, evaluate("val x = 42 x"));
    }

    @Test
    public void objLitFieldAccess() {
        assertEquals(8080, evaluate("{port = 8080}.port"));
    }

    @Test
    public void assign() {
        assertEquals(43, evaluate("val x = 42 x = 43 x"));
    }

    @Ignore
    @Test public void classAssignment() {
        test(null, "val Foo = Int\n val i: Foo = 42");
    }

    @Ignore
    @Test public void inferFunctionResultType() {
        test(null, "fun F(T: Type) = class F {}");
    }

    @Ignore
    @Test public void dataClass() {
//        test(42, "class User(id: Int, name: String) val user = User(42, \"John\") 42");
    }
}
