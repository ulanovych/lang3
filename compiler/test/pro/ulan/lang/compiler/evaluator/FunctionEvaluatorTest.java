package pro.ulan.lang.compiler.evaluator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FunctionEvaluatorTest extends BaseEvaluatorTest {
    @Test
    public void constFunCall() throws Exception {
        assertEquals(42, evaluate("fun f() = 42 f()"));
    }

    @Test
    public void identityFunCall() throws Exception {
        assertEquals(42, evaluate("fun f(x: Int) = x f(42)"));
    }

    @Test
    public void sumFunCall() throws Exception {
        assertEquals(42, evaluate("fun f(x: Int, y: Int) = x + y f(40, 2)"));
    }

    @Test
    public void factorialFunCall() {
        test(24, "fun f(x: Int): Int = x == 1 ? 1 : x * f(x - 1) f(4)");
    }

    @Test
    public void fibonacci() {
        test(8, "fun f(x: Int): Int = x == 1 ? 1 : (x == 2 ? 1 : (f(x - 1) + f(x - 2))) f(6)");
    }

    @Test
    public void funWithLocalVars() throws Exception {
        test(42, "fun f() {val i = 42 i} f()");
    }

    @Test
    public void funReturnsGlobalVar() throws Exception {
        test(42
            , "val i = 42"
            , "fun f() = i"
            , "f()"
        );
    }

    @Test
    public void funSetsGlobalVar() throws Exception {
        test(43
            , "val x = 42"
            , "fun f(y: Int) = x = y"
            , "f(43)"
            , "x"
        );
    }
}
