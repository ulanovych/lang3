package pro.ulan.lang.compiler.semanticanalysis

import org.scalatest.FunSuite
import pro.ulan.lang.compiler.ast.{GetField, Ident, MethodCall, StrLit}
import pro.ulan.lang.compiler.classpathanalysis.{AnalyzedClasspath, Pkg}

class TypeCheckerTest extends FunSuite {
  test("it") {
    // java.lang.System.out.println("Hello, World!")
    val ast = MethodCall(
      GetField(
        GetField(
          GetField(
            Ident("java"),
            "lang"
          ),
          "system"
        ),
        "out"
      ),
      "println", List(StrLit(""))
    )

    val cp = AnalyzedClasspath(List(Pkg("java", List(Pkg("lang", List(), List())), List())))
    typeCheck(ast, cp)
  }
}
