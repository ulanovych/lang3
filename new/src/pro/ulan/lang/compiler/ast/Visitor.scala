package pro.ulan.lang.compiler.ast

trait Visitor {
  def continue(node: Node) = ???

  def visit(node: GetField): Unit
  def visit(node: Ident): Unit
  def visit(node: MethodCall): Unit
  def visit(node: StrLit): Unit
}