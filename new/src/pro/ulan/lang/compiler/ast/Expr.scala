package pro.ulan.lang.compiler.ast

/**
  * The base class for all AST nodes that result in a value during computation.
  * Such as calling functions and methods, reading fields and local variables and so on.
  * Unlike statements.
  */
trait Expr extends Node {

}
