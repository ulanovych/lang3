package pro.ulan.lang.compiler.ast

import pro.ulan.lang.compiler.lexicalanalysis.Token

case class Ident(token: Token) extends Expr

case class GetField(expr: Expr, field: Token) extends Expr


case class MethodCall(expr: Expr, method: Token, args: Seq[Expr]) extends Expr

case class StrLit(token: Token) extends Expr