package pro.ulan.lang.compiler.ast

import pro.ulan.lang.compiler.meta.Type

/**
  * Base class for all AST nodes.
  */
trait Node {
  var tpe: Option[Type] = None
}