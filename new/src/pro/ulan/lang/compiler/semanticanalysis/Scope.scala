package pro.ulan.lang.compiler.semanticanalysis

import scala.collection.mutable

trait Scope {
  def get(key: String): Seq[Any]
  def put(key: String, value: Any)
}

object Scope {
  def apply() = new Scope {
    private val map = mutable.HashMap[String, List[Any]]()

    override def get(key: String): Seq[Any] = {
      map.getOrElseUpdate(key, List())
    }

    override def put(key: String, value: Any): Unit = {
      val list = map.getOrElseUpdate(key, List())
      map.put(key, value :: list)
    }
  }
}
