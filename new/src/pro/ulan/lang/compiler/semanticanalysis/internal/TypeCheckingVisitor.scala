package pro.ulan.lang.compiler.semanticanalysis.internal

import pro.ulan.lang.compiler.ast._
import pro.ulan.lang.compiler.classpathanalysis.{AnalyzedClasspath, Pkg}
import pro.ulan.lang.compiler.semanticanalysis.Scope

class TypeCheckingVisitor(cp: AnalyzedClasspath) extends Visitor {
  val scope = Scope()
  cp.packages.foreach(p => scope.put(p.name, p))

  override def visit(node: GetField): Unit = {
    continue(node.expr)
    node.tpe match {
      case Some(p: Pkg) =>

    }
  }

  override def visit(node: Ident): Unit = {
    scope.get(node.token.value) match {
      case Seq(p: Pkg) =>
        node.tpe = Some(p)
    }
  }

  override def visit(node: MethodCall): Unit = {
    continue(node.expr)
    ???
  }

  override def visit(node: StrLit): Unit = ???
}
