package pro.ulan.lang.compiler.lexicalanalysis

/**
  * Minimal meaningful piece of source code like number, string, identifier, punctuation mark etc.
  * These are produced in the lexical analysis stage and consumed in the parsing stage.
  */
case class Token(value: String) {

}

object Token {
  /**
    * This was written to ease AST construction in tests,
    * e.g. `Ident("x")` instead of `Ident(Token("x"))`
    */
  implicit def stringToToken(token: String): Token = Token(token)
}
