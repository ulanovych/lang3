package lang.compiler;

import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class SerializationBenchmark extends SerializationBenchmarkImpl {
    public static void main(String[] args) throws RunnerException {
        Options options = new OptionsBuilder()
            .include(SerializationBenchmark.class.getSimpleName())
            .forks(1)
            .warmupIterations(10)
            .measurementIterations(10)
            .mode(Mode.AverageTime)
            .build();
        new Runner(options).run();
    }
}
