package lang.compiler

import java.io._

import com.esotericsoftware.kryo.Kryo
import com.esotericsoftware.kryo.io.{Input, Output}
import lang.compiler.classpathanalysis.ClasspathAnalyzer
import lang.compiler.classpathanalysis.caching.NoCache
import lang.compiler.typechecking.TestClasspathHolder
import lang.compiler.typechecking.types.TPackage
import org.nustaq.serialization.FSTConfiguration
import org.openjdk.jmh.annotations.{Benchmark, Scope, Setup, State}
import org.openjdk.jmh.runner.Runner
import org.openjdk.jmh.runner.options.OptionsBuilder

@State(Scope.Benchmark)
class SerializationBenchmarkImpl {
  var pkg: TPackage = _
  var javaBytes: Array[Byte] = _
  var fstBytes: Array[Byte] = _
  var kryoBytes: Array[Byte] = _
  var conf: FSTConfiguration = _
  var kryo: Kryo = _

  @Setup
  def setup(): Unit = {
    val javaHome = System.getProperty("java.home")

    val rtJar = new File(javaHome, "lib/rt.jar")
    val jceJar = new File(javaHome, "lib/jce.jar")

    pkg = TestClasspathHolder.path

    {
      val baos = new ByteArrayOutputStream()
      val oos = new ObjectOutputStream(baos)
      oos.writeObject(pkg)
      oos.close()
      javaBytes = baos.toByteArray
      baos.close()
    }

    {
      conf = FSTConfiguration.createDefaultConfiguration()
      fstBytes = conf.asByteArray(pkg)
    }

    {
      kryo = new Kryo()
      val baos = new ByteArrayOutputStream()
      val output = new Output(baos)
      kryo.writeObject(output, pkg)
      output.close()
      kryoBytes = baos.toByteArray
      baos.close()
    }
  }

  @Benchmark
  def serJava(): Any = {
    val baos = new ByteArrayOutputStream()
    val oos = new ObjectOutputStream(baos)
    oos.writeObject(pkg)
    oos.close()
    val result = baos.toByteArray
    baos.close()
    result
  }

  @Benchmark
  def serFst(): Any = {
    conf.asByteArray(pkg)
  }

//  @Benchmark
//  def serKryo(): Any = {
//    val baos = new ByteArrayOutputStream()
//    val output = new Output(baos)
//    kryo.writeObject(output, pkg)
//    output.close()
//    val bytes = baos.toByteArray
//    baos.close()
//    bytes
//  }

  @Benchmark
  def deserJava(): Any = {
    val bais = new ByteArrayInputStream(javaBytes)
    val ois = new ObjectInputStream(bais)
    val result = ois.readObject()
    ois.close()
    bais.close()
    result
  }

  @Benchmark
  def deserFst(): Any = {
    conf.asObject(fstBytes)
  }

//  @Benchmark
//  def deserKryo(): Any = {
//    val input = new Input(kryoBytes)
//    val obj = kryo.readObject(input, classOf[TPackage])
//    input.close()
//    obj
//  }
}