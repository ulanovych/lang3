package lang.compiler

import lang.compiler.util.measureTime
import net.openhft.compiler.CompilerUtils
import org.scalatest.FunSuite

class MiscTest extends FunSuite {
  test("speed of java compile") {
    val (_, time1) = measureTime {
      CompilerUtils.CACHED_COMPILER.loadFromJava("com.example.Class", """
        package com.example;
        public class Class {
          public static void main(String[] args) {
            System.out.println("Hello, world!");
          }
        };
      """)
    }
    val (_, time2) = measureTime {
      CompilerUtils.CACHED_COMPILER.loadFromJava("com.example.Class2", """
        package com.example;
        public class Class2 {
          public static void main(String[] args) {
            System.out.println("Hello, world!");
          }
        };
      """)
    }
    println(time1)
    println(time2)
  }

  test("speed of asm bytecode generation") {
    import org.objectweb.asm.ClassWriter
    import org.objectweb.asm.Opcodes._
    val (_, time) = measureTime {
      val cw = new ClassWriter(0)
      cw.visit(V1_5, ACC_PUBLIC + ACC_ABSTRACT + ACC_INTERFACE, "pkg/Comparable", null, "java/lang/Object", null)
      cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "LESS", "I", null, new Integer(-1)).visitEnd()
      cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "EQUAL", "I", null, new Integer(0)).visitEnd()
      cw.visitField(ACC_PUBLIC + ACC_FINAL + ACC_STATIC, "GREATER", "I", null, new Integer(1)).visitEnd()
      cw.visitMethod(ACC_PUBLIC + ACC_ABSTRACT, "compareTo", "(Ljava/lang/Object;)I", null, null).visitEnd()
      cw.visitEnd()
      val bytes = cw.toByteArray
      class Loader extends ClassLoader {
        override def loadClass(name: String): Class[_] = {
          name match {
            case "pkg.Comparable" =>
              defineClass("pkg.Comparable", bytes, 0, bytes.length)
            case _ => super.loadClass(name)
          }

        }
      }
      val loader = new Loader
      loader.loadClass("pkg.Comparable")
    }
    println(time)
  }
}
