package lang.compiler.interpretation

import lang.compiler.MyMatchers
import org.scalatest.FunSuiteLike

class InterpreterTest extends BaseInterpreterTest with FunSuiteLike with MyMatchers {
  test("fq hello world") {
    val (_, output) = interpret("""java.lang.System.out.println("Hello world!")""")
    output shouldBe s"Hello world!${System.lineSeparator()}"
  }

  test("hello world with wildcard import") {
    val (_, output) = interpret("""
      import java.lang.*
      System.out.println("Hello world!")
    """)
    output shouldBe s"Hello world!${System.lineSeparator()}"
  }
}
