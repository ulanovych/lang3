package lang.compiler.interpretation

import java.io.{ByteArrayOutputStream, PrintStream}

import lang.compiler.typechecking.BaseTypeCheckerTest

trait BaseInterpreterTest extends BaseTypeCheckerTest {
  def interpret(src: String): (Any, String) = {
    val expr = typeCheck(src)
    val baos = new ByteArrayOutputStream()
    val ps = new PrintStream(baos)
    val out = System.out
    System.setOut(ps)
    val result = try {
      Interpreter.interpret(expr)
    } finally {
      System.setOut(out)
    }

    ps.close()
    (result, new String(baos.toByteArray, "UTF-8"))
  }
}
