package lang.compiler

import org.scalatest.exceptions.TestFailedException

import scala.collection.SeqLike
import org.scalatest.{Matchers => ScalatestMatchers}

trait MyMatchers {
  implicit class ObjectExpectationBuilder[T](actual: T) {
    def shouldBe(expected: T): Unit = {
      if (actual != expected) {
        throw new TestFailedException(s"Expected $expected, got $actual", 1)
      }
    }
  }

  implicit class StringExpectationBuilder(actual: String) {
    def shouldBe(expected: String): Unit = {
      if (actual != expected) {
        throw new TestFailedException(s"""Expected "$expected", got "$actual"""", 1)
      }
    }
  }

  implicit class CollectionExpectationBuilder[T](xs: Seq[T]) {
    def shouldContain(f: T => Boolean) = {
      if (!xs.exists(f)) {
        throw new TestFailedException("Predicate returned false for all in $xs", 1)
      }
    }
    def shouldContain(x: T) = {
      if (!xs.contains(x)) {
        throw new TestFailedException(s"$x was not found in $xs", 1)
      }
    }
  }
}
