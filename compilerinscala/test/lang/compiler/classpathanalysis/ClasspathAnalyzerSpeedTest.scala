package lang.compiler.classpathanalysis

import java.io._
import java.util.jar.JarFile

import lang.compiler
import lang.compiler.classpathanalysis.caching.{CacheImpl, NoCache}
import lang.compiler.typechecking.TestClasspathHolder
import lang.compiler.typechecking.types.TClass
import lang.compiler.util.measureTime
import org.apache.commons.io.IOUtils
import org.scalatest.{FunSpec, Ignore, Matchers}

@Ignore
class ClasspathAnalyzerSpeedTest extends FunSpec with compiler.MyMatchers {
  val javaHome = System.getProperty("java.home")
  val rtJar = new File(javaHome, "lib/rt.jar")
  val jceJar = new File(javaHome, "lib/jce.jar")

  it("scan time") {
//    Thread.sleep(60 * 1000)
    val (_, time) = measureTime {
      ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), NoCache)
    }
    Thread.sleep(60*1000)
    println(s"Scan time: $time")
  }

  it("serialization time") {
    val path = TestClasspathHolder.path
    val (_, time) = measureTime {
      val baos = new ByteArrayOutputStream()
      val stream = new ObjectOutputStream(baos)
      stream.writeObject(path)
      stream.close()
    }
    println(s"Serialization time: $time")
  }

  it("deserialization time") {
    val baos = new ByteArrayOutputStream()
    val stream = new ObjectOutputStream(baos)
    stream.writeObject(TestClasspathHolder.path)
    stream.close()
    val (_, time) = measureTime {
      val bais = new ByteArrayInputStream(baos.toByteArray)
      val stream = new ObjectInputStream(bais)
//      Thread.sleep(30000)
      stream.readObject()
      stream.close()
    }
    println(s"Deserialization time: $time")
  }

  it("count") {
    TestClasspathHolder.path
    println(s"TClass.counter: ${TClass.counter}")
  }

  it("read rt.jar into memory") {
    val (_, time) = measureTime {
      IOUtils.toByteArray(new FileInputStream(rtJar))
    }
    println(s"Read rt.jar into memory: $time")
  }

  it("unzip rt.jar into memory") {
    val (_, time) = measureTime {
      val jarFile = new JarFile(rtJar)
      val entries = jarFile.entries()
      while (entries.hasMoreElements) {
        val element = entries.nextElement()
        if (element.getName.endsWith(".class")) {
          val stream = jarFile.getInputStream(element)
          IOUtils.toByteArray(stream)
          stream.close()
        }
      }
    }
    println(s"unzip rt.jar into memory: $time")
  }

  it("scan rt jar folders") {
    val (_, time) = measureTime {
      val jar = new JarFile(rtJar)
      val entries = jar.entries()
      while (entries.hasMoreElements) {
        val entry = entries.nextElement()
        entry.getName
      }
    }
    println(s"scan rt jar folders: $time")
  }
  it("cache") {
//    val cache = new CacheImpl(9875)
//    val (_, time1) = measureTime {
//      ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), cache)
//    }
//    val (_, time2) = measureTime {
//      ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), cache)
//    }
//    val (_, time3) = measureTime {
//      ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), cache)
//    }
//    println(s"Time1: $time1, time2: $time2, time3: $time3")
  }
}

