package lang.compiler.classpathanalysis

import java.io.File

import lang.compiler
import lang.compiler.classpathanalysis.caching.{CacheImpl, NoCache}
import lang.compiler.typechecking.types._
import org.scalatest.{FlatSpec, FunSpec, FunSuite, Matchers}

class ClasspathAnalyzerTest extends FunSpec with compiler.MyMatchers {
  val javaHome = System.getProperty("java.home")
  val rtJar = new File(javaHome, "lib/rt.jar")
  val jceJar = new File(javaHome, "lib/jce.jar")
  val root = ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), NoCache)

  it("should read fields") {
    val string = root.getClassByFullName("java.lang.String").get
    string.getField("value") shouldBe Some(Field("value", TArray(TChar, 1)))
  }

  it("should read methods") {
    val String = root.getClassByFullName("java.lang.String").get
    val PrintStream = root.getClassByFullName("java.io.PrintStream").get
    PrintStream.getMethods("println") shouldContain(_.parameterTypes == Seq(String))
  }

  it ("should read static fields") {
    val string = root.getClassByFullName("java.lang.System").get
    val field = string.getStaticField("out").get
    field.`type`.asInstanceOf[TClass].fqName shouldBe "java.io.PrintStream"
  }
}

