package lang.compiler.classpathanalysis

import lang.compiler
import org.scalatest.{FunSpec, FunSuite, Matchers}

class ClassFileAnalyzerTest extends FunSpec with compiler.MyMatchers {
  describe("ClassFileAnalyzer should read") {
    val stringClass = ClasspathAnalyzer.analyzeClassFile(getClass.getResourceAsStream("/java/lang/String.class"))

    it("fields") {
      stringClass.fields shouldContain CAField("value", CAArrayType(CAChar, 1))
    }

    it("static fields") {
      val systemClass = ClasspathAnalyzer.analyzeClassFile(getClass.getResourceAsStream("/java/lang/System.class"))
      systemClass.staticFields shouldContain CAStaticField("out", CARef("java/io/PrintStream"))
    }
  }
}
