package lang.compiler

import org.junit.Assert._

class StringExpectationBuilder(val actual: String) {
  def toBe(dummy: Void, expected: String*) {
    assertEquals(expected.mkString("\n"), actual)
  }

  def toBe(expected: String) {
    assertEquals(expected, actual)
  }
}