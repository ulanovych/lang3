package lang.compiler.typechecking

import java.io.File

import lang.compiler.{BaseCompilerTest, CompilationError, RuntimeLangHolder}
import lang.compiler.classpathanalysis.ClasspathAnalyzer
import lang.compiler.classpathanalysis.caching.{CacheImpl, NoCache}
import lang.compiler.lexicalanalysis.Lexer
import lang.compiler.parsing.ParserSuccess
import lang.compiler.parsing.impl.Parsers
import lang.compiler.parsing.trees.Node
import lang.compiler.typechecking.types._
import org.scalatest.exceptions.TestFailedException

trait BaseTypeCheckerTest extends BaseCompilerTest {
  def typeCheck(src: String): Node = {
//    val java = new TPackage("java")
//
//    val lang = new TPackage("lang")
//    val io = new TPackage("io")
//
//    java.addPackage(lang)
//    java.addPackage(io)
//
//    val PrintStream = new TClass("PrintStream", "java.io.PrintStream")
//    val String = new TClass("String", "java.lang.String")
//    PrintStream.addMethod(Method("println", Void, Seq(String)))
//
//    val system = new TClass("System", "java.lang.System")
//    system.addStaticField(StaticField("out", PrintStream))
//    lang.addClass(system)
    val javaHome = System.getProperty("java.home")

    val rtJar = new File(javaHome, "lib/rt.jar")
    val jceJar = new File(javaHome, "lib/jce.jar")

    val pkg = ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), NoCache/*new CacheImpl(9865)*/)

    val tokens = Lexer.tokenize(src)
    val ParserSuccess(expr, _, _) = Parsers.file.parse(tokens, 0)
    val scope = new Scope
    scope.put("java", pkg.getPackage("java").get)
    expr.scope = scope
    val errors = TypeChecker.typeCheck(expr, pkg)
    if (errors.nonEmpty) {
      val s = errors.map(_.getMessageWithCodeLine(src)).mkString("\n\n")
      throw new TestFailedException(s"There were type errors: \n$s", 4)
    }
    expr
  }

  def typeCheck2(src: String): (lang.compiler.parsing.trees.File, Seq[CompilationError]) = {
    val runtimeTokens = Lexer.tokenize(RuntimeLangHolder.src)
    val ParserSuccess(runtimeFile, _, _) = Parsers.file.parse(runtimeTokens, 0)
    val tokens = Lexer.tokenize(src)
    val ParserSuccess(file, _, _) = Parsers.file.parse(tokens, 0)
    val visitor = new TypeCheckingVisitor2(new Scope2)
    visitor.continue(runtimeFile)
    visitor.continue(file)
    val errors = visitor.getErrors
    (file, errors)
  }

  def typeCheckClean(src: String): (lang.compiler.parsing.trees.File, Scope2, Seq[CompilationError]) = {
    val tokens = Lexer.tokenize(src)
    val ParserSuccess(file, _, _) = Parsers.file.parse(tokens, 0)
    val visitor = new TypeCheckingVisitor2(new Scope2)
    visitor.continue(file)
    val errors = visitor.getErrors
    val scope = visitor.getScope
    (file, scope, errors)
  }
}