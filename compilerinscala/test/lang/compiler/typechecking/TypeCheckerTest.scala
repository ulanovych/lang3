package lang.compiler.typechecking

import lang.compiler.parsing.trees._
import lang.compiler.typechecking.errors.TypeMismatch
import lang.compiler.typechecking.types.{TClass, Type}
import org.scalatest.{FunSuiteLike, Matchers}

class TypeCheckerTest extends BaseTypeCheckerTest with FunSuiteLike with Matchers {
  test("it") {
    typeCheck("""java.lang.System.out.println("Hello world!")""")
  }

  test("int lit") {
    val (file, errors) = typeCheck2("42")
    file.stmts(0).asInstanceOf[IntLit].`type`.asInstanceOf[TClass].fqName shouldBe "Int"
  }

  test("symbol not found") {
    val src = "x"
    val (_, errors) = typeCheck2(src)
    errors.foreach(e => println(e.getMessageWithCodeLine(src)))
  }

  test("symbol found") {
    val src = "val x = 42\nx"
    val (_, errors) = typeCheck2(src)
    errors.foreach(e => println(e.getMessageWithCodeLine(src)))
  }

  test("int sum") {
    val src = "2 + 2"
    val (file, errors) = typeCheck2(src)
    errors.foreach(e => println(e.getMessageWithCodeLine(src)))
  }

  test("must replace Ident with GetLocalVar") {
    val src = """
      class Int
      fun f(): Int {
        val i: Int = 1
        i
      }
    """
    val (file, scope, errors) = typeCheckClean(src)
    errors.size shouldBe 0
    file.stmts(1).asInstanceOf[FunDef].body.get.stmts.last match {
      case _: GetLocalVar =>
    }
  }

  test("val def type mismatch") {
    val src = """
      class X
      val x: X = 42"""
    val (file, scope, errors) = typeCheckClean(src)
    errors.size shouldBe 1
    val defn = file.stmts(2).asInstanceOf[ValDef]
    val xType = scope.get("X").get.asInstanceOf[Type]
    val intType = scope.get("Int").get.asInstanceOf[Type]
    errors(0) shouldBe TypeMismatch(xType, intType, defn.expr.get)
  }


  test("print errors") {
    val src = """
      class Int
      val x: Int* = 1
    """
    val (file, scope, errors) = typeCheckClean(src)
    errors.foreach(e => println(e.getMessageWithCodeLine(src)))
  }
}
