package lang.compiler.lexicalanalysis

import org.junit.Assert._

class TokenExpectationBuilder(actualToken: Token) {
  def toBe(expectedToken: Token): Unit = {
    assertEquals(expectedToken, actualToken)
  }

  def toBe(expectedValue: String, expectedType: TokenType): Unit = {
    assertEquals(expectedValue, actualToken.value)
    assertEquals(expectedType, actualToken.`type`)
  }
}
