package lang.compiler.lexicalanalysis

import lang.compiler.BaseCompilerTest
import lang.compiler.lexicalanalysis.impl.LexerImpl

trait BaseLexerTest extends BaseCompilerTest {
    def tokenize(src: String): Seq[Token] = {
        Lexer.tokenize(src)
    }

    def expect(actualToken: Token) = new TokenExpectationBuilder(actualToken)
}
