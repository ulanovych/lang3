package lang.compiler.lexicalanalysis

import org.scalatest.FunSuite

class LexerTest extends FunSuite with BaseLexerTest {
  test("method call one arg") {
    val tokens = tokenize(""" "FF".toInt(16)""")
    expect(tokens(0)).toBe("FF", TokenType.StrLit)
    expect(tokens(1)).toBe(".", TokenType.Dot)
    expect(tokens(2)).toBe("toInt", TokenType.Identifier)
    expect(tokens(3)).toBe("(", TokenType.OpenRoundBracket)
    expect(tokens(4)).toBe("16", TokenType.IntLit)
    expect(tokens(5)).toBe(")", TokenType.CloseRoundBracket)
  }

  test("test") {
    val tokens = tokenize("data User(\n    id: Int,\n    name: String\n)")
    expect(tokens(0)).toBe("data", TokenType.Identifier)
    expect(tokens(1)).toBe("User", TokenType.Identifier)
    expect(tokens(2)).toBe("(", TokenType.OpenRoundBracket)
    expect(tokens(3)).toBe("id", TokenType.Identifier)
    expect(tokens(4)).toBe(":", TokenType.Colon)
    expect(tokens(5)).toBe("Int", TokenType.Identifier)
    expect(tokens(6)).toBe(",", TokenType.Comma)
    expect(tokens(7)).toBe("name", TokenType.Identifier)
    expect(tokens(8)).toBe(":", TokenType.Colon)
    expect(tokens(9)).toBe("String", TokenType.Identifier)
    expect(tokens(10)).toBe(")", TokenType.CloseRoundBracket)
    expect(tokens(11)).toBe("\0", TokenType.Eof)
  }

  test("identifierInBackticks") {
    val tokens = tokenize("fun `<=>`")
    expect(tokens(0)).toBe("fun", TokenType.Identifier)
    expect(tokens(1)).toBe("<=>", TokenType.Identifier)
    expect(tokens(2)).toBe("\0", TokenType.Eof)
  }

  test("objLit") {
    val tokens = tokenize("""{host: "localhost", port: 8080}""")
    expect(tokens(0)).toBe("{", TokenType.OpenCurlyBracket)
    expect(tokens(1)).toBe("host", TokenType.Identifier)
    expect(tokens(2)).toBe(":", TokenType.Colon)
    expect(tokens(3)).toBe("localhost", TokenType.StrLit)
    expect(tokens(4)).toBe(",", TokenType.Comma)
    expect(tokens(5)).toBe("port", TokenType.Identifier)
    expect(tokens(6)).toBe(":", TokenType.Colon)
    expect(tokens(7)).toBe("8080", TokenType.IntLit)
    expect(tokens(8)).toBe("}", TokenType.CloseCurlyBracket)
  }

  test("emptyString") {
    val tokens = tokenize("""x = "" """)
    expect(tokens(0)).toBe("x", TokenType.Identifier)
    expect(tokens(1)).toBe("=", TokenType.Assign)
    expect(tokens(2)).toBe("", TokenType.StrLit)
  }

  test("or") {
    val tokens = tokenize("|")
    expect(tokens(0)).toBe("|", TokenType.VertBar)
  }

  test("genericClass") {
    val tokens = tokenize("class Foo<T>(v: T)")
    expect(tokens(0)).toBe("class", TokenType.Identifier)
    expect(tokens(1)).toBe("Foo", TokenType.Identifier)
    expect(tokens(2)).toBe("<", TokenType.Lt)
    expect(tokens(3)).toBe("T", TokenType.Identifier)
    expect(tokens(4)).toBe(">", TokenType.Gt)
    expect(tokens(5)).toBe("(", TokenType.OpenRoundBracket)
    expect(tokens(6)).toBe("v", TokenType.Identifier)
    expect(tokens(7)).toBe(":", TokenType.Colon)
    expect(tokens(8)).toBe("T", TokenType.Identifier)
    expect(tokens(9)).toBe(")", TokenType.CloseRoundBracket)
  }

  test("wildcard import") {
    val tokens = tokenize("import java.lang.*")
    expect(tokens(0)).toBe("import", TokenType.Identifier)
    expect(tokens(1)).toBe("java", TokenType.Identifier)
    expect(tokens(2)).toBe(".", TokenType.Dot)
    expect(tokens(3)).toBe("lang", TokenType.Identifier)
    expect(tokens(4)).toBe(".", TokenType.Dot)
    expect(tokens(5)).toBe("*", TokenType.Asterisk)
  }

  test("fun def") {
    val tokens = tokenize("fun div(id: String): Div")
    expect(tokens(0)).toBe("fun", TokenType.Identifier)
    expect(tokens(1)).toBe("div", TokenType.Identifier)
    expect(tokens(2)).toBe("(", TokenType.OpenRoundBracket)
    expect(tokens(3)).toBe("id", TokenType.Identifier)
    expect(tokens(4)).toBe(":", TokenType.Colon)
    expect(tokens(5)).toBe("String", TokenType.Identifier)
    expect(tokens(6)).toBe(")", TokenType.CloseRoundBracket)
    expect(tokens(7)).toBe(":", TokenType.Colon)
    expect(tokens(8)).toBe("Div", TokenType.Identifier)
  }

  test("fun type") {
    val tokens = tokenize("() -> Unit")
    expect(tokens(0)).toBe("(", TokenType.OpenRoundBracket)
    expect(tokens(1)).toBe(")", TokenType.CloseRoundBracket)
    expect(tokens(2)).toBe("->", TokenType.FunArrow)
    expect(tokens(3)).toBe("Unit", TokenType.Identifier)
  }
}
