package lang.compiler.parsing

import lang.compiler.parsing.impl.ExprParser
import org.scalatest.FunSpecLike

class ExprParserTest extends BaseParserTest(ExprParser) with FunSpecLike {
  describe("literals") {
    it("int lit") {
      val result = parse("42")
      expect(result).toBe("42")
    }

    it("string lit") {
      val result = parse(""" "Hello world!" """)
      expect(result).toBe(""""Hello world!"""")
    }

    it("ident") {
      val result = parse("f")
      expect(result).toBe("f")
    }
  }

  describe("assignment") {
    it("var") {
      doTest("x = 42", "x = 42")
    }

    it("field") {
      doTest("f.x = 42", "f.x = 42")
    }
  }


  describe("math") {
    it("single op") {
      doTest("2 + 2", "2.+(2)")
    }

    it("precedence") {
      doTest("2 + 2 * 2", "2.+(2.*(2))")
    }
  }

  describe("dot parselet") {
    it("get field") {
      val result = parse("""f.x""")
      expect(result).toBe("""f.x""")
    }

    describe("method call") {
      it("no arg") {
        val result = parse("""Arrays.asList()""")
        expect(result).toBe("""Arrays.asList()""")
        assert(result.asInstanceOf[ParserSuccess[_]].index == 5)
      }

      it("one arg") {
        val result = parse("""Arrays.asList(1)""")
        expect(result).toBe("""Arrays.asList(1)""")
        assert(result.asInstanceOf[ParserSuccess[_]].index == 6)
      }

      it("few args") {
        val result = parse("""Arrays.asList(1, 2)""")
        expect(result).toBe("""Arrays.asList(1, 2)""")
        assert(result.asInstanceOf[ParserSuccess[_]].index == 8)
      }
    }
  }

  describe("tuples") {
    it("0") {
      doTest("()", "()")
    }
    it("1") {
      doTest("(1)", "1")
    }
    it("2") {
      doTest("(1, 2)", "(1, 2)")
    }
  }

  it("""java.lang.System.out.println("Hello world!")""") {
    doTest("""java.lang.System.out.println("Hello world!")""", """java.lang.System.out.println("Hello world!")""")
  }

  it("fun call") {
    doTest("f()", "f()")
  }

  it("generic fun call") {
    doTest("f[X]()", "f[X]()")
  }

  it("generic fun call no parens") {
    doTest("f[X]", "f[X]")
  }
}