package lang.compiler.parsing

import lang.compiler.parsing.impl.Parsers
import lang.compiler.parsing.trees.Import
import org.scalatest.{FunSpec, FunSpecLike}

class ImportParserTest extends BaseParserTest(Parsers.`import`) with FunSpecLike {
  it("should parse wildcard imports") {
    doTest("import java.lang.*", "import java.lang.*")
  }
}
