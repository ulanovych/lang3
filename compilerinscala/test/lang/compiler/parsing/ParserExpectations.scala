package lang.compiler.parsing

import lang.compiler.parsing.trees.Expr
import lang.compiler.parsing.trees.visiting.ToStringVisitor
import org.scalatest.Suite

trait ParserExpectations {
  self: Suite =>

  def expect(actualResult: ParserResult[Expr]) = new {
    def toBe(expectedResult: String): Unit = {
      val expr = actualResult.get
      val visitor = new ToStringVisitor
      expr.accept(visitor)
      assertResult(expectedResult)(visitor.getResult)
    }
  }
}
