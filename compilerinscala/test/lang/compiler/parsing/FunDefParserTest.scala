package lang.compiler.parsing

import lang.compiler.parsing.impl.Parsers
import org.scalatest.FunSuiteLike

class FunDefParserTest extends BaseParserTest(Parsers.funDef) with FunSuiteLike {
  test("1") {
    doTest("fun f", "fun f")
  }

  test("2") {
    doTest("fun f(x: Int)", "fun f(x: Int)")
  }

  test("3") {
    doTest("fun f(x: Int) = x", "fun f(x: Int) = x")
  }
}
