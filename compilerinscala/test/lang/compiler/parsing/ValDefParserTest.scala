package lang.compiler.parsing

import lang.compiler.parsing.impl.Parsers
import org.scalatest.FunSuiteLike

class ValDefParserTest extends BaseParserTest(Parsers.valDef) with FunSuiteLike {
  test("it") {
    doTest("val x = 42", "val x = 42")
  }
}
