package lang.compiler.parsing

import lang.compiler.BaseCompilerTest
import lang.compiler.lexicalanalysis.Lexer
import lang.compiler.parsing.impl.Parser
import lang.compiler.parsing.trees.Node
import lang.compiler.parsing.trees.visiting.ToStringVisitor
import org.scalatest.exceptions.TestFailedException

abstract class BaseParserTest[T <: Node](parser: Parser[T] = null) extends BaseCompilerTest with ParserExpectations {
  def parse(src: String): ParserResult[T] = {
    val tokens = Lexer.tokenize(src)
    parser.parse(tokens, 0)
  }

  def doTest(src: String, expected: String): Unit = {
    parse(src) match {
      case ParserSuccess(expr, _, _) =>
        val visitor = new ToStringVisitor
        expr.accept(visitor)
        val actual = visitor.getResult
        if (actual != expected) {
          throw new TestFailedException(s"""Expected "$expected", got "$actual"""", 2)
        }
      case e: ParserError =>
        throw new TestFailedException(e.getMessageWithCodeLine(src), 2)
    }
  }
}