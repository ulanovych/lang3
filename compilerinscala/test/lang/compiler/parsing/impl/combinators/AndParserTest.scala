package lang.compiler.parsing.impl.combinators

import lang.compiler.MyMatchers
import lang.compiler.lexicalanalysis.{Lexer, TokenType}
import lang.compiler.parsing.{BaseParserTest, ParserSuccess}
import lang.compiler.parsing.impl.Parsers._
import org.scalatest.{FunSpec, FunSpecLike}

class AndParserTest extends FunSpecLike with MyMatchers {
  it("should parse") {
    val parser = token(TokenType.IntLit) ~ token(TokenType.IntLit)
    val ParserSuccess(_, i, _) = parser.parse(Lexer.tokenize("1 2"), 0)
    i shouldBe 2
  }

}
