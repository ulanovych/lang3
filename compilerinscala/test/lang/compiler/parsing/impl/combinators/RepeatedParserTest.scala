package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.{Lexer, Token, TokenType}
import lang.compiler.parsing.{BaseParserTest, ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.impl.{ExprParser, Parser}
import org.scalatest.{FunSpec, FunSuite, Matchers}

class RepeatedParserTest extends FunSpec with Matchers {
  describe("zero plus with separator") {
    val parser = ExprParser.rep(TokenType.Comma, Cardinality.ZeroOrMore)

    it("one rep") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1"), 0)
      exprs.size shouldBe 1
      index shouldBe 1
    }

    it("few reps") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1, 2, 3"), 0)
      exprs.size shouldBe 3
      index shouldBe 5
    }

    it("no rep") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("+"), 0)
      exprs.size shouldBe 0
      index shouldBe 0
    }

    it("error") {
      val ParserError(_, _) = parser.parse(Lexer.tokenize("1, 2, ,"), 0)
    }
  }

  describe("one plus with separator") {
    val parser = ExprParser.rep(TokenType.Comma, Cardinality.OneOrMore)

    it("one rep") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1"), 0)
      exprs.size shouldBe 1
      index shouldBe 1
    }

    it("few reps") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1, 2, 3"), 0)
      exprs.size shouldBe 3
      index shouldBe 5
    }

    it("no rep") {
      val ParserError(_, _) = parser.parse(Lexer.tokenize("+"), 0)
    }

    it("error") {
      val ParserError(_, _) = parser.parse(Lexer.tokenize("1, 2, ,"), 0)
    }
  }

  describe("zero plus without separator") {
    val parser = ExprParser.rep(Cardinality.ZeroOrMore)

    it("one rep") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1"), 0)
      exprs.size shouldBe 1
      index shouldBe 1
    }

    it("few reps") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1 2 3"), 0)
      exprs.size shouldBe 3
      index shouldBe 3
    }

    it("no rep") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("+"), 0)
      exprs.size shouldBe 0
      index shouldBe 0
    }

    it("error") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1 2 ,"), 0)
      exprs.size shouldBe 2
      index shouldBe 2
    }
  }

  describe("one plus without separator") {
    val parser = ExprParser.rep(Cardinality.OneOrMore)

    it("one rep") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1"), 0)
      exprs.size shouldBe 1
      index shouldBe 1
    }

    it("few reps") {
      val ParserSuccess(exprs, index, _) = parser.parse(Lexer.tokenize("1 2 3"), 0)
      exprs.size shouldBe 3
      index shouldBe 3
    }

    it("no rep") {
      val ParserError(_, _) = parser.parse(Lexer.tokenize("+"), 0)
    }
  }
}