package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.{Lexer, TokenType}
import lang.compiler.parsing.impl.Parsers
import org.scalatest.FunSpec

class RepeatingParserTest extends FunSpec{
  val intLit = Parsers.token(TokenType.IntLit)

  it("x") {
    intLit.rep2().parse("1 x 3")
  }
}
