package lang.compiler.parsing

import lang.compiler.parsing.impl.Parsers
import org.scalatest.FunSuiteLike

class EnumDefParserTest extends BaseParserTest(Parsers.stmt) with FunSuiteLike {
  test("it") {
    doTest("enum Gender {Male, Female}",
      "enum Gender {\n" +
      "    Male, Female\n" +
      "}"
    )
  }
}
