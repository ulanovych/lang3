package lang.compiler.parsing

import java.nio.file.{Files, Paths}

import lang.compiler.parsing.impl.Parsers
import lang.compiler.parsing.trees.visiting.ToStringVisitor
import org.scalatest.FunSuiteLike

class HtmlDslParserTest extends BaseParserTest(Parsers.file) with FunSuiteLike {
  test("it") {
    val src = new String(Files.readAllBytes(Paths.get("../htmldsl.lang")), "UTF-8")
    val ParserSuccess(file, _, _) = parse(src)
    val visitor = new ToStringVisitor
    file.accept(visitor)
    println(visitor.getResult)
  }
}
