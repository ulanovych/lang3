package lang.compiler.parsing

import lang.compiler.parsing.impl.Parsers
import org.scalatest.FunSuiteLike

class TypeExprParserTest extends BaseParserTest(Parsers.typeExpr) with FunSuiteLike {
  test("arr") {
    doTest("Int*", "Int.*()")
  }

  test("parametrization") {
    doTest("List[Int]", "List[Int]()")
  }

  test("function type") {
    doTest("() -> Unit", "().->(Unit)")
  }

  test("function type 1") {
    doTest("() -> Unit.X", "().->(Unit.X)")
  }

  test("method type") {
    doTest("Builder[T].() -> Unit", "Builder[T]().().->(Unit)")
  }

  test("dot") {
    doTest("F.X", "F.X")
  }
}
