package lang.compiler

import org.scalatest.Suite

trait BaseCompilerTest extends Suite {
  def expect(actual: String) = new StringExpectationBuilder(actual)
}
