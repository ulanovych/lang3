package lang.compiler.codegeneration.js

import java.nio.file.{Files, Paths}

import lang.compiler.lexicalanalysis.Lexer
import lang.compiler.parsing.ParserSuccess
import lang.compiler.parsing.impl.Parsers
import lang.compiler.parsing.trees.visiting.ToStringVisitor
import org.scalatest.FunSuite

class JsCodeGeneratorTest extends FunSuite {
  test("htmldsl") {
    val src = new String(Files.readAllBytes(Paths.get("../htmldsl.lang")), "UTF-8")
    val ParserSuccess(file, _, _) = Parsers.file.parse(src)
    val visitor = new JsCodeGeneratingVisitor
    try {
      file.accept(visitor)
    } finally {
      println(visitor.getResult)
    }
  }
}
