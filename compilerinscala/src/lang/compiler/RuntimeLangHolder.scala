package lang.compiler

import lang.compiler.util.using
import org.apache.commons.io.IOUtils

object RuntimeLangHolder {
  lazy val src = {
    using(getClass.getResourceAsStream("/runtime.lang"))(IOUtils.toString(_, "UTF-8"))
  }
}
