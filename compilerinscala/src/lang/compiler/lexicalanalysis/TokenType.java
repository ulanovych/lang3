package lang.compiler.lexicalanalysis;

import java.util.HashMap;
import java.util.Map;

public enum TokenType {
    OpenRoundBracket,
    CloseRoundBracket,
    OpenSquareBracket,
    CloseSquareBracket,
    OpenCurlyBracket,
    CloseCurlyBracket,
    Comma,
    Dot,
    Assign,
    Colon,
    StrLit,
    IntLit,
    Identifier,
    Plus,
    Minus,
    Asterisk,
    Slash,
    Whitespace,
    Lt,
    Gt,
    Tilde,
    ExclamationMark,
    VertBar,
    QuestionMark,
    Eq,
    CustomOp,
    FunArrow,
    Eof;

    public static Map<String, TokenType> tokenToType = new HashMap<String, TokenType>(){{
        put("==", TokenType.Eq);
        put("->", TokenType.FunArrow);
        put(".", TokenType.Dot);
        put("=", TokenType.Assign);
        put(":", TokenType.Colon);
        put("+", TokenType.Plus);
        put("-", TokenType.Minus);
        put("*", TokenType.Asterisk);
        put("/", TokenType.Slash);
        put("?", TokenType.QuestionMark);
        put("!", TokenType.ExclamationMark);
        put("`", TokenType.Tilde);
        put("|", TokenType.VertBar);
        put("<", TokenType.Lt);
        put(">", TokenType.Gt);
    }};

    public static TokenType getTokenType(String token) {
        TokenType type = tokenToType.get(token);
        return type != null ? type : TokenType.CustomOp;
    }
}
