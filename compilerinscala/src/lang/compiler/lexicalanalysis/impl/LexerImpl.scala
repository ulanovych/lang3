package lang.compiler.lexicalanalysis.impl

import lang.compiler.lexicalanalysis.{Token, TokenType}

import scala.collection.mutable.ArrayBuffer

object LexerImpl {
  def tokenize(src: String): Seq[Token] = {
    val firstRowNumber = 1
    val firstColumnNumber = 1

    val chars = (src + "\0\0\0").toCharArray

    val tokens = new ArrayBuffer[Token]()

    var index = 0

    var row = firstRowNumber
    var column = firstColumnNumber

    var tokenFirstIndex = 0
    var tokenLastIndex = 0

    var tokenRow = firstRowNumber
    var tokenColumn = firstColumnNumber

    var stop = false

    while (!stop) {
      val chr = chars(index)
      if (chr.isWhitespace) do {
        if (chars(index) == '\n') {
          row += 1
          column = firstColumnNumber
        }
        else column += 1
        index += 1
      } while (chars(index).isWhitespace)
      else if (chr.isLetter) {
        tokenFirstIndex = index
        tokenRow = row
        tokenColumn = column
        do {
          index += 1
          column += 1
        } while (chars(index).isLetterOrDigit)
        tokenLastIndex = index - 1
        val value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1)
        val token = Token(value, TokenType.Identifier, tokenRow, tokenColumn)
        tokens += token
      }
      else if (chr == '`') {
        index += 1
        column += 1
        tokenRow = row
        tokenColumn = column
        tokenFirstIndex = index
        do {
          index += 1
          column += 1
        } while (chars(index) != '`')
        index += 1
        column += 1
        tokenLastIndex = index - 2
        val value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1)
        val token = Token(value, TokenType.Identifier, tokenRow, tokenColumn)
        tokens += token
      }
      else if (chr == '"') {
        tokenRow = row
        tokenColumn = column + 1
        tokenFirstIndex = index + 1
        do {
          index += 1
          column += 1
        } while (chars(index) != '"')
        index += 1
        column += 1
        tokenLastIndex = index - 2
        val value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1)
        val token = Token(value, TokenType.StrLit, tokenRow, tokenColumn)
        tokens += token
      }
      else if (chr.isDigit) {
        tokenFirstIndex = index
        tokenRow = row
        tokenColumn = column
        do {
          index += 1
          column += 1
        } while (chars(index).isDigit)
        tokenLastIndex = index - 1
        val value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1)
        val token = Token(value, TokenType.IntLit, tokenRow, tokenColumn)
        tokens += token
      }
      else if (isOpChar(chr)) {
        tokenFirstIndex = index
        tokenRow = row
        tokenColumn = column
        do {
          index += 1
          column += 1
        } while (isOpChar(chars(index)))
        tokenLastIndex = index - 1
        val value = new String(chars, tokenFirstIndex, tokenLastIndex - tokenFirstIndex + 1)
        val tokenType = TokenType.getTokenType(value)
        if (tokenType == null) throw new RuntimeException("unknown token: " + value)
        val token = Token(value, tokenType, tokenRow, tokenColumn)
        tokens += token
      }
      else {
        tokenRow = row
        tokenColumn = column
        val `type` = chr match {
          case '.' => TokenType.Dot
          case '(' => TokenType.OpenRoundBracket
          case ')' => TokenType.CloseRoundBracket
          case '[' => TokenType.OpenSquareBracket
          case ']' => TokenType.CloseSquareBracket
          case '{' => TokenType.OpenCurlyBracket
          case '}' => TokenType.CloseCurlyBracket
          case ',' => TokenType.Comma
          case '\0' =>
            stop = true
            TokenType.Eof
          case _ =>
            throw new Exception("unknown character: '" + chr + "'")
        }
        index += 1
        column += 1
        val token = Token(Character.toString(chr), `type`, tokenRow, tokenColumn)
        tokens += token
      }
    }
    tokens
  }

  private def isOpChar(chr: Char): Boolean = chr match {
//    case '.' => true
    case '=' => true
    case ':' => true
    case '+' => true
    case '-' => true
    case '*' => true
    case '/' => true
    case '?' => true
    case '!' => true
    case '~' => true
    case '|' => true
    case '<' => true
    case '>' => true
    case _ => false
  }
}