package lang.compiler.lexicalanalysis

import lang.compiler.lexicalanalysis.impl.LexerImpl

object Lexer {
  def tokenize(src: String): Seq[Token] = {
    LexerImpl.tokenize(src)
  }
}