package lang.compiler.lexicalanalysis

case class Token(value: String, `type`: TokenType, line: Int, col: Int)