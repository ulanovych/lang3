package lang.compiler

import java.util.Arrays

import lang.compiler.lexicalanalysis.Token

abstract class CompilationError(firstToken: Token, lastToken: Token) {
  def getMessage: String

  def getMessageWithCodeLine(src: String): String = {
    val lines: Array[String] = src.split("\n")
    val firstLine: String = getMessage
    val lineNumber: Int = firstToken.line
    val lineNumberPrefix: String = new Integer(lineNumber).toString + ": "
    val secondLine: String = lineNumberPrefix + lines(firstToken.line - 1)
    var thirdLine: String = null
    val startColumn: Int = firstToken.col - 1
    if (firstToken.line == lastToken.line) {
      val endColumn: Int = lastToken.col + lastToken.value.length - 1
      val chars: Array[Char] = new Array[Char](endColumn)
      Arrays.fill(chars, 0, lastToken.col - 1, ' ')
      Arrays.fill(chars, startColumn, endColumn, '~')
      thirdLine = new String(new Array[Char](lineNumberPrefix.length)).replace('\0', ' ') + new String(chars)
    }
    else thirdLine = new String(new Array[Char](startColumn)).replace('\0', ' ') + '^'
    firstLine + '\n' + secondLine + '\n' + thirdLine
  }
}