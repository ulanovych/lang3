package lang.compiler.interpretation

import lang.compiler.parsing.trees.Node
import lang.compiler.typechecking.Scope2

object interpret {
  def apply(node: Node, scope: Scope2): Any = {
    val visitor = new InterpretingVisitor2(scope)
    visitor.continue(node)
  }
}
