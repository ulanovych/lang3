package lang.compiler.interpretation

import lang.compiler.parsing.trees.Node

object Interpreter {
  def interpret(node: Node): Unit = {
    val visitor = new InterpretingVisitor
    node.accept(visitor)
  }
}
