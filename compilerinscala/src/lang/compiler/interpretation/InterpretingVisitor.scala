package lang.compiler.interpretation

import lang.compiler.parsing.trees._
import lang.compiler.parsing.trees.visiting.Visitor
import lang.compiler.typechecking.types._

class InterpretingVisitor extends Visitor[Any] {
  override def visit(lit: IntLit): Any = ???

  override def visit(lit: StrLit): Any = {
    lit.token.value
  }

  override def visit(ident: Ident): Any = ???

  override def visit(getField: GetField): Any = {
    getField.expr.`type` match {
      case klass: TClass =>
        getField.field match {
          case field: StaticField =>
            Class.forName(klass.fqName).getField(field.name).get(null)
        }
    }
  }

  override def visit(setField: SetField): Any = ???

  override def visit(call: MethodCall): Any = {
    val obj = continue(call.expr())
    val klass = call.expr().`type`.asInstanceOf[TClass]
    val method = call.method
    val parameterTypes = method.parameterTypes.map {
      case klass: TClass => Class.forName(klass.fqName)
    }
    val reflectionMethod = Class.forName(klass.fqName).getMethod(method.name, parameterTypes: _*)
    val args = call.args.map(continue).map(_.asInstanceOf[Object])
    reflectionMethod.invoke(obj, args: _*)
  }

  override def visit(defn: ValDef): Any = ???

  override def visit(file: File): Any = {
    file.stmts.foreach(continue)
  }

  override def visit(imprt: Import): Any = {}

  override def visit(defn: EnumDef): Any = ???

  override def visit(defn: ClassDef): Any = ???

  override def visit(typaram: TyParam): Any = ???

  override def visit(param: Param): Any = ???

  override def visit(call: FunCall): Any = ???

  override def visit(defn: FunDef): Any = ???

  override def visit(tuple: TupleExpr): Any = ???

  override def visit(mtype: MethodType): Any = ???

  override def visit(body: FunBody): Any = ???

  override def visit(lambda: Lambda): Any = ???

  override def visit(block: ArgsBlock): Any = ???

  override def visit(block: TypeArgsBlock): Any = ???

  override def visit(block: ParamBlock): Any = ???

  override def visit(block: TyParamBlock): Any = ???

  override def visit(block: StmtBlock): Any = ???

  override def visit(block: ParentsBlock): Any = ???

  override def visit(node: GetLocalVar): Any = ???

  override def visit(node: SetLocalVar): Any = ???
}
