package lang.compiler.interpretation

import lang.compiler.parsing.trees._
import lang.compiler.parsing.trees.visiting.Visitor
import lang.compiler.typechecking.Scope2

class InterpretingVisitor2(var scope: Scope2) extends Visitor[Any] {
  override def visit(lit: IntLit): Any = ???

  override def visit(lit: StrLit): Any = ???

  override def visit(ident: Ident): Any = ???

  override def visit(tuple: TupleExpr): Any = ???

  //  def visit(assign: Assign): T
  override def visit(getField: GetField): Any = ???

  override def visit(setField: SetField): Any = ???

  override def visit(call: FunCall): Any = ???

  override def visit(call: MethodCall): Any = ???

  override def visit(defn: ClassDef): Any = ???

  override def visit(defn: EnumDef): Any = ???

  override def visit(defn: ValDef): Any = ???

  override def visit(defn: FunDef): Any = ???

  override def visit(body: FunBody): Any = ???

  override def visit(lambda: Lambda): Any = ???

  override def visit(file: File): Any = ???

  override def visit(imprt: Import): Any = ???

  override def visit(typaram: TyParam): Any = ???

  override def visit(param: Param): Any = ???

  override def visit(mtype: MethodType): Any = ???

  override def visit(block: ArgsBlock): Any = ???

  override def visit(block: TypeArgsBlock): Any = ???

  override def visit(block: ParamBlock): Any = ???

  override def visit(block: TyParamBlock): Any = ???

  override def visit(block: StmtBlock): Any = ???

  override def visit(block: ParentsBlock): Any = ???

  override def visit(node: GetLocalVar): Any = {
    scope.get(node.name.value).orNull
  }

  override def visit(node: SetLocalVar): Any = ???
}
