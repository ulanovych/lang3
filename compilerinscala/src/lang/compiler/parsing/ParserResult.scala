package lang.compiler.parsing

import lang.compiler.CompilationError
import lang.compiler.lexicalanalysis.Token

sealed trait ParserResult[+T] {
  def get: T
  def isSuccess: Boolean
  def isError = !isSuccess
  def map[U](f: T => U): ParserResult[U]
}


case class ParserSuccess[T](value: T, index: Int, errors: Seq[String] = Seq()) extends ParserResult[T] {
  override def get = value
  override def isSuccess: Boolean = true
  override def map[U](f: (T) => U): ParserResult[U] = ParserSuccess(f(value), index)
}

case class ParserError(msg: String, token: Token) extends CompilationError(token, token) with ParserResult[Nothing] {
  override def get = throw new Exception(s"There was parsing error: $msg")
  override def isSuccess: Boolean = false
  override def map[U](f: (Nothing) => U): ParserResult[U] = this

  override def getMessage: String = msg
}