package lang.compiler.parsing.impl

import lang.compiler.lexicalanalysis.TokenType
import lang.compiler.parsing.impl.combinators.ParserCombinatorImplicits
import lang.compiler.parsing.impl.parsers.{KeywordParser, TokenParser}

trait ParserDsl extends ParserCombinatorImplicits {
  def keyword(keyword: String) = KeywordParser(keyword)

  def token(tokenType: TokenType) = TokenParser(tokenType)
}
