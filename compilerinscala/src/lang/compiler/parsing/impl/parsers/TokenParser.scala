package lang.compiler.parsing.impl.parsers

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.impl.Parser
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}

case class TokenParser(tokenType: TokenType) extends Parser[Token] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[Token] = {
    val token = tokens(index)
    if (token.`type` == tokenType) {
      ParserSuccess(token, index + 1)
    } else {
      ParserError(s"Expected keyword $tokenType, got $token", token)
    }
  }
}