package lang.compiler.parsing.impl.parsers

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.impl.Parser

case class KeywordParser(keyword: String) extends Parser[Token] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[Token] = {
    val token = tokens(index)
    if (token.`type` == TokenType.Identifier && token.value == keyword) {
      ParserSuccess(token, index + 1)
    } else {
      ParserError(s"Expected keyword $keyword, got $token", token)
    }
  }
}