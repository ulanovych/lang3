package lang.compiler.parsing.impl

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.{ParserResult, Precedence}
import lang.compiler.parsing.impl.parselets._
import lang.compiler.parsing.pratt.PrattParser
import lang.compiler.parsing.trees.{Expr, Ident, IntLit, StrLit}

object ExprParser extends Parser[Expr] {
  private val prefixParselets = Map(
      TokenType.OpenRoundBracket -> GroupingParselet
    , TokenType.IntLit -> LiteralParselet(IntLit)
    , TokenType.StrLit -> LiteralParselet(StrLit)
    , TokenType.Identifier -> LiteralParselet(Ident)
  )

  private val infixParselets = Map(
    TokenType.Plus -> InfixOperatorParselet(Precedence.Additive.ordinal())
    , TokenType.Minus -> InfixOperatorParselet(Precedence.Additive.ordinal())
    , TokenType.Asterisk -> InfixOperatorParselet(Precedence.Multiplicative.ordinal())
    , TokenType.Slash -> InfixOperatorParselet(Precedence.Multiplicative.ordinal())
    , TokenType.OpenRoundBracket -> FunCallParselet
    , TokenType.OpenSquareBracket -> ParameterizedFunCallParselet
    , TokenType.OpenCurlyBracket -> LeftBraceParselet
    , TokenType.Dot -> DotParselet
    , TokenType.Assign -> AssignParselet
  )

  private val parser = new PrattParser(prefixParselets, infixParselets)

  override def parse(tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    parser.parseExpr(index, tokens)
  }
}