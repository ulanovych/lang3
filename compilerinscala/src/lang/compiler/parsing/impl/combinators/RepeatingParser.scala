package lang.compiler.parsing.impl.combinators

import lang.compiler.CompilationError
import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.{ParserResult, ParserSuccess}
import lang.compiler.parsing.impl.Parser

import scala.collection.mutable.ArrayBuffer

class RepeatingParser[T](parser: Parser[T], until: TokenType) extends Parser[Seq[T]] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[Seq[T]] = {
    val values = ArrayBuffer[T]()
    val errors = ArrayBuffer[CompilationError]()
    var i = index
    var stop = false
    while(!stop) {
      val result = parser.parse(tokens, i)
      result match {
        case ParserSuccess(value, ni, ers) =>
          values += value
      }
    }
    ???
  }
}
