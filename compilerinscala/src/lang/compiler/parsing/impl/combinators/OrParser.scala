package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.impl.Parser
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}

case class OrParser[T, A <: T, B <: T](parserA: Parser[A], parserB: Parser[B]) extends Parser[T] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[T] = {
    parserA.parse(tokens, index) match {
      case x: ParserSuccess[A] => x
      case e: ParserError =>
        parserB.parse(tokens, index) match {
          case e: ParserError => e
          case x: ParserSuccess[B] => x
        }
    }
  }
}