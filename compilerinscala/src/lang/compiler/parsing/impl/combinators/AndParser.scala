package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.impl.Parser

case class AndParser[A, B](parserA: Parser[A], parserB: Parser[B]) extends Parser[(A, B)] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[(A, B)] = {
    parserA.parse(tokens, index) match {
      case e: ParserError => e
      case ParserSuccess(valueA, index, _) =>
        parserB.parse(tokens, index) match {
          case e: ParserError => e
          case ParserSuccess(valueB, index, _) =>
            ParserSuccess((valueA, valueB), index)
        }
    }
  }
}