package lang.compiler.parsing.impl.combinators;

public enum Cardinality {
    ZeroOrMore, OneOrMore
}
