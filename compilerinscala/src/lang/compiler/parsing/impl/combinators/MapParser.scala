package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.ParserResult
import lang.compiler.parsing.impl.Parser

case class MapParser[A, B](parser: Parser[A], f: A => B) extends Parser[B] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[B] = {
    parser.parse(tokens, index).map(f)
  }
}