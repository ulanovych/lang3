package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.impl.Parser

case class OptParser[+T](parser: Parser[T]) extends Parser[Option[T]] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[Option[T]] = {
    parser.parse(tokens, index) match {
      case e: ParserSuccess[T] => e.map(Some.apply)
      case e: ParserError => ParserSuccess(None, index)
    }
  }
}
