package lang.compiler.parsing.impl.combinators

import lang.compiler.parsing.impl.Parser

trait AndCombinatorImplicits {
  implicit def a1[A](parserA: Parser[A]) = new {
    def ~[B](parserB: Parser[B]): Parser[(A, B)] = AndParser(parserA, parserB)
  }

  implicit def a2[A, B](parser: Parser[(A, B)]) = new {
    def ~[C](that: Parser[C]): Parser[(A, B, C)] = {
      AndParser(parser, that).map(x => (x._1._1, x._1._2, x._2))
    }
  }

  implicit def a3[A, B, C](parser: Parser[(A, B, C)]) = new {
    def ~[D](that: Parser[D]): Parser[(A, B, C, D)] = {
      AndParser(parser, that).map(x => (x._1._1, x._1._2, x._1._3, x._2))
    }
  }

  implicit def a4[A, B, C, D](parser: Parser[(A, B, C, D)]) = new {
    def ~[E](that: Parser[E]): Parser[(A, B, C, D, E)] = {
      AndParser(parser, that).map(x => (x._1._1, x._1._2, x._1._3, x._1._4, x._2))
    }
  }

  implicit def a5[A, B, C, D, E](parser: Parser[(A, B, C, D, E)]) = new {
    def ~[F](that: Parser[F]): Parser[(A, B, C, D, E, F)] = {
      AndParser(parser, that).map(x => (x._1._1, x._1._2, x._1._3, x._1._4, x._1._5, x._2))
    }
  }

  implicit def a6[A, B, C, D, E, F](parser: Parser[(A, B, C, D, E, F)]) = new {
    def ~[G](that: Parser[G]): Parser[(A, B, C, D, E, F, G)] = {
      AndParser(parser, that).map(x => (x._1._1, x._1._2, x._1._3, x._1._4, x._1._5, x._1._6, x._2))
    }
  }
}
