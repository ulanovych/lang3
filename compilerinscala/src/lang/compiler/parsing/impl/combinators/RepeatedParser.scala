package lang.compiler.parsing.impl.combinators

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.impl.Parser

import scala.collection.mutable.ArrayBuffer

case class RepeatedParser[+T](parser: Parser[T], separator: Option[TokenType], cardinality: Cardinality) extends Parser[Seq[T]] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[Seq[T]] = {
    parser.parse(tokens, index) match {
      case e: ParserError => cardinality match {
        case Cardinality.ZeroOrMore => ParserSuccess(Seq(), index)
        case Cardinality.OneOrMore => e
      }
      case ParserSuccess(value, i, _) =>
        val values = ArrayBuffer[T]()
        values += value
        var index = i
        var result: ParserResult[Seq[T]] = null
        var stop = false
        separator match {
          case Some(sep) =>
            while (!stop) {
              if (tokens(index).`type` == sep) {
                parser.parse(tokens, index + 1) match {
                  case ParserSuccess(value, i, _) =>
                    values += value
                    index = i
                  case e: ParserError =>
                    result = e
                    stop = true
                }
              } else {
                result = ParserSuccess(values, index)
                stop = true
              }
            }
          case None =>
            while(!stop) {
              parser.parse(tokens, index) match {
                case ParserSuccess(value, i, _) =>
                  values += value
                  index = i
                case e: ParserError =>
                  result = ParserSuccess(values, index)
                  stop = true
              }
            }
        }
        result
    }
  }
}