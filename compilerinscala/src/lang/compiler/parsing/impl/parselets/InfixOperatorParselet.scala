package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.pratt.{InfixParselet, PrattParser}
import lang.compiler.parsing.trees.{ArgsBlock, Expr, MethodCall}

case class InfixOperatorParselet(override val precedence: Int) extends InfixParselet {
  override def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    val result = parser.parseExpr(index, tokens, precedence)
    result match {
      case ParserSuccess(right, i, _) => ParserSuccess(new MethodCall(left, token, Some(ArgsBlock(right.firstToken, Seq(right), right.lastToken))), i)
      case e: ParserError => ParserSuccess(new MethodCall(left, token, None), index)
    }
  }
}