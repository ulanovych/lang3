package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.impl.combinators.Cardinality
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}
import lang.compiler.parsing.pratt.{PrattParser, PrefixParselet}
import lang.compiler.parsing.trees.{Expr, TupleExpr}

object GroupingParselet extends PrefixParselet {
  override def parse(parser: PrattParser, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    parser.rep(TokenType.Comma, Cardinality.ZeroOrMore).parse(tokens, index) match {
      case ParserSuccess(values, i, _) =>
        if (tokens(i).`type` != TokenType.CloseRoundBracket) {
          ParserError("Expecting close round bracket", tokens(i))
        } else {
          val expr = values.length match {
            case 1 => values.head
            case _ => TupleExpr(tokens(index - 1), values, tokens(i))
          }
          ParserSuccess(expr, i + 1)
        }
      case _: ParserError =>
        throw new Exception("Unreachable")
    }
  }
}
