package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.{ParserResult, ParserSuccess}
import lang.compiler.parsing.pratt.{PrattParser, PrefixParselet}
import lang.compiler.parsing.trees.Expr

case class LiteralParselet[T <: Expr](f: Token => T) extends PrefixParselet {
  override def parse(parser: PrattParser, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    ParserSuccess(f(token), index)
  }
}
