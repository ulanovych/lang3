package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.impl.Parsers
import lang.compiler.parsing.{ParserResult, Precedence}
import lang.compiler.parsing.pratt.{InfixParselet, PrattParser}
import lang.compiler.parsing.trees.{ArgsBlock, Expr, FunCall, Lambda}

object LeftBraceParselet extends InfixParselet {
  override def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    Parsers.stmtBlock.parse(tokens, index - 1) map { stmts =>
      val lambda = Lambda(None, stmts)
      FunCall(left, None, Some(ArgsBlock(lambda.firstToken, Seq(lambda), lambda.lastToken)))
    }
  }

  override val precedence: Int = Precedence.Call.ordinal()
}
