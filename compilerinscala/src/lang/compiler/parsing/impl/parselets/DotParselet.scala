package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.impl.{ExprParser, Parsers}
import lang.compiler.parsing.impl.combinators.Cardinality
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess, Precedence}
import lang.compiler.parsing.pratt.{InfixParselet, PrattParser}
import lang.compiler.parsing.trees.{Expr, GetField, MethodCall, MethodType}

object DotParselet extends InfixParselet {
  override def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    var i = index
    val token = tokens(i)
    if (token.`type` == TokenType.Identifier ) {
      i += 1
      if (tokens(i).`type` != TokenType.OpenRoundBracket ) {
        ParserSuccess(GetField(left, token), i)
      } else {
        Parsers.argsBlock.parse(tokens, i) match {
          case ParserSuccess(values, index, _) =>
            ParserSuccess(new MethodCall(left, token, Some(values)), index)
          case _: ParserError => throw new Exception("Unreachable")
        }
      }
    } else {
      Parsers.typeExpr.parse(tokens, i) match {
        case ParserSuccess(right, i, _) =>
          ParserSuccess(MethodType(left, right), i)
        case _: ParserError =>
          ParserError(s"Expected identifier, got $token", token)
      }
    }
  }

  override val precedence = Precedence.Dot.ordinal()
}