package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.impl.{ParserDsl, Parsers}
import lang.compiler.parsing.impl.combinators.Cardinality
import lang.compiler.parsing.{ParserResult, ParserSuccess, Precedence}
import lang.compiler.parsing.pratt.{InfixParselet, PrattParser}
import lang.compiler.parsing.trees.{Expr, FunCall}

object FunCallParselet extends InfixParselet with ParserDsl {
  override def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    Parsers.argsBlock.parse(tokens, index - 1) map { x =>
      FunCall(left, None, Some(x))
    }
  }

  override val precedence: Int = Precedence.Call.ordinal()
}
