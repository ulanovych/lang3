package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.impl.{ParserDsl, Parsers}
import lang.compiler.parsing.{ParserResult, Precedence}
import lang.compiler.parsing.pratt.{InfixParselet, PrattParser}
import lang.compiler.parsing.trees.{Expr, FunCall}

object ParameterizedFunCallParselet extends InfixParselet with ParserDsl {
  override def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    val p = Parsers.typeArgsBlock ~ Parsers.argsBlock.opt
    p.parse(tokens, index - 1) map { x =>
      FunCall(left, Some(x._1), x._2)
    }
  }

  override val precedence: Int = Precedence.Call.ordinal()
}
