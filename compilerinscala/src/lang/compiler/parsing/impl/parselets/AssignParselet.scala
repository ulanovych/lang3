package lang.compiler.parsing.impl.parselets

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess, Precedence}
import lang.compiler.parsing.pratt.{InfixParselet, PrattParser}
import lang.compiler.parsing.trees._

object AssignParselet extends InfixParselet {
  override def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    parser.parseExpr(index, tokens) match {
      case e: ParserError => e
      case ParserSuccess(right, index, _) => left match {
        case ident: Ident => ParserSuccess(SetLocalVar(ident.token, right), index)
        case x: GetField => ParserSuccess(SetField(x.expr, x.name, right), index)
      }
    }
  }

  override val precedence = Precedence.Assignment.ordinal()
}
