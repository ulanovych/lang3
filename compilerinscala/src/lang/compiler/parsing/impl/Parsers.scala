package lang.compiler.parsing.impl

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.lexicalanalysis.TokenType._
import lang.compiler.parsing.ParserResult
import lang.compiler.parsing.impl.combinators.{Cardinality, ParserCombinatorImplicits}
import lang.compiler.parsing.impl.parsers.{KeywordParser, TokenParser}
import lang.compiler.parsing.trees._

object Parsers extends ParserDsl {
  def forwardRef[T](f: => Parser[T]): Parser[T] = new Parser[T] {
    override def parse(tokens: Seq[Token], index: Int): ParserResult[T] = f.parse(tokens, index)
  }

  val ident = token(Identifier)

  val expr = ExprParser

  val typeExpr = TypeExprParser

  val `import` = keyword("import") ~ ident ~ (token(Dot) ~ ident).* ~ token(Dot) ~ token(Asterisk) map { t =>
    val xs = t._3
    val x = xs.foldLeft[Expr](Ident(t._2)) {
      case (lhs, (dot, name)) => GetField(lhs, name)
    }
    Import(t._1, x)
  }

  val param = ident ~ token(Colon) ~ typeExpr map { x => Param(x._1, x._3) }
  val params = param.rep(Comma, Cardinality.ZeroOrMore)
  val paramsBlock = token(OpenRoundBracket) ~ params ~ token(CloseRoundBracket) map (ParamBlock.apply _ tupled)

  val typaram = ident ~ (token(Colon) ~ typeExpr).map(_._2).opt map { x => TyParam(x._1, x._2) }
  val typarams = typaram.rep(Comma, Cardinality.ZeroOrMore)
  val typaramsBlock = token(OpenSquareBracket) ~ typarams ~ token(CloseSquareBracket) map (TyParamBlock.apply _ tupled)

  val arg = expr
  val args = arg.rep(Comma, Cardinality.ZeroOrMore)
  val argsBlock = token(OpenRoundBracket) ~ args ~ token(CloseRoundBracket) map (ArgsBlock.apply _ tupled)

  val typeArg = typeExpr
  val typeArgs = typeArg.rep(Comma, Cardinality.ZeroOrMore)
  val typeArgsBlock = token(OpenSquareBracket) ~ typeArgs ~ token(CloseSquareBracket) map (TypeArgsBlock.apply _ tupled)

  val parents = token(Colon) ~ typeExpr.rep(Comma, Cardinality.OneOrMore) map { ParentsBlock.apply _ tupled }

  val returnType = (token(Colon) ~ typeExpr).map(_._2)

  val valDef = keyword("val") ~ ident ~ returnType.opt ~ (token(TokenType.Assign) ~ expr).map(_._2).opt map { x =>
    ValDef(x._1, x._2, x._3, x._4)
  }

  val stmtBlock = token(OpenCurlyBracket) ~ forwardRef(stmt).rep(Cardinality.ZeroOrMore) ~ token(CloseCurlyBracket) map { x =>
    new StmtBlock(x._1, x._2, x._3)
  }

  val enumDef = keyword("enum") ~ ident ~ token(OpenCurlyBracket) ~ ident.rep(Comma, Cardinality.ZeroOrMore) ~ token(CloseCurlyBracket) map { x =>
    EnumDef(x._1, x._2, x._4, x._5)
  }

  val classDef = (keyword("trait") | keyword("class")) ~ ident ~ typaramsBlock.opt ~ paramsBlock.opt ~ parents.opt ~ stmtBlock.opt map { ClassDef.apply _ tupled }

  val funBody = (token(TokenType.Assign) ~ expr).map(x => ExprFunBody(x._2)) | stmtBlock.map(BlockFunBody)

  val funDef = keyword("fun") ~ ident ~ typaramsBlock.opt ~ paramsBlock.opt ~ returnType.opt ~ funBody.opt map {
    FunDef.apply _ tupled
  }

  val stmt: Parser[Stmt] = classDef | valDef | funDef | enumDef | `import` | expr

  val file = stmt.rep(Cardinality.ZeroOrMore).map(File)
}