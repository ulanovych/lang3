package lang.compiler.parsing.impl

import lang.compiler.lexicalanalysis.{Lexer, Token, TokenType}
import lang.compiler.parsing.ParserResult
import lang.compiler.parsing.impl.combinators._

trait Parser[+T] {
  def parse(src: String): ParserResult[T] = {
    val tokens = Lexer.tokenize(src)
    parse(tokens, 0)
  }

  def parse(tokens: Seq[Token], index: Int): ParserResult[T]

  def rep(cardinality: Cardinality) = RepeatedParser(this, None, cardinality)
  def rep(separator: TokenType, cardinality: Cardinality) = RepeatedParser(this, Some(separator), cardinality)
  def rep2(): Parser[Seq[T]] = ???

  def opt = OptParser(this)

  def map[U](f: T => U): Parser[U] = MapParser(this, f)

  def |[U >: T](that: Parser[U]): Parser[U] = OrParser(this, that)

  def * = RepeatedParser(this, None, Cardinality.ZeroOrMore)
  def + = RepeatedParser(this, None, Cardinality.ZeroOrMore)
}