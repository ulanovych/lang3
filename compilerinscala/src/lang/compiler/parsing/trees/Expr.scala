package lang.compiler.parsing.trees

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.trees.visiting.Visitor
import lang.compiler.typechecking.Scope
import lang.compiler.typechecking.types.{Member, TMethod, Type}

trait Stmt extends Node

trait Expr extends Stmt with HasType {
}

case class IntLit(token: Token) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = token
  override def lastToken = token
}

case class StrLit(token: Token) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = token
  override def lastToken = token
}

case class Ident(token: Token) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = token
  override def lastToken = token
}

//case class Assign(name: Token, expr: Expr) extends Expr {
//  override def accept[T](visitor: Visitor[T]) = visitor.visit(this)
//
//  override def setScopeForChildren(scope: Scope): Unit = {
//    expr.scope = scope
//  }
//}
//
case class GetField(expr: Expr, name: Token) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  var field: Member = _

  override def firstToken = expr.firstToken
  override def lastToken = name
}

case class SetField(expr: Expr, name: Token, value: Expr) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = expr.firstToken
  override def lastToken = value.lastToken
}

case class FunCall(expr: Expr, typeArgs: Option[TypeArgsBlock], args: Option[ArgsBlock]) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = expr.firstToken
  override def lastToken = (Seq(expr) ++ typeArgs ++ args).last.lastToken
}

class MethodCall(_expr: Expr, val name: Token, val argsBlock: Option[ArgsBlock]) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  var method: TMethod = _
  
  val expr = childNode(_expr)

  val args = argsBlock.map(_.args).getOrElse(Seq.empty)

  override def firstToken = expr().firstToken
  override def lastToken = (Some(name) ++ argsBlock.map(_.lastToken)).last
}

case class ValDef(keyword: Token, name: Token, _type: Option[Expr], expr: Option[Expr]) extends Stmt {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  val `type` = childNodeOpt(_type)

  override def firstToken = keyword
  override def lastToken = (`type`().map(_.lastToken) ++ expr.map(_.lastToken)).last
}

case class FunDef(keyword: Token, name: Token, typaramsBlock: Option[TyParamBlock], paramsBlock: Option[ParamBlock], _type: Option[Expr], body: Option[FunBody]) extends Stmt {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  val params = paramsBlock.map(_.params).getOrElse(Seq.empty)

  val `type` = childNodeOpt(_type)

  override def firstToken = keyword
  override def lastToken = (Some(name) ++ typaramsBlock.map(_.lastToken) ++ paramsBlock.map(_.lastToken) ++ `type`().map(_.lastToken) ++ body.map(_.lastToken)).last
}

case class File(stmts: Seq[Stmt]) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = stmts.head.firstToken
  override def lastToken = stmts.last.lastToken
}

case class Import(keyword: Token, expr: Expr) extends Stmt {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = keyword
  override def lastToken = expr.lastToken
}

case class ClassDef(
  keyword: Token, name: Token, typaramsBlock: Option[TyParamBlock], paramsBlock: Option[ParamBlock],
  parentsBlock: Option[ParentsBlock], body: Option[StmtBlock])
extends Stmt {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = keyword
  override def lastToken = (Some(name) ++ typaramsBlock.map(_.lastToken) ++ paramsBlock.map(_.lastToken) ++ parentsBlock.map(_.lastToken) ++ body.map(_.lastToken)).last
}

case class EnumDef(keyword: Token, name: Token, members: Seq[Token], rightBrace: Token) extends Stmt {
  override def accept[T](visitor: Visitor[T]): T = {
    visitor.visit(this)
  }

  override def firstToken = keyword
  override def lastToken = rightBrace
}

case class TyParam(name: Token, upperBound: Option[Expr]) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = name
  override def lastToken = (Some(name) ++ upperBound.map(_.lastToken)).last
}

case class Param(name: Token, `type`: Expr) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = name
  override def lastToken = `type`.lastToken
}

case class TupleExpr(leftParen: Token, values: Seq[Expr], rightParen: Token) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = leftParen
  override def lastToken = rightParen
}

case class MethodType(lhs: Expr, rhs: Expr) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = lhs.firstToken
  override def lastToken = rhs.lastToken
}

trait FunBody extends Node {
  val stmts: Seq[Stmt]
}

case class ExprFunBody(expr: Expr) extends FunBody {
  override val stmts: Seq[Stmt] = Seq(expr)

  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = expr.firstToken
  override def lastToken = expr.lastToken
}

case class BlockFunBody(stmtBlock: StmtBlock) extends FunBody {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override val stmts: Seq[Stmt] = stmtBlock.stmts()

  override def firstToken = stmtBlock.firstToken
  override def lastToken = stmtBlock.lastToken
}

case class Lambda(paramBlock: Option[ParamBlock], stmtBlock: StmtBlock) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = (paramBlock.map(_.firstToken) ++ Some(stmtBlock.firstToken)).head
  override def lastToken = stmtBlock.lastToken
}

case class ArgsBlock(leftParen: Token, args: Seq[Expr], rightParen: Token) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = leftParen
  override def lastToken = rightParen
}

case class TypeArgsBlock(leftBracket: Token, typeArgs: Seq[Expr], rightBracket: Token) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = leftBracket
  override def lastToken = rightBracket
}

case class ParamBlock(leftParen: Token, params: Seq[Param], rightParen: Token) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = leftParen
  override def lastToken = rightParen
}

case class TyParamBlock(leftBracket: Token, typarams: Seq[TyParam], rightBracket: Token) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = leftBracket
  override def lastToken = rightBracket
}

class StmtBlock(leftBrace: Token, _stmts: Seq[Stmt], rightBrace: Token) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  val stmts = childNodeSeq(_stmts.toArray)

  override def firstToken = leftBrace
  override def lastToken = rightBrace
}

case class ParentsBlock(keyword: Token, parents: Seq[Expr]) extends Node {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = keyword
  override def lastToken = parents.last.lastToken
}

case class GetLocalVar(name: Token) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = name
  override def lastToken = name
}

case class SetLocalVar(name: Token, value: Expr) extends Expr {
  override def accept[T](visitor: Visitor[T]): T = visitor.visit(this)

  override def firstToken = name
  override def lastToken = value.lastToken
}