package lang.compiler.parsing.trees

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.trees.visiting.Visitor
import lang.compiler.typechecking.Scope

import scala.collection.mutable.ArrayBuffer

trait Node {
  private var _scope: Scope = null
  def scope = _scope
  def scope_=(scope: Scope): Unit = {
    _scope = scope
//    setScopeForChildren(scope)
  }
//  def setScopeForChildren(scope: Scope)
  def accept[T](visitor: Visitor[T]): T

  def firstToken: Token
  def lastToken: Token

  var parent: Node = null

  def replace(that: Node) = parent.childNodes.find(_.replace(this, that)).get

  private val childNodes = ArrayBuffer[ChildNode[Node]]()

  def childNode[T <: Node](node: T) = new ChildNodeX(node)
  def childNodeOpt[T <: Node](node: Option[T]) = new ChildNodeOpt(node)
  def childNodeSeq[T <: Node](nodes: Array[T]) = new ChildNodeSeq(nodes)

  trait ChildNode[T <: Node] {
    def replace(oldNode: T, newNode: T): Boolean
  }

  class ChildNodeX[T <: Node](var value: T) extends ChildNode[T] {
    childNodes += this.asInstanceOf[ChildNodeX[Node]]
    value.parent = Node.this
    def apply(): T = value
    def :=(newChildNode: T): Unit = {
      value.parent = null
      value = newChildNode
      value.parent = Node.this
    }

    override def replace(oldNode: T, newNode: T): Boolean = {
      if (value == oldNode) {
        this := newNode
        true
      } else {
        false
      }
    }
  }

  class ChildNodeOpt[T <: Node](var value: Option[T]) extends ChildNode[T] {
    childNodes += this.asInstanceOf[ChildNode[Node]]
    value.foreach(_.parent = Node.this)
    def apply(): Option[T] = value
    def :=(newChildNode: Option[T]): Unit = {
      value.foreach(_.parent = null)
      value = newChildNode
      value.foreach(_.parent = Node.this)
    }

    override def replace(oldNode: T, newNode: T): Boolean = {
      if (value.map(_ == oldNode).getOrElse(false)) {
        this := Some(newNode)
        true
      } else {
        false
      }
    }
  }

  class ChildNodeSeq[T <: Node](var values: Array[T]) extends ChildNode[T] {
    childNodes += this.asInstanceOf[ChildNode[Node]]
    values.foreach(_.parent = Node.this)
    def apply(): Seq[T] = values
//    def :=(newChildNodes: Array[T]): Unit = {
//      values.foreach(_.parent = null)
//      values = newChildNodes
//      values.foreach(_.parent = Node.this)
//    }

    override def replace(oldNode: T, newNode: T): Boolean = {
      values.indexOf(oldNode) match {
        case -1 => false
        case i =>
          oldNode.parent = null
          values(i) = newNode
          newNode.parent = Node.this
          true
      }
    }
  }
}
