package lang.compiler.parsing.trees.visiting

import lang.compiler.parsing.trees._

trait Visitor[T] {
  def continue(node: Node): T = {
    node.accept(this)
  }

  def visit(lit: IntLit): T
  def visit(lit: StrLit): T
  def visit(ident: Ident): T
  def visit(tuple: TupleExpr): T
//  def visit(assign: Assign): T
  def visit(getField: GetField): T
  def visit(setField: SetField): T
  def visit(call: FunCall): T
  def visit(call: MethodCall): T
  def visit(defn: ClassDef): T
  def visit(defn: EnumDef): T
  def visit(defn: ValDef): T
  def visit(defn: FunDef): T
  def visit(body: FunBody): T
  def visit(lambda: Lambda): T
  def visit(file: File): T
  def visit(imprt: Import): T
  def visit(typaram: TyParam): T
  def visit(param: Param): T
  def visit(mtype: MethodType): T
  def visit(block: ArgsBlock): T
  def visit(block: TypeArgsBlock): T
  def visit(block: ParamBlock): T
  def visit(block: TyParamBlock): T
  def visit(block: StmtBlock): T
  def visit(block: ParentsBlock): T
  def visit(node: GetLocalVar): T
  def visit(node: SetLocalVar): T
}