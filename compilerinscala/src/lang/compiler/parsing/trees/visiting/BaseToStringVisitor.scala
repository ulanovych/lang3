package lang.compiler.parsing.trees.visiting

trait BaseToStringVisitor extends Visitor[Unit] {
  private val builder = new StringBuilder
  private var indentLevel = 0
  private val indentString = "    "

  protected def append(ss: String*) {
    ss.foreach(builder.append)
  }

  protected def appendln() {
    appendln("", 0)
  }

  protected def appendln(s: String) {
    appendln(s, 0)
  }

  protected def appendln(indexDelta: Int) {
    appendln("", indexDelta)
  }

  protected def appendln(s: String, indentDelta: Int) {
    builder.append(s)
    builder.append("\n")
    indentLevel += indentDelta
    var i = 0
    while (i < indentLevel) {
      append(indentString)
      i += 1
    }
  }

  def getResult = {
    builder.toString
  }
}