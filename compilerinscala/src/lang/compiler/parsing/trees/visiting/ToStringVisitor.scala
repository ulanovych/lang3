package lang.compiler.parsing.trees.visiting

import lang.compiler.parsing.trees._
import lang.compiler.util.forEach

class ToStringVisitor extends BaseToStringVisitor {
  override def visit(lit: IntLit): Unit = {
    append(lit.token.value)
  }

  override def visit(setField: SetField): Unit = {
    continue(setField.expr)
    append(".", setField.name.value, " = ")
    continue(setField.value)
  }

  override def visit(lit: StrLit): Unit = {
    append("\"", lit.token.value, "\"")
  }


  override def visit(ident: Ident): Unit = {
    append(ident.token.value)
  }

  override def visit(getField: GetField): Unit = {
    getField.expr.accept(this)
    append(".", getField.name.value)
  }

  override def visit(call: MethodCall): Unit = {
    call.expr().accept(this)
    append(".", call.name.value, "(")
    var i = 0
    val args = call.args
    while(i < args.size - 1) {
      args(i).accept(this)
      i += 1
      append(", ")
    }
    args.lastOption.foreach(continue)
    append(")")
  }

  override def visit(defn: ValDef): Unit = {
    append("val ", defn.name.value, " = ")
    defn.expr.foreach(continue)
  }

  override def visit(file: File): Unit = {
    val stmts = file.stmts
    var i = 0
    while(i < stmts.size - 1) {
      continue(stmts(i))
      i += 1
      appendln()
      appendln()
    }
    stmts.lastOption.foreach(continue)
  }

  override def visit(imprt: Import): Unit = {
    append("import ")
    continue(imprt.expr)
    append(".*")
  }

  override def visit(defn: EnumDef): Unit = {
    appendln(s"enum ${defn.name.value} {", 1)
    appendln(defn.members.map(_.value).mkString(", "), -1)
    append("}")
  }

  override def visit(defn: ClassDef): Unit = {
    append("class ", defn.name.value)
    defn.typaramsBlock.foreach { block =>
      append("[")
      block.typarams.foreach(continue)
      append("]")
    }
    defn.paramsBlock.foreach { block =>
      append("(")
      forEach(block.params) apply continue between append(", ")
      append(")")
    }
    defn.parentsBlock.foreach { block =>
      val parents =
      append(": ")
      forEach(block.parents) apply continue between append(", ")
    }

    defn.body.foreach { block =>
      appendln(" {", 1)
      var i = 0
      forEach(block.stmts()) apply continue between appendln()
      append("}")
    }
  }

  override def visit(typaram: TyParam): Unit = {
    append(typaram.name.value)
    typaram.upperBound.foreach( ub => {
      append(" : ")
      continue(ub)
    })
  }

  override def visit(param: Param): Unit = {
    append(param.name.value, ": ")
    continue(param.`type`)
  }

  override def visit(call: FunCall): Unit = {
    continue(call.expr)

    call.typeArgs.foreach { block =>
      append("[")
      forEach(block.typeArgs) apply continue between append(", ")
      append("]")
    }

    call.args.foreach { block =>
      append("(")
      forEach(block.args) apply continue between append(", ")
      append(")")
    }
  }

  override def visit(defn: FunDef): Unit = {
    append("fun ", defn.name.value)

    defn.typaramsBlock.foreach { block =>
      append("[")
      forEach(block.typarams) apply continue between append(", ")
      append("]")
    }

    defn.paramsBlock.foreach { block =>
      append("(")
      forEach(block.params) apply continue between append(", ")
      append(")")
    }

    defn.`type`().foreach { `type` =>
      append(": ")
      continue(`type`)
    }
    defn.body.foreach(continue)
  }


  override def visit(body: FunBody): Unit = {
    body match {
      case body: ExprFunBody =>
        append(" = ")
        continue(body.expr)
      case body: BlockFunBody =>
        appendln(" {", 1)
        val xs = body.stmts
        var i = 0
        while (i < xs.length - 1) {
          continue(xs(i))
          i += 1
          appendln()
        }
        while (i < xs.length) {
          continue(xs(i))
          i += 1
        }
        appendln(-1)
        append("}")
    }
  }

  override def visit(tuple: TupleExpr): Unit = {
    append("(")
    val xs = tuple.values
    var i = 0
    while (i < xs.length - 1) {
      continue(xs(i))
      i += 1
      append(", ")
    }
    while (i < xs.length) {
      continue(xs(i))
      i += 1
    }
    append(")")
  }

  override def visit(mtype: MethodType): Unit = {
    continue(mtype.lhs)
    append(".")
    continue(mtype.rhs)
  }

  override def visit(lambda: Lambda): Unit = {
    appendln("{", 1)
    forEach(lambda.stmtBlock.stmts()) apply continue between appendln()
    appendln(-1)
    append("}")
  }

  override def visit(block: ArgsBlock): Unit = ???

  override def visit(block: TypeArgsBlock): Unit = ???

  override def visit(block: ParamBlock): Unit = ???

  override def visit(block: TyParamBlock): Unit = ???

  override def visit(block: StmtBlock): Unit = ???

  override def visit(block: ParentsBlock): Unit = ???

  override def visit(node: GetLocalVar): Unit = ???

  override def visit(node: SetLocalVar): Unit = {
    append(node.name.value, " = ")
    continue(node.value)
  }
}