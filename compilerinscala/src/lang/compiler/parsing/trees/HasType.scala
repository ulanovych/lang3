package lang.compiler.parsing.trees

import lang.compiler.typechecking.types.Type

trait HasType {
  private var _type: Type = _

  def `type` = _type

  def `type_=`(`type`: Type): Unit = {
    _type = `type`
  }
}