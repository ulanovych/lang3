package lang.compiler.parsing.pratt

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.ParserResult
import lang.compiler.parsing.trees.Expr

trait PrefixParselet {
  def parse(parser: PrattParser, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr]
}