package lang.compiler.parsing.pratt

import lang.compiler.lexicalanalysis.{Token, TokenType}
import lang.compiler.parsing.impl.Parser
import lang.compiler.parsing.trees.Expr
import lang.compiler.parsing.{ParserError, ParserResult, ParserSuccess}

class PrattParser(
  prefixParselets: Map[TokenType, PrefixParselet],
  infixParselets: Map[TokenType, InfixParselet]
) extends Parser[Expr] {
  override def parse(tokens: Seq[Token], index: Int): ParserResult[Expr] = {
    parseExpr(index, tokens)
  }

  def parseExpr(index: Int, tokens: Seq[Token], precedence: Int = 0): ParserResult[Expr] = {
    var idx = index
    var token = tokens(idx)
    idx +=1
    val prefixParselet = prefixParselets.get(token.`type`)
    if (prefixParselet.isEmpty) {
      return ParserError("no prefix parselet for " + token, token)
    }
    val res = prefixParselet.get.parse(this, token, tokens, idx)
    if (res.isError) return res
    idx = res.asInstanceOf[ParserSuccess[_]].index
    var left = res.get
    while (precedence < infixParselets.get(tokens(idx).`type`).map(_.precedence).getOrElse(0)) {
      token = tokens(idx)
      idx += 1
      val infixParselet = infixParselets(token.`type`)
      infixParselet.parse(this, left, token, tokens, idx) match {
        case ParserSuccess(expr, i, _) =>
          left = expr
          idx = i
        case e: ParserError =>
          return e
      }
    }
    ParserSuccess(left, idx)
  }
}