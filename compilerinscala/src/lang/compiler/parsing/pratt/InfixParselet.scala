package lang.compiler.parsing.pratt

import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.ParserResult
import lang.compiler.parsing.trees.Expr

trait InfixParselet {
  def parse(parser: PrattParser, left: Expr, token: Token, tokens: Seq[Token], index: Int): ParserResult[Expr]
  val precedence: Int
}