package lang.compiler.classpathanalysis

import scala.collection.mutable
import lang.compiler.typechecking.types._

case class AnalyzedClasspath(entries: Seq[AnalyzedJar]) {
  def build(): TPackage = {
    val classes = entries.flatMap(_.classes)

    val map = mutable.HashMap[String, TClass]()

    val set = mutable.HashSet[String]()

    classes.foreach { c =>
      val fullName = c.fullName.replace('/', '.')
      map.put(c.fullName, new TClass(fullName))
    }
//    classes.foreach(c => map.put(c.fullName, new Class(c.fullName.split('.').last, c.fullName)))

    def convert(tpe: CAType): Type = tpe match {
      case CAByte => TByte
      case CAShort => TShort
      case CAInt => TInt
      case CALong => TLong
      case CAChar => TChar
      case CAFloat => TFloat
      case CADouble => TDouble
      case CABoolean => TBoolean
      case CAVoid => TVoid
      case CAArrayType(tpe, arity) => TArray(convert(tpe), arity)
      case CARef(s) =>
        set += s
        map.get(s) match {
          case Some(klass) => klass
          case None => {
            throw new Exception(s"Class $s not found")

          }
      }
    }

    val rootPackage = new TPackage("<root>")

    classes.foreach { c =>
      val klass = map.get(c.fullName).get
      var pkg = rootPackage
      klass.fqName.split('.').dropRight(1).foreach(s => {
        pkg.getPackage(s) match {
          case Some(p1) =>
            pkg = p1
          case None =>
            val p1 = new TPackage(s)
            pkg.addPackage(p1)
            pkg = p1
        }
      })

      pkg.addClass(klass)
    }

    classes.foreach { c =>
      val klass = rootPackage.getClassByFullName(c.fullName.split('/')).get
      c.fields.foreach { f =>
        klass.addField(Field(f.name, convert(f.`type`)))
      }

      c.methods foreach { m =>
        klass.addMethod(TMethod(m.name, convert(m.returnType), m.parameterTypes.map(convert)))
      }

      c.staticFields.foreach { f =>
        klass.addStaticField(StaticField(f.name, convert(f.`type`)))
      }
    }
    println(set.size)
    rootPackage
  }

}
