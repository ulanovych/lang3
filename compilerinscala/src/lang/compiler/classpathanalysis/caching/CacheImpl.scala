package lang.compiler.classpathanalysis.caching
import java.io._
import java.net.{ConnectException, Socket}

import lang.compiler.typechecking.types.TPackage
import lang.compiler.util.measureTime
import org.apache.commons.io.IOUtils

class CacheImpl(port: Int) extends Cache {
  def connect: Option[Socket] = {
    try {
      Some(connect_!)
    } catch {
      case e: ConnectException => None
    }
  }

  def connect_! : Socket = {
    new Socket("localhost", port)
  }

  def launch: Socket = {
    val time = System.currentTimeMillis()
    val cp = System.getProperty("java.class.path")
    val description = "Classpath analyzer cache server"
    val mainClass = "lang.compiler.classpathanalysis.caching.Server"
    Runtime.getRuntime.exec(s"""java -Dprocess.description="$description" -Dport=$port -classpath "$cp" $mainClass""")
    var socket: Option[Socket] = None
    while (socket.isEmpty && (System.currentTimeMillis() - time < 5000)) {
      socket = connect
    }
    socket.getOrElse(connect_!)
  }

  override def get(path: Seq[File]): Option[TPackage] = {
    var socket: Socket = null

    var sis: InputStream = null
    var sos: OutputStream = null

    var ois: ObjectInputStream = null
    var oos: ObjectOutputStream = null

    try {
      socket = connect.getOrElse(launch)

      sis = socket.getInputStream
      sos = socket.getOutputStream

      oos = new ObjectOutputStream(sos)

      oos.writeObject(Get(path))

      val bytes = IOUtils.toByteArray(sis)

      ois = new ObjectInputStream(new ByteArrayInputStream(bytes))

      val (obj, readTime) = measureTime(ois.readObject())
      println(s"Read time: $readTime")
      obj match {
        case pkg: TPackage =>
          Some(pkg)
      }
    } finally {
      if (ois != null) ois.close()
      if (oos != null) oos.close()
      if (sos != null) sos.close()
      if (sis != null) sis.close()
      if (socket != null) socket.close()
    }
  }

  override def put(path: Seq[File], pkg: TPackage): Unit = {}
}
