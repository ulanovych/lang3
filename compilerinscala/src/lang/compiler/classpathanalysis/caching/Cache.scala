package lang.compiler.classpathanalysis.caching

import java.io.File

import lang.compiler.typechecking.types.TPackage

trait Cache {
  def get(path: Seq[File]): Option[TPackage]
  def put(path: Seq[File], pkg: TPackage): Unit
}
