package lang.compiler.classpathanalysis.caching
import java.io.File

import lang.compiler.typechecking.types.TPackage

object NoCache extends Cache {
  override def get(path: Seq[File]): Option[TPackage] = None

  override def put(path: Seq[File], pkg: TPackage): Unit = {}
}
