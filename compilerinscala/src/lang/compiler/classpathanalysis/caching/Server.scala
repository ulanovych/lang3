package lang.compiler.classpathanalysis.caching

import java.io.{ByteArrayOutputStream, File, ObjectInputStream, ObjectOutputStream}
import java.net.ServerSocket

import lang.compiler.classpathanalysis.ClasspathAnalyzer
import lang.compiler.typechecking.types.TPackage
import lang.compiler.util.measureTime

import scala.collection.mutable

trait CacheCommand extends Serializable

case class Get(path: Seq[File]) extends CacheCommand
case class Put() extends CacheCommand

object Server extends App {
  val serverSocket = new ServerSocket(System.getProperty("port").toInt)

  val map = mutable.HashMap[Long, Array[Byte]]()

  while(true) {
    val clientSocket = serverSocket.accept()

    val sis = clientSocket.getInputStream
    val sos = clientSocket.getOutputStream

    val ois = new ObjectInputStream(sis)

    val cmd = ois.readObject()

    cmd match {
      case Get(classpath) =>
        val cpHash = ClasspathAnalyzer.hashClassPath(classpath)
        val bytes = map.getOrElseUpdate(cpHash, {
          val pkg = ClasspathAnalyzer.analyzeClasspath(classpath)
          val baos = new ByteArrayOutputStream()
          val stream = new ObjectOutputStream(baos)
          stream.writeObject(pkg)
          stream.close()
          val bytes = baos.toByteArray
          baos.close()
          bytes
        })
        sos.write(bytes)
      case Put() =>
    }

    ois.close()

    sis.close()
    sos.close()

    clientSocket.close()
  }
}