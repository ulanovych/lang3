package lang.compiler.classpathanalysis

sealed trait CAType extends Serializable {

}

case class CAField(name: String, `type`: CAType) {

}

case class CAMethod(name: String, returnType: CAType, parameterTypes: Seq[CAType])

case class CAClass(
  fullName: String,
  fields: Seq[CAField],
  methods: Seq[CAMethod],
  staticFields: Seq[CAStaticField],
  staticMethods: Seq[CAStaticMethod]
)

case class CAStaticField(name: String, `type`: CAType)

class CAStaticMethod

class CAConstructor

case class CAArrayType(`type`: CAType, arity: Int) extends CAType

sealed trait CAPrimitiveType extends CAType

object CAByte extends CAPrimitiveType
object CAShort extends CAPrimitiveType
object CAInt extends CAPrimitiveType
object CALong extends CAPrimitiveType
object CAChar extends CAPrimitiveType
object CAFloat extends CAPrimitiveType
object CADouble extends CAPrimitiveType
object CABoolean extends CAPrimitiveType
object CAVoid extends CAPrimitiveType
case class CARef(s: String) extends CAType
