package lang.compiler.classpathanalysis

import java.io.{File, InputStream}
import java.util.jar.JarFile
import java.util.zip.ZipInputStream

import lang.compiler.classpathanalysis.caching.Cache
import lang.compiler.typechecking.types.TPackage
import org.objectweb.asm.ClassReader

import scala.collection.mutable.ArrayBuffer

object ClasspathAnalyzer {
  def analyzeClasspath(classpath: Seq[File], cache: Cache): TPackage = {
    val hash = hashClassPath(classpath)
    cache.get(classpath).getOrElse {

      val pkg = analyzeClasspath(classpath)
      cache.put(classpath, pkg)
      pkg
    }
  }

  def hashClassPath(path: Seq[File]): Long = {
    path.map(hashEntry).sum
  }

  def hashEntry(entry: File): Long = {
    if (entry.getName.endsWith(".jar")) {
      hashJar(entry)
    } else if (entry.isDirectory) {
      hashDir(entry)
    } else {
      ???
    }
  }

  def hashJar(jar: File): Long = {
    jar.lastModified()
  }

  def hashDir(dir: File): Long = {
    ???
  }

  def analyzeClasspath(classpath: Seq[File]): TPackage = {
    val entries = classpath.map(analyzeEntry)
    AnalyzedClasspath(entries).build()
  }

  def analyzeEntry(file: File): AnalyzedJar = {
    scanJar(file)
  }

  def scanJar(file: File): AnalyzedJar = {
    val jarFile = new JarFile(file)
    val entries = jarFile.entries()
    val classes = ArrayBuffer[CAClass]()
    while (entries.hasMoreElements) {
      val element = entries.nextElement()
      if (element.getName.endsWith(".class")) {
        val stream = jarFile.getInputStream(element)
        val klass = analyzeClassFile(stream)
        classes += klass
        stream.close()
      }
    }
    AnalyzedJar(classes)
  }

  def analyzeClassFile(stream: InputStream): CAClass = {
    val reader = new ClassReader(stream)
    val visitor = new ClassAnalyzerVisitor
    reader.accept(visitor, 0)
    visitor.getResult
  }

  def analyzeClass(klass: Class[_]): CAClass = ???
}