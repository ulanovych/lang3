package lang.compiler.classpathanalysis

import org.objectweb.asm.{ClassVisitor, FieldVisitor, MethodVisitor, Opcodes}

import scala.collection.mutable.ArrayBuffer

class ClassAnalyzerVisitor extends ClassVisitor(Opcodes.ASM5) {
  private val constructors = ArrayBuffer[CAConstructor]()
  private val fields = ArrayBuffer[CAField]()
  private val methods = ArrayBuffer[CAMethod]()
  private val staticFields = ArrayBuffer[CAStaticField]()
  private val staticMethods = ArrayBuffer[CAStaticMethod]()
  private var name: String = _

  override def visit(version: Int, access: Int, name: String, signature: String, superName: String, interfaces: Array[String]): Unit = {
    this.name = name
//    println(s"version: $version, access: $access, name: $name, signature: $signature, superName: $superName, interfaces: ${interfaces.toList}")
  }

  override def visitField(access: Int, name: String, desc: String, signature: String, value: scala.Any): FieldVisitor = {
    val static = is(access, Opcodes.ACC_STATIC)
    val tpe = parseTypeDescriptor(desc)
    if (static) {
      staticFields += CAStaticField(name, tpe)
//      println(s"Static field - access: $access, name: $name, type: $tpe, desc: $desc, signature: $signature, value: $value")
    } else {
      fields += CAField(name, tpe)
//      println(s"Field - access: $access, name: $name, type: $tpe, desc: $desc, signature: $signature, value: $value")
    }
    null
  }

  override def visitMethod(access: Int, name: String, desc: String, signature: String, exceptions: Array[String]): MethodVisitor = {
    val exs = if (exceptions eq null) List() else exceptions.toList
    val static = is(access, Opcodes.ACC_STATIC)
    if (static) {
//      println(s"Static method - access: $access, name: $name, desc: $desc, signature: $signature, exceptions: $exs")
    } else {
      val (returnType, parameterTypes) = parseMethodDescriptor(desc)
      val method = CAMethod(name, returnType, parameterTypes)
      methods += method
//      println(s"Method - access: $access, name: $name, desc: $desc, signature: $signature, exceptions: $exs")
    }
    null
  }

  def is(flags: Int, flag: Int) = (flags & flag) != 0

  def parseTypeDescriptor(desc: String): CAType = {
    parseTypeDescriptor(desc, 0)._1
  }

  def parseMethodDescriptor(desc: String): (CAType, Seq[CAType]) = {
    var i = 1 // skip opening paren
    val parameterTypes = ArrayBuffer[CAType]()
    while (desc(i) != ')') {
      val (tpe, j) = parseTypeDescriptor(desc, i)
      parameterTypes += tpe
      i = j
    }
    i += 1 // skip closing paren
    val (returnType, _) = parseTypeDescriptor(desc, i)
    (returnType, parameterTypes)
  }

  def parseTypeDescriptor(desc: String, index: Int): (CAType, Int) = {
    var i = index
    desc(i) match {
      case 'B' => (CAByte, i + 1)
      case 'S' => (CAShort, i + 1)
      case 'I' => (CAInt, i + 1)
      case 'J' => (CALong, i + 1)
      case 'C' => (CAChar, i + 1)
      case 'F' => (CAFloat, i + 1)
      case 'D' => (CADouble, i + 1)
      case 'Z' => (CABoolean, i + 1)
      case 'V' => (CAVoid, i + 1)
      case 'L' =>
        while (desc(i) != ';') {
          i += 1
        }
        (CARef(desc.substring(index + 1, i)), i + 1)
      case '[' =>
        i = i + 1
        var arity = 1
        while (desc(i) == '[') {
          arity += 1
          i += 1
        }
        val (tpe, idx) = parseTypeDescriptor(desc, i)
        (CAArrayType(tpe, arity), i)
    }
  }

  def getResult = CAClass(name, fields, methods, staticFields, staticMethods)

}
