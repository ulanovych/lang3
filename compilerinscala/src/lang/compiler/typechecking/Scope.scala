package lang.compiler.typechecking

import lang.compiler.typechecking.types.TPackage

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class Scope {
  private val storage = mutable.HashMap[String, Any]()
  private val packages = ArrayBuffer[TPackage]()

  def get(key: String): Seq[Any] = {
    (storage.get(key) ++ packages.flatMap(_.getMembers(key))).toList
  }

  def put(key: String, value: Any): Option[Any] = storage.put(key, value)

  def add(pkg: TPackage): Unit = {
    packages += pkg
  }
}