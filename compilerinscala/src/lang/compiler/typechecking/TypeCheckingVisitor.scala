package lang.compiler.typechecking

import lang.compiler.parsing.trees._
import lang.compiler.parsing.trees.visiting.Visitor
import lang.compiler.typechecking.types._

import scala.collection.mutable.ArrayBuffer

class TypeCheckingVisitor(cp: TPackage) extends Visitor[Unit] {
  private val errors = ArrayBuffer[TypeError]()

  def getErrors: Seq[TypeError] = errors.toList

  override def visit(lit: IntLit): Unit = ???

  override def visit(lit: StrLit): Unit = {
    lit.`type` = cp.getClassByFullName("java.lang.String").get
  }

  override def visit(ident: Ident): Unit = {
    val token = ident.token
    val name = token.value
    val xs = ident.scope.get(name)
    xs.length match {
      case 0 =>
        errors += new TypeError(s"Symbol $name not found", token)
      case x if x > 1 =>
        errors += new TypeError(s"Overloading not supported: $xs", token)
      case 1 =>
        xs.head match {
          case pkg: TPackage =>
            ident.`type` = pkg
          case cl: TClass =>
            ident.`type` = cl
        }
    }
  }

  override def visit(getField: GetField): Unit = {
    val lhs = getField.expr
    continue(lhs)
    lhs.`type` match {
      case null =>
        errors += new TypeError("No type lhs", getField.name)
      case _ =>
        lhs.`type`.getMember(getField.name.value) match {
          case Some(pkg: TPackage) =>
            getField.`type` = pkg
          case Some(klass: TClass) =>
            getField.`type` = klass
          case Some(field: StaticField) =>
            getField.`type` = field.`type`
            getField.field = field
        }
    }
    if (lhs.`type` != null) {

    }
  }

  override def visit(setField: SetField): Unit = ???

  override def visit(call: MethodCall): Unit = {
    val lhs = call.expr()
    continue(lhs)
    lhs.`type` match {
      case klass: TClass =>
        val name = call.name.value
        call.args.foreach(continue)
        val parameterTypes = call.args.map(_.`type`)
        val overloads = klass.getMethods(name)
        overloads.filter(_.parameterTypes == parameterTypes) match {
          case xs if xs.isEmpty =>
            val msg = s"""Class $klass has no method $name(${parameterTypes.mkString(", ")}), available methods: $overloads"""
            throw new Exception(msg)
          case xs if xs.length == 1 =>
            call.`type` = xs.head.returnType
            call.method = xs.head
          case xs => throw new Exception("Overloading not supported")
        }
      case null =>
    }
  }

  override def visit(defn: ValDef): Unit = ???

  override def visit(file: File): Unit = {
    file.stmts.foreach(continue)
  }

  override def visit(imprt: Import): Unit = {
    continue(imprt.expr)
    imprt.expr.`type` match {
      case pkg: TPackage => imprt.scope.add(pkg)
    }
  }

  override def visit(defn: EnumDef): Unit = ???

  override def visit(defn: ClassDef): Unit = ???

  override def visit(typaram: TyParam): Unit = ???

  override def visit(param: Param): Unit = ???

  override def visit(call: FunCall): Unit = ???

  override def visit(defn: FunDef): Unit = ???

  override def visit(tuple: TupleExpr): Unit = ???

  override def visit(mtype: MethodType): Unit = ???

  override def visit(body: FunBody): Unit = ???

  override def visit(lambda: Lambda): Unit = ???

  override def visit(block: ArgsBlock): Unit = ???

  override def visit(block: TypeArgsBlock): Unit = ???

  override def visit(block: ParamBlock): Unit = ???

  override def visit(block: TyParamBlock): Unit = ???

  override def visit(block: StmtBlock): Unit = ???

  override def visit(block: ParentsBlock): Unit = ???

  override def visit(node: GetLocalVar): Unit = ???

  override def visit(node: SetLocalVar): Unit = ???
}
