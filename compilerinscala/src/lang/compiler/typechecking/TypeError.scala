package lang.compiler.typechecking

import lang.compiler.CompilationError
import lang.compiler.lexicalanalysis.Token
import lang.compiler.parsing.trees.Node

class TypeError(msg: String, firstToken: Token, lastToken: Token) extends CompilationError(firstToken, lastToken) {
  def this(msg: String, token: Token) = this(msg, token, token)
  def this(msg: String, node: Node) = this(msg, node.firstToken, node.lastToken)
  override def getMessage: String = s"${getClass.getSimpleName}: $msg"
}