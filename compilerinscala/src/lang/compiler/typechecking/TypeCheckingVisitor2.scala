package lang.compiler.typechecking

import lang.compiler.interpretation.interpret
import lang.compiler.parsing.trees._
import lang.compiler.parsing.trees.visiting.Visitor
import lang.compiler.typechecking.errors.{SymbolNotFound, TypeMismatch}
import lang.compiler.typechecking.types._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class TypeCheckingVisitor2(var scope: Scope2) extends Visitor[Unit] {
  private val errors = ArrayBuffer[TypeError]()
  private var typeScope = new Scope2
  private val stack = mutable.Stack[Any](new TPackage("<root>"))
  private def enclosing = stack.top


  def getErrors = errors
  def getPackage = stack.top.asInstanceOf[TPackage]
  def getScope = scope

  def error(msg: String, node: Node): Unit = {
    errors += new TypeError(msg, node)
  }

  def error(e: TypeError): Unit = {
    errors += e
  }

  override def visit(lit: IntLit): Unit = {
    scope.get("Int") match {
      case Some(t: Type) =>
        lit.`type` = t
      case other =>
        error(s"Expected Type, got $other", lit)
    }
  }

  override def visit(lit: StrLit): Unit = ???

  override def visit(ident: Ident): Unit = {
    scope.get(ident.token.value) match {
      case Some(thing) => thing match {
        case local: TLocalVar =>
          val node = GetLocalVar(ident.token)
          node.`type` = local.`type`
          ident.replace(node)
      }
      case None =>
        error(SymbolNotFound(ident.token.value, ident))
    }
  }

  override def visit(tuple: TupleExpr): Unit = ???

  override def visit(getField: GetField): Unit = ???

  override def visit(setField: SetField): Unit = ???

  override def visit(call: FunCall): Unit = ???

  override def visit(call: MethodCall): Unit = {
    continue(call.expr())
    call.expr().`type` match {
      case klass: TClass =>
        val methodName = call.name.value
        val methods = klass.getMethods(methodName)
        methods.length match {
          case 0 =>
            errors += new TypeError(s"Type $klass has no method $methodName", call.name)
          case x if x > 1 =>
            errors += new TypeError(s"Type $klass has many methods $methodName", call.name)
          case 1 =>
            val method = methods.head
            val args = call.args
            if (args.length != method.parameterTypes.length) {
              errors += new TypeError(s"Expected ${method.parameterTypes.length} arguments, got ${args.length} arguments", call.name)
            } else {
              var i = 0
              var thereWereErrors = false
              while (i < args.length) {
                val arg = args(i)
                val paramType = method.parameterTypes(i)
                continue(arg)
                if (!arg.`type`.isSubtypeOf(paramType)) {
                  errors += new TypeError(s"Expected $paramType, got ${arg.`type`}", arg)
                  thereWereErrors = true
                }
                i += 1
              }
              if (!thereWereErrors) {
                call.method = method
                call.`type` = method.returnType
              }
            }
        }
      case any =>
        errors += new TypeError(s"Don't know how to call methods on $any", call.name)
    }
  }

  override def visit(defn: ClassDef): Unit = {
    val klass = new TClass(defn.name.value)
//    enclosing match {
//      case pkg: TPackage =>
//        pkg.addClass(klass)
//    }
    scope.put(klass.simpleName, klass)
    typeScope.put(klass.simpleName, TLocalVar(klass.simpleName, klass))
//    stack.push(klass)
    defn.body.foreach { block =>
      block.stmts().foreach(continue)
    }
//    stack.pop()
  }

  override def visit(defn: EnumDef): Unit = ???

  // expr is by name, cause it can be changed during type checking, e.g. Ident to GetLocalVar
  def evalType(expr: => Expr): Type = {
    val (_, ers) = typeCheck(expr, typeScope)
    if (ers.nonEmpty) {
      errors ++= ers
      null
    } else {
      interpret(expr, scope) match {
        case t: Type =>
          t
        case other =>
          error(s"Expected type, got $other", expr)
          null
      }
    }
  }

  override def visit(defn: ValDef): Unit = {
    defn.expr match {
      case Some(expr) =>
        continue(expr)
        defn.`type`() match {
          case Some(_) =>
            evalType(defn.`type`().get) match {
              case t: Type =>
                if (!expr.`type`.isSubtypeOf(t)) {
                  error(TypeMismatch(t, expr.`type`, expr))
                } else {
                  scope.put(defn.name.value, TLocalVar(defn.name.value, t))
                }
              case other => error(s"Expected type, got $other", defn.`type`().get)
            }
          case None => expr.`type` match {
            case t: Type =>
              scope.put(defn.name.value, TLocalVar(defn.name.value, t))
          }

        }
    }
  }

  override def visit(defn: FunDef): Unit = {
    val paramTypes = ArrayBuffer[Type]()
    defn.params.foreach { param =>
      continue(param.`type`)
      interpret(param.`type`, scope) match {
        case t: Type => paramTypes += t
        case other =>
          error(s"Expected type, got $other", param.`type`)
      }
    }

    val returnType = evalType(defn.`type`().get) match {
      case t: Type => Some(t)
      case other =>
        error(s"Expected type, got $other", defn.`type`().get)
        None
    }
    returnType match {
      case Some(t) if paramTypes.length == defn.params.length =>
        stack.top match {
          case klass: TClass =>
            val method = TMethod(defn.name.value, t, paramTypes)
            klass.addMethod(method)
          case pkg: TPackage =>
            val function = TFunction(defn.name.value, t, paramTypes)
            pkg.addFunction(function)
        }
      case None =>
        error("Ignoring due to errors above", defn)
    }

    defn.body.foreach { body =>
      body.stmts.foreach(continue)
    }
  }

  override def visit(body: FunBody): Unit = ???

  override def visit(lambda: Lambda): Unit = ???

  override def visit(file: File): Unit = {
    file.stmts.foreach(continue)
  }

  override def visit(imprt: Import): Unit = ???

  override def visit(typaram: TyParam): Unit = ???

  override def visit(param: Param): Unit = {
    continue(param.`type`)
  }

  override def visit(mtype: MethodType): Unit = ???

  override def visit(block: ArgsBlock): Unit = ???

  override def visit(block: TypeArgsBlock): Unit = ???

  override def visit(block: ParamBlock): Unit = ???

  override def visit(block: TyParamBlock): Unit = ???

  override def visit(block: StmtBlock): Unit = ???

  override def visit(block: ParentsBlock): Unit = ???

  override def visit(node: GetLocalVar): Unit = ???

  override def visit(node: SetLocalVar): Unit = ???
}
