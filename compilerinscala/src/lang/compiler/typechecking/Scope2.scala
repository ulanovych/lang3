package lang.compiler.typechecking

import scala.collection.mutable

class Scope2 {
  private val storage = mutable.HashMap[String, Any]()

  def get(key: String): Option[Any] = {
    storage.get(key)
  }

  def put(key: String, value: Any): Unit = storage.put(key, value)
}
