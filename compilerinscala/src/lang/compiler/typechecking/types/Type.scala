package lang.compiler.typechecking.types

trait Type extends Serializable {
  def getMember(key: String): Option[Member]
  def isSubtypeOf(that: Type): Boolean = this == that
}

trait JavaPrimitiveType extends Type {
  override def getMember(key: String): Option[Member] = None
}

object TByte extends JavaPrimitiveType
object TShort extends JavaPrimitiveType
object TInt extends JavaPrimitiveType
object TLong extends JavaPrimitiveType
object TChar extends JavaPrimitiveType
object TFloat extends JavaPrimitiveType
object TDouble extends JavaPrimitiveType
object TBoolean extends JavaPrimitiveType
object TVoid extends JavaPrimitiveType

case class TArray(`type`: Type, arity: Int) extends Type {
  override def getMember(key: String): Option[Member] = None
}