package lang.compiler.typechecking.types

case class Field(name: String, `type`: Type) extends Member

case class TMethod(name: String, returnType: Type, parameterTypes: Seq[Type]) extends Member
case class TFunction(name: String, returnType: Type, parameterTypes: Seq[Type]) extends Member

case class StaticField(name: String, `type`: Type) extends Member

case class StaticMethod(name: String, returnType: Type, parameterTypes: Seq[Type]) extends Member

case class TLocalVar(name: String, `type`: Type)