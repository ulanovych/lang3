package lang.compiler.typechecking.types

import scala.collection.mutable

class TPackage(val simpleName: String) extends Type with Member {
  private val packages = mutable.HashMap[String, TPackage]()
  private val classes = mutable.HashMap[String, TClass]()
  private val functions = mutable.HashMap[String, TFunction]()

  override def getMember(key: String): Option[Member] = {
    (packages.get(key), classes.get(key)) match {
      case (Some(pkg), None) => Some(pkg)
      case (None, Some(klass)) => Some(klass)
      case (None, None) => None
    }
  }

  def getMembers(name: String): Seq[Member] = {
    (packages.get(name) ++ classes.get(name)).toList
  }

  def getPackages = packages.values

  def getClasses = classes.values

  def getClassBySimpleName(name: String): Option[TClass] = {
    classes.get(name)
  }

  def getClassByFullName(name: String): Option[TClass] = {
    getClassByFullName(name.split('.'))
  }

  def getClassByFullName(name: Seq[String]): Option[TClass] = {
    if (name.length == 1) {
      getClassBySimpleName(name.head)
    } else {
      packages.get(name.head).flatMap(_.getClassByFullName(name.drop(1)))
    }
  }

  def getPackage(name: String): Option[TPackage] = packages.get(name)

  def addPackage(pkg: TPackage): Unit = {
    packages.put(pkg.simpleName, pkg).asInstanceOf[None.type]
  }

  def addClass(klass: TClass): Unit = {
    classes.put(klass.simpleName, klass).asInstanceOf[None.type]
  }

  def addFunction(fun: TFunction): Unit = {
    functions.put(fun.name, fun).asInstanceOf[None.type]
  }

  override def toString: String = s"Package($simpleName)"
}
