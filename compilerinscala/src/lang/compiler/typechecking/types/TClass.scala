package lang.compiler.typechecking.types

import java.io.ObjectInputStream

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class TClass(val fqName: String) extends Type with Member {
  private val fields = mutable.HashMap[String, Field]()
  private val methods = mutable.HashMap[String, ArrayBuffer[TMethod]]()
  private val staticFields = mutable.HashMap[String, StaticField]()
  private val staticMethods = mutable.HashMap[String, StaticMethod]()

  val simpleName = fqName.split('.').last

  TClass.counter += 1

  override def getMember(key: String): Option[Member] = {
    staticFields.get(key)
  }

  def getField(name: String): Option[Field] = {
    fields.get(name)
  }

  def getMethods = methods.flatMap(_._2)

  def getMethod(name: String, returnType: Type, parameterTypes: Seq[Type]): Option[TMethod] = {
    methods.get(name) flatMap { methods =>
      methods.find { method =>
        method.returnType == returnType && method.parameterTypes == parameterTypes
      }
    }
  }

  def getMethods(name: String): Seq[TMethod] = {
    methods.getOrElse(name, Seq.empty)
  }

  def getStaticField(name: String): Option[StaticField] = {
    staticFields.get(name)
  }

  def getFields = fields.values

  def addField(field: Field): Unit = {
    fields.put(field.name, field)
  }

  def addMethod(method: TMethod): Unit = {
    val overloads = methods.getOrElseUpdate(method.name, ArrayBuffer())
    overloads += method
  }

  def addStaticField(field: StaticField): Unit = {
    staticFields.put(field.name, field)
  }

  def addStaticMethod(method: StaticMethod): Unit = {
    staticMethods.put(method.name, method)
  }

  override def toString: String = s"TClass($fqName)"
}

object TClass {
  var counter = 0
}