package lang.compiler.typechecking.errors

import lang.compiler.parsing.trees.Node
import lang.compiler.typechecking.TypeError
import lang.compiler.typechecking.types.Type

case class TypeMismatch(expected: Type, actual: Type, node: Node) extends TypeError(s"expected $expected, got $actual", node)

case class SymbolNotFound(symbol: String, node: Node) extends TypeError(s"$symbol", node)