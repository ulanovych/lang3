package lang.compiler.typechecking

import java.io.File

import lang.compiler.classpathanalysis.ClasspathAnalyzer
import lang.compiler.classpathanalysis.caching.NoCache
import lang.compiler.typechecking.types.TPackage
import lang.compiler.util.measureTime

object TestClasspathHolder {
  lazy val path: TPackage = {
    val javaHome = System.getProperty("java.home")

    val rtJar = new File(javaHome, "lib/rt.jar")
    val jceJar = new File(javaHome, "lib/jce.jar")

    val (pkg, scanTime) = measureTime {
      ClasspathAnalyzer.analyzeClasspath(Seq(rtJar, jceJar), NoCache)
    }
    println(s"TestClasspathHolder scan time: $scanTime")
    pkg
  }
}
