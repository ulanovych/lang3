package lang.compiler.typechecking

import lang.compiler.parsing.trees.Node

object typeCheck {
  def apply[T <: Node](node: T, scope: Scope2): (T, Seq[TypeError]) = {
    val visitor = new TypeCheckingVisitor2(scope)
    visitor.continue(node)
    val errors = visitor.getErrors
    (node, errors)
  }
}
