package lang.compiler.typechecking

import lang.compiler.parsing.trees.{Expr, Node}
import lang.compiler.typechecking.types.TPackage

object TypeChecker {
  def typeCheck(node: Node, cp: TPackage): Seq[TypeError] = {
    val visitor = new TypeCheckingVisitor(cp)
    node.accept(visitor)
    visitor.getErrors
  }

  def typeCheck2(node: Node): Seq[TypeError] = {
    val visitor = new TypeCheckingVisitor2(new Scope2)
    node.accept(visitor)
    visitor.getErrors
  }
}