package lang.compiler.util

object forEach {
  def apply[T](xs: Seq[T]) = new {
    def apply(f: T => Unit) = new {
      def between(g: => Unit) = {
        var i = 0
        while(i < xs.length - 1) {
          f(xs(i))
          g
          i += 1
        }
        while(i < xs.length) {
          f(xs(i))
          i += 1
        }
      }
    }
  }
}