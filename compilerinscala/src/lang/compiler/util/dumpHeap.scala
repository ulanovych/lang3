package lang.compiler.util

import java.lang.management.ManagementFactory

import com.sun.management.HotSpotDiagnosticMXBean

object dumpHeap {
  def apply(fileName: String, live: Boolean): Unit = {
    bean.dumpHeap(fileName, live)
  }

  lazy val bean: HotSpotDiagnosticMXBean = {
    val server = ManagementFactory.getPlatformMBeanServer
    ManagementFactory.newPlatformMXBeanProxy(server, "com.sun.management:type=HotSpotDiagnostic", classOf[HotSpotDiagnosticMXBean])
  }
}
