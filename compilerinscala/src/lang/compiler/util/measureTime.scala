package lang.compiler.util

object measureTime {
  def apply[T](f: => T): (T, Long) = {
    val before = System.currentTimeMillis()
    val result = f
    val after = System.currentTimeMillis()
    (result, after - before)
  }
}