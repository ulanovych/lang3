package lang.compiler.util

import java.io.InputStream

object using {
  def apply[T](stream: InputStream)(f: InputStream => T): T = {
    val result = try {
      f(stream)
    } finally {
      stream.close()
    }
    result
  }
}