package lang.compiler.codegeneration.js

import lang.compiler.parsing.trees.Node

object JsCodeGenerator {
  def generate(node: Node): String = {
    val visitor = new JsCodeGeneratingVisitor
    node.accept(visitor)
    visitor.getResult
  }
}
