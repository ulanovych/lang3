package lang.compiler.codegeneration.js

import lang.compiler.parsing.trees._
import lang.compiler.parsing.trees.visiting.BaseToStringVisitor
import lang.compiler.util.forEach

class JsCodeGeneratingVisitor extends BaseToStringVisitor{
  override def visit(lit: IntLit): Unit = ???

  override def visit(lit: StrLit): Unit = {
    append("\"", lit.token.value, "\"")
  }

  override def visit(ident: Ident): Unit = {
    append(ident.token.value)
  }

  override def visit(tuple: TupleExpr): Unit = ???

  override def visit(getField: GetField): Unit = {
    continue(getField.expr)
    append(".", getField.name.value)
  }

  override def visit(setField: SetField): Unit = ???

  override def visit(call: FunCall): Unit = {
    continue(call.expr)
    append("(")
    call.args.map(_.args).foreach { args =>
      forEach(args) apply continue between {
        append(", ")
      }
    }
    append(")")
  }

  override def visit(call: MethodCall): Unit = {
    continue(call.expr())
    append(".", call.name.value, "(")
    forEach(call.argsBlock.map(_.args).getOrElse(Seq())) apply continue between {
      append(", ")
    }
    append(")")
  }

  override def visit(defn: ClassDef): Unit = {
    append("var ", defn.name.value, " = ")
  }


  override def visit(defn: EnumDef): Unit = {
    append("var ", defn.name.value, " = ")
  }

  override def visit(defn: ValDef): Unit = {
    append("var ", defn.name.value, " = ")
    defn.expr.foreach { continue }
  }

  override def visit(defn: FunDef): Unit = {
    append("var ", defn.name.value, " = function(")

    defn.paramsBlock.foreach { block =>
      forEach(block.params) apply continue between {
        append(", ")
      }
    }
    appendln(") {", 1)
    forEach(defn.body.map(_.stmts).getOrElse(Seq())) apply continue between {
      appendln()
    }
    appendln(-1)
    append("}")
  }

  override def visit(body: FunBody): Unit = ???

  override def visit(lambda: Lambda): Unit = {
    appendln("function() {", 1)
    forEach(lambda.stmtBlock.stmts()) apply continue between {
      appendln()
    }
    appendln(-1)
    append(")")
  }

  override def visit(file: File): Unit = {
    forEach(file.stmts) apply {stmt =>
      continue(stmt)
      append(";")
    } between {
      appendln()
      appendln()
    }
  }

  override def visit(imprt: Import): Unit = ???

  override def visit(typaram: TyParam): Unit = ???

  override def visit(param: Param): Unit = {
    append(param.name.value)
  }

  override def visit(mtype: MethodType): Unit = ???

  override def visit(block: ArgsBlock): Unit = ???

  override def visit(block: TypeArgsBlock): Unit = ???

  override def visit(block: ParamBlock): Unit = ???

  override def visit(block: TyParamBlock): Unit = ???

  override def visit(block: StmtBlock): Unit = ???

  override def visit(block: ParentsBlock): Unit = ???

  override def visit(node: GetLocalVar): Unit = ???

  override def visit(node: SetLocalVar): Unit = ???
}
